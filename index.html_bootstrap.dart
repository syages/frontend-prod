library app_bootstrap;

import 'package:polymer/polymer.dart';

import 'package:polymer/src/build/log_injector.dart';
import 'package:core_elements/core_a11y_keys.dart' as i0;
import 'package:core_elements/core_selection.dart' as i1;
import 'package:core_elements/core_selector.dart' as i2;
import 'package:core_elements/core_animated_pages.dart' as i3;
import 'package:core_elements/core_animation.dart' as i4;
import 'package:core_elements/core_animation_group.dart' as i5;
import 'package:core_elements/core_collapse.dart' as i6;
import 'package:core_elements/core_drag_drop.dart' as i7;
import 'package:core_elements/core_media_query.dart' as i8;
import 'package:core_elements/core_drawer_panel.dart' as i9;
import 'package:core_elements/core_meta.dart' as i10;
import 'package:core_elements/core_transition.dart' as i11;
import 'package:core_elements/core_key_helper.dart' as i12;
import 'package:core_elements/core_overlay_layer.dart' as i13;
import 'package:core_elements/core_overlay.dart' as i14;
import 'package:core_elements/core_dropdown.dart' as i15;
import 'package:core_elements/core_dropdown_base.dart' as i16;
import 'package:core_elements/core_focusable.dart' as i17;
import 'package:core_elements/core_iconset.dart' as i18;
import 'package:core_elements/core_icon.dart' as i19;
import 'package:core_elements/core_iconset_svg.dart' as i20;
import 'package:core_elements/core_dropdown_menu.dart' as i21;
import 'package:core_elements/core_field.dart' as i22;
import 'package:core_elements/core_header_panel.dart' as i23;
import 'package:core_elements/core_icon_button.dart' as i24;
import 'package:core_elements/core_image.dart' as i25;
import 'package:core_elements/core_input.dart' as i26;
import 'package:core_elements/core_item.dart' as i27;
import 'package:core_elements/core_label.dart' as i28;
import 'package:core_elements/core_layout_grid.dart' as i29;
import 'package:core_elements/core_layout_trbl.dart' as i30;
import 'package:core_elements/core_list_dart.dart' as i31;
import 'package:core_elements/core_localstorage_dart.dart' as i32;
import 'package:core_elements/core_menu.dart' as i33;
import 'package:core_elements/core_menu_button.dart' as i34;
import 'package:core_elements/core_pages.dart' as i35;
import 'package:core_elements/core_range.dart' as i36;
import 'package:core_elements/core_toolbar.dart' as i37;
import 'package:core_elements/core_scaffold.dart' as i38;
import 'package:core_elements/core_scroll_header_panel.dart' as i39;
import 'package:core_elements/core_scroll_threshold.dart' as i40;
import 'package:core_elements/core_shared_lib.dart' as i41;
import 'package:core_elements/core_signals.dart' as i42;
import 'package:core_elements/core_slide.dart' as i43;
import 'package:core_elements/core_splitter.dart' as i44;
import 'package:core_elements/core_style.dart' as i45;
import 'package:core_elements/core_submenu.dart' as i46;
import 'package:core_elements/core_tooltip.dart' as i47;
import 'package:core_elements/core_transition_css.dart' as i48;
import 'package:paper_elements/paper_shadow.dart' as i49;
import 'package:paper_elements/paper_dialog_base.dart' as i50;
import 'package:paper_elements/paper_action_dialog.dart' as i51;
import 'package:paper_elements/paper_autogrow_textarea.dart' as i52;
import 'package:paper_elements/paper_ripple.dart' as i53;
import 'package:paper_elements/paper_button_base.dart' as i54;
import 'package:paper_elements/paper_button.dart' as i55;
import 'package:paper_elements/paper_radio_button.dart' as i56;
import 'package:paper_elements/paper_checkbox.dart' as i57;
import 'package:paper_elements/paper_dialog.dart' as i58;
import 'package:paper_elements/paper_dialog_transition.dart' as i59;
import 'package:paper_elements/paper_dropdown_transition.dart' as i60;
import 'package:paper_elements/paper_dropdown.dart' as i61;
import 'package:paper_elements/paper_dropdown_menu.dart' as i62;
import 'package:paper_elements/paper_fab.dart' as i63;
import 'package:paper_elements/paper_icon_button.dart' as i64;
import 'package:paper_elements/paper_input_decorator.dart' as i65;
import 'package:paper_elements/paper_input.dart' as i66;
import 'package:paper_elements/paper_item.dart' as i67;
import 'package:paper_elements/paper_menu_button.dart' as i68;
import 'package:paper_elements/paper_progress.dart' as i69;
import 'package:paper_elements/paper_radio_group.dart' as i70;
import 'package:paper_elements/paper_slider.dart' as i71;
import 'package:paper_elements/paper_spinner.dart' as i72;
import 'package:paper_elements/paper_tab.dart' as i73;
import 'package:paper_elements/paper_tabs.dart' as i74;
import 'package:paper_elements/paper_toast.dart' as i75;
import 'package:paper_elements/paper_toggle_button.dart' as i76;
import 'package:syages/controllers/syd_error_reporter.dart' as i77;
import 'package:syages/controllers/syv_dashboard.dart' as i78;
import 'package:syages/controllers/syv_login.dart' as i79;
import 'package:syages/controllers/syv_navbar.dart' as i80;
import 'package:syages/controllers/syv_newsfeed.dart' as i81;
import 'package:syages/controllers/syv_profile_form.dart' as i82;
import 'package:syages/controllers/a/syv_bugreport_detail.dart' as i83;
import 'package:syages/controllers/a/syv_bugreport_list.dart' as i84;
import 'package:syages/controllers/aps/syv_administrator_form.dart' as i85;
import 'package:syages/controllers/aps/syv_administrator_list.dart' as i86;
import 'package:syages/controllers/aps/syv_course_form.dart' as i87;
import 'package:syages/controllers/aps/syv_course_list.dart' as i88;
import 'package:syages/controllers/aps/syv_diploma_list.dart' as i89;
import 'package:syages/controllers/aps/syv_domain_form.dart' as i90;
import 'package:syages/controllers/aps/syv_domain_list.dart' as i91;
import 'package:syages/controllers/aps/syv_intern_form.dart' as i92;
import 'package:syages/controllers/aps/syv_intern_list.dart' as i93;
import 'package:syages/controllers/aps/syv_news_form.dart' as i94;
import 'package:syages/controllers/aps/syv_president_form.dart' as i95;
import 'package:syages/controllers/aps/syv_president_list.dart' as i96;
import 'package:syages/controllers/aps/syv_teacher_form.dart' as i97;
import 'package:syages/controllers/aps/syv_teacher_list.dart' as i98;
import 'package:syages/controllers/aps/syv_staff_form.dart' as i99;
import 'package:syages/controllers/aps/syv_staff_list.dart' as i100;
import 'package:syages/controllers/apst/syv_evaluation_form.dart' as i101;
import 'package:syages/controllers/apst/syv_lecture_form.dart' as i102;
import 'package:syages/controllers/i/syv_first_login.dart' as i103;
import 'package:syages/controllers/t/syv_teacher_lessons.dart' as i104;
import 'package:syages/controllers/t/syv_classroom.dart' as i105;
import 'package:syages/controllers/elements/sye_domain_selector.dart' as i106;
import 'package:syages/controllers/elements/sye_domains_selector.dart' as i107;
import 'package:syages/controllers/elements/paperDatePicker/paper_date_picker_dialog.dart' as i108;
import 'package:syages/controllers/elements/paperDatePicker/paper_date_picker.dart' as i109;
import 'package:syages/controllers/elements/paperDatePicker/paper_year_picker.dart' as i110;
import 'package:syages/controllers/elements/sye_teacher_selector.dart' as i111;
import 'package:syages/controllers/context_tester.dart' as i112;
import 'package:syages/syages_app.dart' as i113;
import 'index.html.0.dart' as i114;
import 'package:smoke/smoke.dart' show Declaration, PROPERTY, METHOD;
import 'package:smoke/static.dart' show useGeneratedCode, StaticConfiguration;
import 'package:core_elements/core_list_dart.dart' as smoke_0;
import 'package:polymer/polymer.dart' as smoke_1;
import 'package:observe/src/metadata.dart' as smoke_2;
import 'package:observe/src/observable_list.dart' as smoke_3;
import 'package:core_elements/core_localstorage_dart.dart' as smoke_4;
import 'package:syages/controllers/syd_error_reporter.dart' as smoke_5;
import 'package:syages/controllers/syv_dashboard.dart' as smoke_6;
import 'package:syages/ViewController.dart' as smoke_7;
import 'package:syages/SerializableElement.dart' as smoke_8;
import 'package:syages/models/User.dart' as smoke_9;
import 'package:syages/controllers/syv_login.dart' as smoke_10;
import 'package:syages/controllers/syv_navbar.dart' as smoke_11;
import 'package:syages/controllers/syv_newsfeed.dart' as smoke_12;
import 'package:syages/controllers/syv_profile_form.dart' as smoke_13;
import 'package:syages/controllers/a/syv_bugreport_detail.dart' as smoke_14;
import 'package:syages/models/Report.dart' as smoke_15;
import 'package:syages/controllers/a/syv_bugreport_list.dart' as smoke_16;
import 'package:syages/controllers/aps/syv_administrator_form.dart' as smoke_17;
import 'package:syages/models/Administrator.dart' as smoke_18;
import 'package:syages/controllers/aps/syv_administrator_list.dart' as smoke_19;
import 'package:syages/controllers/aps/syv_course_form.dart' as smoke_20;
import 'package:syages/models/Course.dart' as smoke_21;
import 'package:syages/controllers/aps/syv_course_list.dart' as smoke_22;
import 'package:syages/controllers/aps/syv_diploma_list.dart' as smoke_23;
import 'package:syages/controllers/aps/syv_domain_form.dart' as smoke_24;
import 'package:syages/models/Domain.dart' as smoke_25;
import 'package:syages/controllers/aps/syv_domain_list.dart' as smoke_26;
import 'package:syages/controllers/aps/syv_intern_form.dart' as smoke_27;
import 'package:syages/models/Intern.dart' as smoke_28;
import 'package:syages/controllers/aps/syv_intern_list.dart' as smoke_29;
import 'package:syages/controllers/aps/syv_news_form.dart' as smoke_30;
import 'package:syages/models/Information.dart' as smoke_31;
import 'package:syages/controllers/aps/syv_president_form.dart' as smoke_32;
import 'package:syages/models/President.dart' as smoke_33;
import 'package:syages/controllers/aps/syv_president_list.dart' as smoke_34;
import 'package:syages/controllers/aps/syv_teacher_form.dart' as smoke_35;
import 'package:syages/models/Teacher.dart' as smoke_36;
import 'package:syages/controllers/aps/syv_teacher_list.dart' as smoke_37;
import 'package:syages/controllers/aps/syv_staff_form.dart' as smoke_38;
import 'package:syages/models/Staff.dart' as smoke_39;
import 'package:syages/controllers/aps/syv_staff_list.dart' as smoke_40;
import 'package:syages/controllers/apst/syv_evaluation_form.dart' as smoke_41;
import 'package:syages/models/Evaluation.dart' as smoke_42;
import 'package:syages/controllers/apst/syv_lecture_form.dart' as smoke_43;
import 'package:syages/models/Lecture.dart' as smoke_44;
import 'package:syages/controllers/i/syv_first_login.dart' as smoke_45;
import 'package:syages/controllers/t/syv_teacher_lessons.dart' as smoke_46;
import 'package:syages/controllers/t/syv_classroom.dart' as smoke_47;
import 'package:syages/models/Lesson.dart' as smoke_48;
import 'package:syages/controllers/elements/sye_domain_selector.dart' as smoke_49;
import 'package:syages/controllers/elements/sye_domains_selector.dart' as smoke_50;
import 'package:syages/controllers/elements/paperDatePicker/paper_date_picker_dialog.dart' as smoke_51;
import 'package:syages/controllers/elements/paperDatePicker/paper_date_picker.dart' as smoke_52;
import 'package:syages/controllers/elements/paperDatePicker/paper_year_picker.dart' as smoke_53;
import 'package:syages/controllers/elements/sye_teacher_selector.dart' as smoke_54;
import 'package:syages/controllers/context_tester.dart' as smoke_55;
import 'package:syages/syages_app.dart' as smoke_56;
abstract class _M0 {} // PolymerElement & ChangeNotifier
abstract class _M1 {} // SerializableElement & ChangeNotifier
abstract class _M2 {} // ViewController & ChangeNotifier

void main() {
  useGeneratedCode(new StaticConfiguration(
      checkedMode: false,
      getters: {
        #action: (o) => o.action,
        #actions: (o) => o.actions,
        #addEval: (o) => o.addEval,
        #addLecture: (o) => o.addLecture,
        #administrator: (o) => o.administrator,
        #administratorId: (o) => o.administratorId,
        #administratorIdChanged: (o) => o.administratorIdChanged,
        #administrators: (o) => o.administrators,
        #author: (o) => o.author,
        #autoSaveDisabled: (o) => o.autoSaveDisabled,
        #bubble: (o) => o.bubble,
        #canGoBack: (o) => o.canGoBack,
        #canGoForward: (o) => o.canGoForward,
        #canGoHome: (o) => o.canGoHome,
        #cancel: (o) => o.cancel,
        #changeListener: (o) => o.changeListener,
        #checkedDomains: (o) => o.checkedDomains,
        #classroom: (o) => o.classroom,
        #coefficient: (o) => o.coefficient,
        #collapsed: (o) => o.collapsed,
        #commentary: (o) => o.commentary,
        #context: (o) => o.context,
        #controller: (o) => o.controller,
        #count: (o) => o.count,
        #course: (o) => o.course,
        #courseId: (o) => o.courseId,
        #courseIdChanged: (o) => o.courseIdChanged,
        #courses: (o) => o.courses,
        #create: (o) => o.create,
        #createdAt: (o) => o.createdAt,
        #currentDay: (o) => o.currentDay,
        #currentMonth: (o) => o.currentMonth,
        #currentYear: (o) => o.currentYear,
        #data: (o) => o.data,
        #date: (o) => o.date,
        #day: (o) => o.day,
        #dayOfMonth: (o) => o.dayOfMonth,
        #dayOfWeek: (o) => o.dayOfWeek,
        #days: (o) => o.days,
        #deployed: (o) => o.deployed,
        #dialogOpened: (o) => o.dialogOpened,
        #diploma: (o) => o.diploma,
        #diplomas: (o) => o.diplomas,
        #displayed: (o) => o.displayed,
        #domain: (o) => o.domain,
        #domainId: (o) => o.domainId,
        #domainIdChanged: (o) => o.domainIdChanged,
        #domains: (o) => o.domains,
        #edit: (o) => o.edit,
        #email: (o) => o.email,
        #enabled: (o) => o.enabled,
        #evaluation: (o) => o.evaluation,
        #evaluationId: (o) => o.evaluationId,
        #evaluationIdChanged: (o) => o.evaluationIdChanged,
        #evaluations: (o) => o.evaluations,
        #filter: (o) => o.filter,
        #firstName: (o) => o.firstName,
        #formatDate: (o) => o.formatDate,
        #formattedYear: (o) => o.formattedYear,
        #goBack: (o) => o.goBack,
        #goForward: (o) => o.goForward,
        #goHome: (o) => o.goHome,
        #goToProfileSettings: (o) => o.goToProfileSettings,
        #gradingScale: (o) => o.gradingScale,
        #grid: (o) => o.grid,
        #groups: (o) => o.groups,
        #groupsChanged: (o) => o.groupsChanged,
        #heading: (o) => o.heading,
        #height: (o) => o.height,
        #hour: (o) => o.hour,
        #id: (o) => o.id,
        #immediateDate: (o) => o.immediateDate,
        #immediateDateChanged: (o) => o.immediateDateChanged,
        #infiniteScrolling: (o) => o.infiniteScrolling,
        #information: (o) => o.information,
        #informationId: (o) => o.informationId,
        #informationIdChanged: (o) => o.informationIdChanged,
        #informations: (o) => o.informations,
        #initialize: (o) => o.initialize,
        #intern: (o) => o.intern,
        #internId: (o) => o.internId,
        #interns: (o) => o.interns,
        #isGroup: (o) => o.isGroup,
        #isPhone: (o) => o.isPhone,
        #job: (o) => o.job,
        #kind: (o) => o.kind,
        #label: (o) => o.label,
        #lastName: (o) => o.lastName,
        #lastSchool: (o) => o.lastSchool,
        #lecture: (o) => o.lecture,
        #lectureId: (o) => o.lectureId,
        #lectureIdChanged: (o) => o.lectureIdChanged,
        #lectures: (o) => o.lectures,
        #length: (o) => o.length,
        #lesson: (o) => o.lesson,
        #lessonId: (o) => o.lessonId,
        #lessonIdChanged: (o) => o.lessonIdChanged,
        #lessonName: (o) => o.lessonName,
        #lessons: (o) => o.lessons,
        #loaded: (o) => o.loaded,
        #locale: (o) => o.locale,
        #loggedUser: (o) => o.loggedUser,
        #login: (o) => o.login,
        #logout: (o) => o.logout,
        #max: (o) => o.max,
        #mediaChanged: (o) => o.mediaChanged,
        #message: (o) => o.message,
        #min: (o) => o.min,
        #model: (o) => o.model,
        #month: (o) => o.month,
        #monthShortName: (o) => o.monthShortName,
        #months: (o) => o.months,
        #more: (o) => o.more,
        #multi: (o) => o.multi,
        #n: (o) => o.n,
        #name: (o) => o.name,
        #navbar: (o) => o.navbar,
        #navigationClass: (o) => o.navigationClass,
        #number: (o) => o.number,
        #office: (o) => o.office,
        #onTap: (o) => o.onTap,
        #open: (o) => o.open,
        #page: (o) => o.page,
        #pageChanged: (o) => o.pageChanged,
        #password: (o) => o.password,
        #pickDate: (o) => o.pickDate,
        #pictureUrl: (o) => o.pictureUrl,
        #pop: (o) => o.pop,
        #post: (o) => o.post,
        #president: (o) => o.president,
        #presidentId: (o) => o.presidentId,
        #presidentIdChanged: (o) => o.presidentIdChanged,
        #presidents: (o) => o.presidents,
        #priority: (o) => o.priority,
        #professionalProjet: (o) => o.professionalProjet,
        #profileIdChanged: (o) => o.profileIdChanged,
        #push: (o) => o.push,
        #recurrence: (o) => o.recurrence,
        #refresh: (o) => o.refresh,
        #replace: (o) => o.replace,
        #report: (o) => o.report,
        #reportId: (o) => o.reportId,
        #reportIdChanged: (o) => o.reportIdChanged,
        #reports: (o) => o.reports,
        #resetSelection: (o) => o.resetSelection,
        #route: (o) => o.route,
        #runwayFactor: (o) => o.runwayFactor,
        #save: (o) => o.save,
        #scrollFinished: (o) => o.scrollFinished,
        #scrollTarget: (o) => o.scrollTarget,
        #selected: (o) => o.selected,
        #selectedChanged: (o) => o.selectedChanged,
        #selectedHandler: (o) => o.selectedHandler,
        #selection: (o) => o.selection,
        #selectionEnabled: (o) => o.selectionEnabled,
        #send: (o) => o.send,
        #setDate: (o) => o.setDate,
        #showDatePicker: (o) => o.showDatePicker,
        #showYearPicker: (o) => o.showYearPicker,
        #staff: (o) => o.staff,
        #staffId: (o) => o.staffId,
        #staffIdChanged: (o) => o.staffIdChanged,
        #staffs: (o) => o.staffs,
        #startDayOfWeek: (o) => o.startDayOfWeek,
        #status: (o) => o.status,
        #supervisor: (o) => o.supervisor,
        #tapHandler: (o) => o.tapHandler,
        #teacher: (o) => o.teacher,
        #teacherId: (o) => o.teacherId,
        #teacherIdChanged: (o) => o.teacherIdChanged,
        #teachers: (o) => o.teachers,
        #text: (o) => o.text,
        #title: (o) => o.title,
        #toggle: (o) => o.toggle,
        #tokenList: (o) => o.tokenList,
        #type: (o) => o.type,
        #typeSelect: (o) => o.typeSelect,
        #updateData: (o) => o.updateData,
        #useRaw: (o) => o.useRaw,
        #user: (o) => o.user,
        #userId: (o) => o.userId,
        #userInfo: (o) => o.userInfo,
        #validate: (o) => o.validate,
        #value: (o) => o.value,
        #valueChanged: (o) => o.valueChanged,
        #values: (o) => o.values,
        #valuesChanged: (o) => o.valuesChanged,
        #weekDayNames: (o) => o.weekDayNames,
        #width: (o) => o.width,
        #y: (o) => o.y,
        #year: (o) => o.year,
        #yearChanged: (o) => o.yearChanged,
        #yearElementHeight: (o) => o.yearElementHeight,
        #yearSelected: (o) => o.yearSelected,
        #years: (o) => o.years,
      },
      setters: {
        #action: (o, v) { o.action = v; },
        #administrator: (o, v) { o.administrator = v; },
        #administratorId: (o, v) { o.administratorId = v; },
        #administrators: (o, v) { o.administrators = v; },
        #autoSaveDisabled: (o, v) { o.autoSaveDisabled = v; },
        #bubble: (o, v) { o.bubble = v; },
        #canGoBack: (o, v) { o.canGoBack = v; },
        #canGoForward: (o, v) { o.canGoForward = v; },
        #canGoHome: (o, v) { o.canGoHome = v; },
        #checkedDomains: (o, v) { o.checkedDomains = v; },
        #classroom: (o, v) { o.classroom = v; },
        #coefficient: (o, v) { o.coefficient = v; },
        #commentary: (o, v) { o.commentary = v; },
        #context: (o, v) { o.context = v; },
        #controller: (o, v) { o.controller = v; },
        #count: (o, v) { o.count = v; },
        #course: (o, v) { o.course = v; },
        #courseId: (o, v) { o.courseId = v; },
        #courses: (o, v) { o.courses = v; },
        #data: (o, v) { o.data = v; },
        #date: (o, v) { o.date = v; },
        #dayOfMonth: (o, v) { o.dayOfMonth = v; },
        #dayOfWeek: (o, v) { o.dayOfWeek = v; },
        #deployed: (o, v) { o.deployed = v; },
        #diplomas: (o, v) { o.diplomas = v; },
        #displayed: (o, v) { o.displayed = v; },
        #domain: (o, v) { o.domain = v; },
        #domainId: (o, v) { o.domainId = v; },
        #domains: (o, v) { o.domains = v; },
        #edit: (o, v) { o.edit = v; },
        #email: (o, v) { o.email = v; },
        #evaluation: (o, v) { o.evaluation = v; },
        #evaluationId: (o, v) { o.evaluationId = v; },
        #evaluations: (o, v) { o.evaluations = v; },
        #filter: (o, v) { o.filter = v; },
        #firstName: (o, v) { o.firstName = v; },
        #formattedYear: (o, v) { o.formattedYear = v; },
        #gradingScale: (o, v) { o.gradingScale = v; },
        #grid: (o, v) { o.grid = v; },
        #groups: (o, v) { o.groups = v; },
        #height: (o, v) { o.height = v; },
        #hour: (o, v) { o.hour = v; },
        #id: (o, v) { o.id = v; },
        #immediateDate: (o, v) { o.immediateDate = v; },
        #infiniteScrolling: (o, v) { o.infiniteScrolling = v; },
        #information: (o, v) { o.information = v; },
        #informationId: (o, v) { o.informationId = v; },
        #informations: (o, v) { o.informations = v; },
        #intern: (o, v) { o.intern = v; },
        #internId: (o, v) { o.internId = v; },
        #interns: (o, v) { o.interns = v; },
        #isPhone: (o, v) { o.isPhone = v; },
        #job: (o, v) { o.job = v; },
        #lastName: (o, v) { o.lastName = v; },
        #lastSchool: (o, v) { o.lastSchool = v; },
        #lecture: (o, v) { o.lecture = v; },
        #lectureId: (o, v) { o.lectureId = v; },
        #lectures: (o, v) { o.lectures = v; },
        #lesson: (o, v) { o.lesson = v; },
        #lessonId: (o, v) { o.lessonId = v; },
        #lessonName: (o, v) { o.lessonName = v; },
        #lessons: (o, v) { o.lessons = v; },
        #loaded: (o, v) { o.loaded = v; },
        #locale: (o, v) { o.locale = v; },
        #loggedUser: (o, v) { o.loggedUser = v; },
        #max: (o, v) { o.max = v; },
        #message: (o, v) { o.message = v; },
        #min: (o, v) { o.min = v; },
        #model: (o, v) { o.model = v; },
        #monthShortName: (o, v) { o.monthShortName = v; },
        #multi: (o, v) { o.multi = v; },
        #name: (o, v) { o.name = v; },
        #navbar: (o, v) { o.navbar = v; },
        #navigationClass: (o, v) { o.navigationClass = v; },
        #number: (o, v) { o.number = v; },
        #office: (o, v) { o.office = v; },
        #page: (o, v) { o.page = v; },
        #password: (o, v) { o.password = v; },
        #pictureUrl: (o, v) { o.pictureUrl = v; },
        #post: (o, v) { o.post = v; },
        #president: (o, v) { o.president = v; },
        #presidentId: (o, v) { o.presidentId = v; },
        #presidents: (o, v) { o.presidents = v; },
        #priority: (o, v) { o.priority = v; },
        #professionalProjet: (o, v) { o.professionalProjet = v; },
        #recurrence: (o, v) { o.recurrence = v; },
        #report: (o, v) { o.report = v; },
        #reportId: (o, v) { o.reportId = v; },
        #reports: (o, v) { o.reports = v; },
        #runwayFactor: (o, v) { o.runwayFactor = v; },
        #scrollTarget: (o, v) { o.scrollTarget = v; },
        #selected: (o, v) { o.selected = v; },
        #selection: (o, v) { o.selection = v; },
        #selectionEnabled: (o, v) { o.selectionEnabled = v; },
        #staff: (o, v) { o.staff = v; },
        #staffId: (o, v) { o.staffId = v; },
        #staffs: (o, v) { o.staffs = v; },
        #startDayOfWeek: (o, v) { o.startDayOfWeek = v; },
        #teacher: (o, v) { o.teacher = v; },
        #teacherId: (o, v) { o.teacherId = v; },
        #teachers: (o, v) { o.teachers = v; },
        #text: (o, v) { o.text = v; },
        #title: (o, v) { o.title = v; },
        #type: (o, v) { o.type = v; },
        #useRaw: (o, v) { o.useRaw = v; },
        #user: (o, v) { o.user = v; },
        #userId: (o, v) { o.userId = v; },
        #userInfo: (o, v) { o.userInfo = v; },
        #value: (o, v) { o.value = v; },
        #values: (o, v) { o.values = v; },
        #width: (o, v) { o.width = v; },
        #year: (o, v) { o.year = v; },
        #yearElementHeight: (o, v) { o.yearElementHeight = v; },
        #years: (o, v) { o.years = v; },
      },
      parents: {
        smoke_0.CoreList: _M0,
        smoke_4.CoreLocalStorage: _M0,
        smoke_8.SerializableElement: _M0,
        smoke_7.ViewController: _M1,
        smoke_14.BugReportDetailController: _M2,
        smoke_16.BugReportListController: _M2,
        smoke_17.AdministratorFormController: _M2,
        smoke_19.AdministratorListController: _M2,
        smoke_20.CourseFormController: _M2,
        smoke_22.CourseListController: _M2,
        smoke_23.DiplomaListController: _M2,
        smoke_24.DomainFormController: _M2,
        smoke_26.DomainListController: _M2,
        smoke_27.InternFormController: _M2,
        smoke_29.InternListController: _M2,
        smoke_30.NewsFormController: _M2,
        smoke_32.PresidentFormController: _M2,
        smoke_34.PresidentListController: _M2,
        smoke_38.StaffFormController: _M2,
        smoke_40.StaffListController: _M2,
        smoke_35.TeacherFormController: _M2,
        smoke_37.TeacherListController: _M2,
        smoke_41.EvaluationFormController: _M2,
        smoke_43.LectureFormController: _M2,
        smoke_55.ContextTesterController: _M2,
        smoke_52.PaperDatePickerController: _M0,
        smoke_51.PaperDatePickerDialogController: _M0,
        smoke_53.PaperYearPickerController: _M0,
        smoke_49.DomainSelector: _M0,
        smoke_50.DomainsSelector: _M0,
        smoke_54.TeacherSelector: _M0,
        smoke_45.FirstLoginController: _M2,
        smoke_5.ErrorReporterController: _M0,
        smoke_6.DashboardController: smoke_7.ViewController,
        smoke_10.LoginController: _M2,
        smoke_11.ActionController: _M0,
        smoke_11.ActionGroupController: _M0,
        smoke_11.NavbarController: _M0,
        smoke_12.NewsFeedController: _M2,
        smoke_13.profileFormController: _M2,
        smoke_47.TeacherClassroomController: _M2,
        smoke_46.TeacherLessonsController: _M2,
        smoke_56.$SyagesAppController: _M0,
        _M0: smoke_1.PolymerElement,
        _M1: smoke_8.SerializableElement,
        _M2: smoke_7.ViewController,
      },
      declarations: {
        smoke_0.CoreList: {
          #data: const Declaration(#data, smoke_3.ObservableList, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #grid: const Declaration(#grid, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #groups: const Declaration(#groups, smoke_3.ObservableList, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #groupsChanged: const Declaration(#groupsChanged, Function, kind: METHOD),
          #height: const Declaration(#height, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #initialize: const Declaration(#initialize, Function, kind: METHOD, annotations: const [const smoke_1.ObserveProperty('data grid width template scrollTarget')]),
          #multi: const Declaration(#multi, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #resetSelection: const Declaration(#resetSelection, Function, kind: METHOD, annotations: const [const smoke_1.ObserveProperty('multi selectionEnabled')]),
          #runwayFactor: const Declaration(#runwayFactor, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #scrollTarget: const Declaration(#scrollTarget, dynamic, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #selection: const Declaration(#selection, dynamic, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #selectionEnabled: const Declaration(#selectionEnabled, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #updateData: const Declaration(#updateData, Function, kind: METHOD, annotations: const [const smoke_1.ObserveProperty('data')]),
          #width: const Declaration(#width, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
        },
        smoke_4.CoreLocalStorage: {
          #autoSaveDisabled: const Declaration(#autoSaveDisabled, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #loaded: const Declaration(#loaded, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #name: const Declaration(#name, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #useRaw: const Declaration(#useRaw, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #value: const Declaration(#value, dynamic, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #valueChanged: const Declaration(#valueChanged, Function, kind: METHOD),
        },
        smoke_8.SerializableElement: {
          #controller: const Declaration(#controller, smoke_8.SerializableElement, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_7.ViewController: {
          #loggedUser: const Declaration(#loggedUser, smoke_9.User, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_14.BugReportDetailController: {
          #report: const Declaration(#report, smoke_15.Report, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #reportId: const Declaration(#reportId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #reportIdChanged: const Declaration(#reportIdChanged, Function, kind: METHOD),
        },
        smoke_16.BugReportListController: {
          #filter: const Declaration(#filter, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #reports: const Declaration(#reports, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_17.AdministratorFormController: {
          #administrator: const Declaration(#administrator, smoke_18.Administrator, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #administratorId: const Declaration(#administratorId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #administratorIdChanged: const Declaration(#administratorIdChanged, Function, kind: METHOD),
        },
        smoke_19.AdministratorListController: {
          #administrators: const Declaration(#administrators, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_20.CourseFormController: {
          #course: const Declaration(#course, smoke_21.Course, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #courseId: const Declaration(#courseId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #courseIdChanged: const Declaration(#courseIdChanged, Function, kind: METHOD),
        },
        smoke_22.CourseListController: {
          #courses: const Declaration(#courses, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_23.DiplomaListController: {
          #diplomas: const Declaration(#diplomas, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_24.DomainFormController: {
          #domain: const Declaration(#domain, smoke_25.Domain, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #domainId: const Declaration(#domainId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #domainIdChanged: const Declaration(#domainIdChanged, Function, kind: METHOD),
        },
        smoke_26.DomainListController: {
          #domains: const Declaration(#domains, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_27.InternFormController: {
          #intern: const Declaration(#intern, smoke_28.Intern, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #internId: const Declaration(#internId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
        },
        smoke_29.InternListController: {
          #interns: const Declaration(#interns, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_30.NewsFormController: {
          #edit: const Declaration(#edit, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #information: const Declaration(#information, smoke_31.Information, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #informationId: const Declaration(#informationId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #informationIdChanged: const Declaration(#informationIdChanged, Function, kind: METHOD),
        },
        smoke_32.PresidentFormController: {
          #president: const Declaration(#president, smoke_33.President, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #presidentId: const Declaration(#presidentId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #presidentIdChanged: const Declaration(#presidentIdChanged, Function, kind: METHOD),
        },
        smoke_34.PresidentListController: {
          #presidents: const Declaration(#presidents, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_38.StaffFormController: {
          #staff: const Declaration(#staff, smoke_39.Staff, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #staffId: const Declaration(#staffId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #staffIdChanged: const Declaration(#staffIdChanged, Function, kind: METHOD),
        },
        smoke_40.StaffListController: {
          #staffs: const Declaration(#staffs, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_35.TeacherFormController: {
          #teacher: const Declaration(#teacher, smoke_36.Teacher, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #teacherId: const Declaration(#teacherId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #teacherIdChanged: const Declaration(#teacherIdChanged, Function, kind: METHOD),
        },
        smoke_37.TeacherListController: {
          #teachers: const Declaration(#teachers, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_41.EvaluationFormController: {
          #evaluation: const Declaration(#evaluation, smoke_42.Evaluation, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #evaluationId: const Declaration(#evaluationId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #evaluationIdChanged: const Declaration(#evaluationIdChanged, Function, kind: METHOD),
          #lessonId: const Declaration(#lessonId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #lessonName: const Declaration(#lessonName, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
        },
        smoke_43.LectureFormController: {
          #lecture: const Declaration(#lecture, smoke_44.Lecture, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #lectureId: const Declaration(#lectureId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #lectureIdChanged: const Declaration(#lectureIdChanged, Function, kind: METHOD),
          #lessonId: const Declaration(#lessonId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #lessonName: const Declaration(#lessonName, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
        },
        smoke_55.ContextTesterController: {
          #count: const Declaration(#count, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #model: const Declaration(#model, Map, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_52.PaperDatePickerController: {
          #date: const Declaration(#date, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #infiniteScrolling: const Declaration(#infiniteScrolling, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #max: const Declaration(#max, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #min: const Declaration(#min, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #startDayOfWeek: const Declaration(#startDayOfWeek, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_51.PaperDatePickerDialogController: {
          #dayOfMonth: const Declaration(#dayOfMonth, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #dayOfWeek: const Declaration(#dayOfWeek, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #formattedYear: const Declaration(#formattedYear, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #immediateDate: const Declaration(#immediateDate, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #immediateDateChanged: const Declaration(#immediateDateChanged, Function, kind: METHOD),
          #infiniteScrolling: const Declaration(#infiniteScrolling, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #max: const Declaration(#max, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #min: const Declaration(#min, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #monthShortName: const Declaration(#monthShortName, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #page: const Declaration(#page, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #pageChanged: const Declaration(#pageChanged, Function, kind: METHOD),
          #startDayOfWeek: const Declaration(#startDayOfWeek, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #value: const Declaration(#value, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #year: const Declaration(#year, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #yearChanged: const Declaration(#yearChanged, Function, kind: METHOD),
        },
        smoke_53.PaperYearPickerController: {
          #max: const Declaration(#max, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #min: const Declaration(#min, DateTime, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #selected: const Declaration(#selected, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #year: const Declaration(#year, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #yearChanged: const Declaration(#yearChanged, Function, kind: METHOD),
          #yearElementHeight: const Declaration(#yearElementHeight, int, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #years: const Declaration(#years, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_49.DomainSelector: {
          #domains: const Declaration(#domains, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #selected: const Declaration(#selected, dynamic, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #selectedChanged: const Declaration(#selectedChanged, Function, kind: METHOD),
          #value: const Declaration(#value, smoke_25.Domain, kind: PROPERTY, annotations: const [smoke_1.published]),
          #valueChanged: const Declaration(#valueChanged, Function, kind: METHOD),
        },
        smoke_50.DomainsSelector: {
          #checkedDomains: const Declaration(#checkedDomains, Map, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #domains: const Declaration(#domains, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #values: const Declaration(#values, List, kind: PROPERTY, annotations: const [smoke_1.published]),
          #valuesChanged: const Declaration(#valuesChanged, Function, kind: METHOD),
        },
        smoke_54.TeacherSelector: {
          #selected: const Declaration(#selected, dynamic, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #selectedChanged: const Declaration(#selectedChanged, Function, kind: METHOD),
          #teachers: const Declaration(#teachers, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #value: const Declaration(#value, smoke_36.Teacher, kind: PROPERTY, annotations: const [smoke_1.published]),
          #valueChanged: const Declaration(#valueChanged, Function, kind: METHOD),
        },
        smoke_45.FirstLoginController: {
          #model: const Declaration(#model, smoke_45.FirstLoginModel, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_5.ErrorReporterController: {
          #deployed: const Declaration(#deployed, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #message: const Declaration(#message, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #priority: const Declaration(#priority, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #type: const Declaration(#type, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_6.DashboardController: {},
        smoke_10.LoginController: {
          #isPhone: const Declaration(#isPhone, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #model: const Declaration(#model, smoke_10.LoginModel, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_11.ActionController: {
          #model: const Declaration(#model, smoke_11.Action, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
        },
        smoke_11.ActionGroupController: {
          #model: const Declaration(#model, smoke_11.ActionGroup, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
        },
        smoke_11.NavbarController: {
          #canGoBack: const Declaration(#canGoBack, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #canGoForward: const Declaration(#canGoForward, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #canGoHome: const Declaration(#canGoHome, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #displayed: const Declaration(#displayed, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #navbar: const Declaration(#navbar, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #user: const Declaration(#user, smoke_9.User, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_12.NewsFeedController: {
          #informations: const Declaration(#informations, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_13.profileFormController: {
          #profileIdChanged: const Declaration(#profileIdChanged, Function, kind: METHOD),
          #user: const Declaration(#user, smoke_9.User, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #userId: const Declaration(#userId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
        },
        smoke_47.TeacherClassroomController: {
          #evaluations: const Declaration(#evaluations, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #lectures: const Declaration(#lectures, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #lesson: const Declaration(#lesson, smoke_48.Lesson, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #lessonId: const Declaration(#lessonId, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_1.published]),
          #lessonIdChanged: const Declaration(#lessonIdChanged, Function, kind: METHOD),
        },
        smoke_46.TeacherLessonsController: {
          #lessons: const Declaration(#lessons, List, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
        smoke_56.$SyagesAppController: {
          #bubble: const Declaration(#bubble, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #isPhone: const Declaration(#isPhone, bool, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
          #mediaChanged: const Declaration(#mediaChanged, Function, kind: METHOD),
          #navigationClass: const Declaration(#navigationClass, String, kind: PROPERTY, annotations: const [smoke_2.reflectable, smoke_2.observable]),
        },
      },
      names: {
        #action: r'action',
        #actions: r'actions',
        #addEval: r'addEval',
        #addLecture: r'addLecture',
        #administrator: r'administrator',
        #administratorId: r'administratorId',
        #administratorIdChanged: r'administratorIdChanged',
        #administrators: r'administrators',
        #author: r'author',
        #autoSaveDisabled: r'autoSaveDisabled',
        #bubble: r'bubble',
        #canGoBack: r'canGoBack',
        #canGoForward: r'canGoForward',
        #canGoHome: r'canGoHome',
        #cancel: r'cancel',
        #changeListener: r'changeListener',
        #checkedDomains: r'checkedDomains',
        #classroom: r'classroom',
        #coefficient: r'coefficient',
        #collapsed: r'collapsed',
        #commentary: r'commentary',
        #context: r'context',
        #controller: r'controller',
        #count: r'count',
        #course: r'course',
        #courseId: r'courseId',
        #courseIdChanged: r'courseIdChanged',
        #courses: r'courses',
        #create: r'create',
        #createdAt: r'createdAt',
        #currentDay: r'currentDay',
        #currentMonth: r'currentMonth',
        #currentYear: r'currentYear',
        #data: r'data',
        #date: r'date',
        #day: r'day',
        #dayOfMonth: r'dayOfMonth',
        #dayOfWeek: r'dayOfWeek',
        #days: r'days',
        #deployed: r'deployed',
        #dialogOpened: r'dialogOpened',
        #diploma: r'diploma',
        #diplomas: r'diplomas',
        #displayed: r'displayed',
        #domain: r'domain',
        #domainId: r'domainId',
        #domainIdChanged: r'domainIdChanged',
        #domains: r'domains',
        #edit: r'edit',
        #email: r'email',
        #enabled: r'enabled',
        #evaluation: r'evaluation',
        #evaluationId: r'evaluationId',
        #evaluationIdChanged: r'evaluationIdChanged',
        #evaluations: r'evaluations',
        #filter: r'filter',
        #firstName: r'firstName',
        #formatDate: r'formatDate',
        #formattedYear: r'formattedYear',
        #goBack: r'goBack',
        #goForward: r'goForward',
        #goHome: r'goHome',
        #goToProfileSettings: r'goToProfileSettings',
        #gradingScale: r'gradingScale',
        #grid: r'grid',
        #groups: r'groups',
        #groupsChanged: r'groupsChanged',
        #heading: r'heading',
        #height: r'height',
        #hour: r'hour',
        #id: r'id',
        #immediateDate: r'immediateDate',
        #immediateDateChanged: r'immediateDateChanged',
        #infiniteScrolling: r'infiniteScrolling',
        #information: r'information',
        #informationId: r'informationId',
        #informationIdChanged: r'informationIdChanged',
        #informations: r'informations',
        #initialize: r'initialize',
        #intern: r'intern',
        #internId: r'internId',
        #interns: r'interns',
        #isGroup: r'isGroup',
        #isPhone: r'isPhone',
        #job: r'job',
        #kind: r'kind',
        #label: r'label',
        #lastName: r'lastName',
        #lastSchool: r'lastSchool',
        #lecture: r'lecture',
        #lectureId: r'lectureId',
        #lectureIdChanged: r'lectureIdChanged',
        #lectures: r'lectures',
        #length: r'length',
        #lesson: r'lesson',
        #lessonId: r'lessonId',
        #lessonIdChanged: r'lessonIdChanged',
        #lessonName: r'lessonName',
        #lessons: r'lessons',
        #loaded: r'loaded',
        #locale: r'locale',
        #loggedUser: r'loggedUser',
        #login: r'login',
        #logout: r'logout',
        #max: r'max',
        #mediaChanged: r'mediaChanged',
        #message: r'message',
        #min: r'min',
        #model: r'model',
        #month: r'month',
        #monthShortName: r'monthShortName',
        #months: r'months',
        #more: r'more',
        #multi: r'multi',
        #n: r'n',
        #name: r'name',
        #navbar: r'navbar',
        #navigationClass: r'navigationClass',
        #number: r'number',
        #office: r'office',
        #onTap: r'onTap',
        #open: r'open',
        #page: r'page',
        #pageChanged: r'pageChanged',
        #password: r'password',
        #pickDate: r'pickDate',
        #pictureUrl: r'pictureUrl',
        #pop: r'pop',
        #post: r'post',
        #president: r'president',
        #presidentId: r'presidentId',
        #presidentIdChanged: r'presidentIdChanged',
        #presidents: r'presidents',
        #priority: r'priority',
        #professionalProjet: r'professionalProjet',
        #profileIdChanged: r'profileIdChanged',
        #push: r'push',
        #recurrence: r'recurrence',
        #refresh: r'refresh',
        #replace: r'replace',
        #report: r'report',
        #reportId: r'reportId',
        #reportIdChanged: r'reportIdChanged',
        #reports: r'reports',
        #resetSelection: r'resetSelection',
        #route: r'route',
        #runwayFactor: r'runwayFactor',
        #save: r'save',
        #scrollFinished: r'scrollFinished',
        #scrollTarget: r'scrollTarget',
        #selected: r'selected',
        #selectedChanged: r'selectedChanged',
        #selectedHandler: r'selectedHandler',
        #selection: r'selection',
        #selectionEnabled: r'selectionEnabled',
        #send: r'send',
        #setDate: r'setDate',
        #showDatePicker: r'showDatePicker',
        #showYearPicker: r'showYearPicker',
        #staff: r'staff',
        #staffId: r'staffId',
        #staffIdChanged: r'staffIdChanged',
        #staffs: r'staffs',
        #startDayOfWeek: r'startDayOfWeek',
        #status: r'status',
        #supervisor: r'supervisor',
        #tapHandler: r'tapHandler',
        #teacher: r'teacher',
        #teacherId: r'teacherId',
        #teacherIdChanged: r'teacherIdChanged',
        #teachers: r'teachers',
        #text: r'text',
        #title: r'title',
        #toggle: r'toggle',
        #tokenList: r'tokenList',
        #type: r'type',
        #typeSelect: r'typeSelect',
        #updateData: r'updateData',
        #useRaw: r'useRaw',
        #user: r'user',
        #userId: r'userId',
        #userInfo: r'userInfo',
        #validate: r'validate',
        #value: r'value',
        #valueChanged: r'valueChanged',
        #values: r'values',
        #valuesChanged: r'valuesChanged',
        #weekDayNames: r'weekDayNames',
        #width: r'width',
        #y: r'y',
        #year: r'year',
        #yearChanged: r'yearChanged',
        #yearElementHeight: r'yearElementHeight',
        #yearSelected: r'yearSelected',
        #years: r'years',
      }));
  new LogInjector().injectLogsFromUrl('index.html._buildLogs');
  configureForDeployment([
      i0.upgradeCoreA11yKeys,
      i1.upgradeCoreSelection,
      i2.upgradeCoreSelector,
      i3.upgradeCoreAnimatedPages,
      i4.upgradeCoreAnimation,
      i4.upgradeCoreAnimationKeyframe,
      i4.upgradeCoreAnimationProp,
      i5.upgradeCoreAnimationGroup,
      i6.upgradeCoreCollapse,
      i7.upgradeCoreDragDrop,
      i8.upgradeCoreMediaQuery,
      i9.upgradeCoreDrawerPanel,
      i10.upgradeCoreMeta,
      i11.upgradeCoreTransition,
      i12.upgradeCoreKeyHelper,
      i13.upgradeCoreOverlayLayer,
      i14.upgradeCoreOverlay,
      i15.upgradeCoreDropdown,
      i16.upgradeCoreDropdownBase,
      i18.upgradeCoreIconset,
      i19.upgradeCoreIcon,
      i20.upgradeCoreIconsetSvg,
      i21.upgradeCoreDropdownMenu,
      i22.upgradeCoreField,
      i23.upgradeCoreHeaderPanel,
      i24.upgradeCoreIconButton,
      i25.upgradeCoreImage,
      i26.upgradeCoreInput,
      i27.upgradeCoreItem,
      i28.upgradeCoreLabel,
      i29.upgradeCoreLayoutGrid,
      i30.upgradeCoreLayoutTrbl,
      () => Polymer.register('core-list-dart', i31.CoreList),
      () => Polymer.register('core-localstorage-dart', i32.CoreLocalStorage),
      i33.upgradeCoreMenu,
      i34.upgradeCoreMenuButton,
      i35.upgradeCorePages,
      i36.upgradeCoreRange,
      i37.upgradeCoreToolbar,
      i38.upgradeCoreScaffold,
      i39.upgradeCoreScrollHeaderPanel,
      i40.upgradeCoreScrollThreshold,
      i41.upgradeCoreSharedLib,
      i42.upgradeCoreSignals,
      i43.upgradeCoreSlide,
      i44.upgradeCoreSplitter,
      i45.upgradeCoreStyle,
      i46.upgradeCoreSubmenu,
      i47.upgradeCoreTooltip,
      i48.upgradeCoreTransitionCss,
      i49.upgradePaperShadow,
      i50.upgradePaperDialogBase,
      i51.upgradePaperActionDialog,
      i52.upgradePaperAutogrowTextarea,
      i53.upgradePaperRipple,
      i54.upgradePaperButtonBase,
      i55.upgradePaperButton,
      i56.upgradePaperRadioButton,
      i57.upgradePaperCheckbox,
      i58.upgradePaperDialog,
      i59.upgradePaperDialogTransition,
      i60.upgradePaperDropdownTransition,
      i61.upgradePaperDropdown,
      i62.upgradePaperDropdownMenu,
      i63.upgradePaperFab,
      i64.upgradePaperIconButton,
      i65.upgradePaperInputDecorator,
      i66.upgradePaperInput,
      i67.upgradePaperItem,
      i68.upgradePaperMenuButton,
      i69.upgradePaperProgress,
      i70.upgradePaperRadioGroup,
      i71.upgradePaperSlider,
      i72.upgradePaperSpinner,
      i73.upgradePaperTab,
      i74.upgradePaperTabs,
      i75.upgradePaperToast,
      i76.upgradePaperToggleButton,
      () => Polymer.register('syd-error-reporter', i77.ErrorReporterController),
      () => Polymer.register('syv-dashboard', i78.DashboardController),
      () => Polymer.register('syv-login', i79.LoginController),
      () => Polymer.register('syv-navbar', i80.NavbarController),
      () => Polymer.register('sye-navbar-action', i80.ActionController),
      () => Polymer.register('sye-navbar-group', i80.ActionGroupController),
      () => Polymer.register('syv-newsfeed', i81.NewsFeedController),
      () => Polymer.register('syv-profile-form', i82.profileFormController),
      () => Polymer.register('syv-bugreport-detail--a', i83.BugReportDetailController),
      () => Polymer.register('syv-bugreport-list--a', i84.BugReportListController),
      () => Polymer.register('syv-administrator-form--aps', i85.AdministratorFormController),
      () => Polymer.register('syv-administrator-list--aps', i86.AdministratorListController),
      () => Polymer.register('syv-course-form--aps', i87.CourseFormController),
      () => Polymer.register('syv-course-list--aps', i88.CourseListController),
      () => Polymer.register('syv-diploma-list--aps', i89.DiplomaListController),
      () => Polymer.register('syv-domain-form--aps', i90.DomainFormController),
      () => Polymer.register('syv-domain-list--aps', i91.DomainListController),
      () => Polymer.register('syv-intern-form--aps', i92.InternFormController),
      () => Polymer.register('syv-intern-list--aps', i93.InternListController),
      () => Polymer.register('syv-news-form--aps', i94.NewsFormController),
      () => Polymer.register('syv-president-form--aps', i95.PresidentFormController),
      () => Polymer.register('syv-president-list--aps', i96.PresidentListController),
      () => Polymer.register('syv-teacher-form--aps', i97.TeacherFormController),
      () => Polymer.register('syv-teacher-list--aps', i98.TeacherListController),
      () => Polymer.register('syv-staff-form--aps', i99.StaffFormController),
      () => Polymer.register('syv-staff-list--aps', i100.StaffListController),
      () => Polymer.register('syv-evaluation-form--apst', i101.EvaluationFormController),
      () => Polymer.register('syv-lecture-form--apst', i102.LectureFormController),
      () => Polymer.register('syv-first-login--i', i103.FirstLoginController),
      () => Polymer.register('syv-teacher-lessons--t', i104.TeacherLessonsController),
      () => Polymer.register('syv-classroom--t', i105.TeacherClassroomController),
      () => Polymer.register('sye-domain-selector', i106.DomainSelector),
      () => Polymer.register('sye-domains-selector', i107.DomainsSelector),
      () => Polymer.register('paper-date-picker-dialog', i108.PaperDatePickerDialogController),
      () => Polymer.register('paper-date-picker', i109.PaperDatePickerController),
      () => Polymer.register('paper-year-picker', i110.PaperYearPickerController),
      () => Polymer.register('sye-teacher-selector', i111.TeacherSelector),
      () => Polymer.register('context-tester', i112.ContextTesterController),
      () => Polymer.register('syages-app', i113.$SyagesAppController),
    ]);
  i114.main();
}
