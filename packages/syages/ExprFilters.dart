library syages.ExprFilters;

import "syages.dart";

abstract class filters {
	static get date => () => (DateTime dt) {
    	var hour = dt.hour < 10 ? "0${dt.hour}" : dt.hour.toString();
    	var minute = dt.minute < 10 ? "0${dt.minute}" : dt.minute.toString();
    	var second = dt.second < 10 ? "0${dt.second}" : dt.second.toString();

    	return '${i18n.WEEKDAYS[dt.weekday-1]} ${dt.day} ${i18n.MONTHS[dt.month]} ${dt.year} à ${hour}:${minute}:${second}';
    };
}