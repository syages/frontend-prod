@documented("f0e21a2e")
library syages.$Cache;

import "syages.dart";

/// Cette classe permet la conservation de certaines informations en cache afin d'améliorer l'UX.
///
/// Cache est une instance unique permettant de conserver certaines informations non ou peu
/// périssables comme les informations sur l'utilisateur connecté par exemple.
class $Cache {
	/// Variable privée contenant l'unique instance du Cache
	static $Cache _sharedInstance = new $Cache._();

	/// L'utilisateur connecté, ou null si personne ne l'est
	User _user = null;

	/// Constructeur privé utilisé pour l'instantiation de l'instance unique
	$Cache._() : super()
	{
		// Lorsqu'une deconnexion à lieu, le cache est en parti vidé
		SyagesSession.sharedInstance.onLogout.listen((SyagesSession s) {
			_user = null;
		});
	}

	/// Getter statique pour récupérer l'instance unique
	static $Cache get sharedInstance => _sharedInstance;

	/// L'utilisateur connecté, ou null si personne ne l'est
	User get user => _user;
	/// Permet de mettre à jour l'utilisateur connecté dans le cache
	set user(User u) { _user = u; }
}
