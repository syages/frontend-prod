library syages.SerializableContext;

import 'syages.dart';

/// Interface pour la sérialisation des contextes
///
///
abstract class SerializableElement extends PolymerElement with ChangeNotifier  {
	@reflectable @observable SerializableElement get controller => __$controller; SerializableElement __$controller; @reflectable set controller(SerializableElement value) { __$controller = notifyPropertyChange(#controller, __$controller, value); }

	SerializableElement.created() : super.created()
	{
		controller = this;
	}

	void deserializeState(Map<String, dynamic> state);
	Map<String, dynamic> serializedState();
	bool shouldSerializeState() { return true; }
}