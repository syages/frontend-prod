library syages.$SyagesAppController;

import 'syages.dart';

@CustomTag("syages-app")
/// Le cœur de l'application (bas-niveau) et controlleur de l'élement syages-app
class $SyagesAppController extends PolymerElement with ChangeNotifier  {
	@reflectable @observable String get bubble => __$bubble; String __$bubble = ""; @reflectable set bubble(String value) { __$bubble = notifyPropertyChange(#bubble, __$bubble, value); }
	Stack<Context> _contextsHistory = new Stack<Context>(); // Historique des contextes précédants
	Stack<Context> _repushableContexts = new Stack<Context>(); // Historique des contextes suivants
	ViewController _controller = null;
	bool _ignoreHashChanges = false;
	StreamController<SyagesAppEvent> _onContextPopController, _onContextPushController, _onContextReplacementController;
	bool _userWantsNavigation = false;
	Element _view = null;

	$SyagesAppController.created() : super.created()
	{
		_onContextPopController = new StreamController<SyagesAppEvent>();
		_onContextPushController = new StreamController<SyagesAppEvent>();
		_onContextReplacementController = new StreamController<SyagesAppEvent>();
		new SyagesInstance().configure(
				[_onContextPopController.stream,
				 _onContextPushController.stream,
				 _onContextReplacementController.stream
				 ] as List<Stream<SyagesAppEvent>>);
	}

	ready() {
		SyagesInstance.shared.applicationController = this;
		SyagesInstance.shared.launch();
		window.onHashChange.listen((HashChangeEvent e) {
			if (_ignoreHashChanges) return;
			Uri u = Uri.parse(e.newUrl);
			if (u.fragment != currentContext.route) {
				SyagesInstance.shared.pushRoute(u.fragment);
			}
		});
	}

	// DOM helpers
    Node get containerNode => ($['container'] as Node);

	// Contexts managements
    Stack<Context> get historyStack => _contextsHistory;
    Stack<Context> get repushStack => _repushableContexts;
	Context get currentContext => _contextsHistory.length == 0 ? null : _contextsHistory.top;
	Stream<SyagesAppEvent> get onContextPopped => _onContextPopController.stream;
	Stream<SyagesAppEvent> get onContextPushed => _onContextPushController.stream;
	Stream<SyagesAppEvent> get onContextReplaced => _onContextReplacementController.stream;

	Context popContext() {
		var ctx = _contextsHistory.pop();
		_repushableContexts.push(ctx);
    	_onContextPopController != null ? _onContextPopController.add(new SyagesAppEvent(this, ctx)) : 0;
		_refreshContext();
		_refreshNavigationClass();
		return ctx;
	}

	void pushContext(Context ctx) {
		if (_controller != null && _controller.shouldSerializeState())
		{
			currentContext.state = _controller.serializedState();
		}
		_contextsHistory.push(ctx);
		_repushableContexts.clear();
    	_onContextPushController != null ? _onContextPushController.add(new SyagesAppEvent(this, ctx)) : 0;
    	_refreshContext();
    	_refreshNavigationClass();
    }

	void replaceContext(Context ctx) {
		_contextsHistory.replace(ctx);
		_repushableContexts.clear();
    	_onContextReplacementController != null ? _onContextReplacementController.add(new SyagesAppEvent(this, ctx)) : 0;
    	_refreshContext();
    	_refreshNavigationClass();
    }

	void repushContext() {
		var ctx = _repushableContexts.pop();
		if (_controller != null && _controller.shouldSerializeState())
		{
			currentContext.state = _controller.serializedState();
		}
		_contextsHistory.push(ctx);
    	_onContextPushController != null ? _onContextPushController.add(new SyagesAppEvent(this, ctx)) : 0;
    	_refreshContext();
    	_refreshNavigationClass();
	}

	void _refreshContext()
	{
	    while (containerNode.childNodes.isNotEmpty)
	    	containerNode.childNodes.first.remove();

	    if (currentContext == null) {
	    	window.location.href = "null";
	    	return;
	    }

	    if (currentContext.configuration.requireAuth && SyagesSession.sharedInstance.isConnected == false)
	    {
	    	pushContext(ContextFactory.loginThenPopContext());
	    	return;
	    }

	    _ignoreHashChanges = true;
	    window.history.go(window.history.length-1);
	    window.location.hash = currentContext.route;
	    _ignoreHashChanges = false;

	    view = document.createElement(currentContext.configuration.controller);
	    containerNode.append(view);
	}

	// Media queries
	void mediaChanged(Event e) {
		var detail = new JsObject.fromBrowserObject(e)['detail'];
		isPhone = detail['matches'];
		_refreshNavigationClass();
	}

	// Navigation bar status
	@reflectable @observable bool get isPhone => __$isPhone; bool __$isPhone = false; @reflectable set isPhone(bool value) { __$isPhone = notifyPropertyChange(#isPhone, __$isPhone, value); }
	@reflectable @observable String get navigationClass => __$navigationClass; String __$navigationClass = ""; @reflectable set navigationClass(String value) { __$navigationClass = notifyPropertyChange(#navigationClass, __$navigationClass, value); }
	bool get isNavigationVisible
	   => shouldNavigationDisplay && ((isPhone && userWantsNavigation) || !isPhone);

	bool get shouldNavigationDisplay => currentContext.configuration.withNavbar;

	bool get userWantsNavigation => _userWantsNavigation;
	void set userWantsNavigation(bool val) {
		_userWantsNavigation = val;
		_refreshNavigationClass();
	}
	void userToggledNavigation() { userWantsNavigation = !userWantsNavigation; }

	void _refreshNavigationClass() {
		navigationClass = isNavigationVisible ? (isPhone?"mobile navEnabled":"navEnabled") : (isPhone&&shouldNavigationDisplay ?"mobile":"");
	}

	// Current controller and view
	ViewController get controller => _controller;
	void set controller(ViewController controller) {
		_controller = controller;
		_controller.deserializeState(currentContext.state);
	}
	Element get view => _view;
	void set view(Element el) {
		_view = el;
	}
}

class SyagesAppEvent
{
	final $SyagesAppController appController;
	final Context context;

	SyagesAppEvent($SyagesAppController appController, Context context) :
		this.appController = appController,
		this.context = context;
}