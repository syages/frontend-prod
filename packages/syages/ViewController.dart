library syages.ViewController;

import 'syages.dart';

abstract class ViewController extends SerializableElement with ChangeNotifier  {
	ViewController.created() : super.created()
	{
		User.me().then((User me) {
        	loggedUser = toObservable(me);
        });
	}

	@reflectable @observable User get loggedUser => __$loggedUser; User __$loggedUser; @reflectable set loggedUser(User value) { __$loggedUser = notifyPropertyChange(#loggedUser, __$loggedUser, value); }

	attached() {
		super.attached();
		new SyagesInstance().applicationController.controller = this;
	}
}