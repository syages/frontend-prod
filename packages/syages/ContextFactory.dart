library syages.ContextFactory;

import "syages.dart";

abstract class ContextFactory {
	// Common contexts
	static Context contactDirectory() => new Context(Routes.contactDirectory, "contactDirectory");
	static Context dashboard() => new Context(Routes.dashboard, "dashboard");
	static Context login() => new Context(Routes.root, "");
	static Context loginThenPopContext() => new Context(Routes.root, "", {"onLogin": "popContext"});
	static Context loginThenRedirectTo(Context redirectCtx) => new Context(Routes.root, "", {"onLogin": "pushContext", "context": redirectCtx});
	static Context logout() => new Context(Routes.root, "", {"reason": "logout"});
	static Context newsFeed() => new Context(Routes.newsFeed, "news");
	static Context profileSettings() => new Context(Routes.profileSettings, "profile/settings");
	static Context root() => SyagesSession.sharedInstance.isConnected ? dashboard() : login();

	// System Administrators
	static Context a_bugReports() => new Context(Routes.a_bugReports, "bugReport");
	static Context a_bugReportDetail(String reportId) => new Context(Routes.a_bugReportDetail, "bugReport/$reportId", {"reportId": reportId});
	static Context aps_administrators() => new Context(Routes.aps_administrators, "administrators");
	static Context aps_administratorForm([String administratorId = "new"]) => new Context(Routes.aps_administratorForm, "administrators/$administratorId", {"administratorId": administratorId});
	static Context aps_courses() => new Context(Routes.aps_courses, "courses");
	static Context aps_courseForm([String courseId = "new"]) => new Context(Routes.aps_courseForm, "courses/$courseId", {'courseId': courseId});
	static Context aps_diplomas() => new Context(Routes.aps_diplomas, "diplomas");
	static Context aps_domains() => new Context(Routes.aps_domains, "domains");
    static Context aps_domainForm([String domainId = "new"]) => new Context(Routes.aps_domainForm, "domains/$domainId", {"domainId": domainId});
    static Context aps_interns() => new Context(Routes.aps_interns, "interns");
    static Context aps_internForm([String internId = "new"]) => new Context(Routes.aps_internForm, "interns/$internId", {"internId": internId});
	static Context aps_newsForm([String informationId = "new"]) => new Context(Routes.aps_newsForm, "news/$informationId", {"informationId": informationId});
	static Context aps_presidents() => new Context(Routes.aps_presidents, "presidents");
	static Context aps_presidentForm([String presidentId = "new"]) => new Context(Routes.aps_presidentForm, "presidents/$presidentId", {"presidentId": presidentId});
	static Context aps_staffs() => new Context(Routes.aps_staffs, "staffs");
	static Context aps_staffForm([String staffId = "new"]) => new Context(Routes.aps_staffForm, "staffs/$staffId", {"staffId": staffId});
	static Context aps_teachers() => new Context(Routes.aps_teachers, "teachers");
	static Context aps_teacherForm([String teacherId = "new"]) => new Context(Routes.aps_teacherForm, "teachers/$teacherId", {"teacherId": teacherId});
	static Context apst_evaluationForm(String lessonId, String lessonName, [String evaluationId = "new"]) => new Context(Routes.apst_evaluationForm, "evaluations/$evaluationId", {"evaluationId": evaluationId, "lessonId": lessonId, "lessonName": lessonName});
	static Context apst_lectureForm(String lessonId, String lessonName,[String lectureId = "new"]) => new Context(Routes.apst_lectureForm, "lecture/$lectureId", {"lectureId": lectureId, "lessonId": lessonId, "lessonName": lessonName});

	// President
	static Context ps_diplomas() => new Context(Routes.ps_diplomas, "diplomas");

	// Staff members

	// Teachers
	static Context t_lessons() => new Context(Routes.t_lessons, "lessons");
	static Context t_classroom(String lessonId) => new Context(Routes.t_classroom, "classroom/$lessonId", {"lessonId": lessonId});
	static Context t_lectures() => new Context(Routes.t_lectures, "lectures");

	// Interns
	static Context i_absences() => new Context(Routes.i_absences, "absences");
	static Context i_firstLogin() => new Context(Routes.i_firstLogin, "firstLogin");

	// Debug
	static Context contextTester([String field = ""]) => new Context(Routes.contextTester, "ctx/",
			{"model": {"field": field}});
}
