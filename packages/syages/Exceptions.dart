@documented("f0e21a2e")
library syages.Exceptions;

import "syages.dart";

/// Exception levée lorsqu'une requete HTTP à retournée un code 400
class BadRequestException implements Exception {
	BadRequestException();
}

/// Exception levée lorsqu'une requete HTTP à retournée un code 404
class NotFoundException implements Exception {
	NotFoundException();
}

/// Exception levée lorsqu'une requete HTTP n'a pu aboutir
class RequestFailureException implements Exception {
	RequestFailureException();
}

/// Exception levée lorsqu'une erreur est survenu dans au Runtime
class RuntimeException implements Exception {
	final String _message;

	RuntimeException(String message) :
		_message = message;

	String toString() => "Exception: $_message";
}

/// Exception levée lorsqu'une requete HTTP à retournée un code 401
class UnauthorizedException implements Exception {
	UnauthorizedException();
}

/// Exception levée lorsqu'une requete HTTP à retournée un code innatendu
class UnknownException implements Exception {
	UnknownException();
}