library syages.models.Period;

import "../syages.dart";

class Period extends ChangeNotifier {
	@reflectable @observable Diploma get diploma => __$diploma; Diploma __$diploma; @reflectable set diploma(Diploma value) { __$diploma = notifyPropertyChange(#diploma, __$diploma, value); }
	@reflectable @observable DateTime get endDate => __$endDate; DateTime __$endDate; @reflectable set endDate(DateTime value) { __$endDate = notifyPropertyChange(#endDate, __$endDate, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable int get ordre => __$ordre; int __$ordre; @reflectable set ordre(int value) { __$ordre = notifyPropertyChange(#ordre, __$ordre, value); }
	@reflectable @observable DateTime get startDate => __$startDate; DateTime __$startDate; @reflectable set startDate(DateTime value) { __$startDate = notifyPropertyChange(#startDate, __$startDate, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	Period(String id, Diploma diploma, int ordre, DateTime startDate, DateTime endDate) :
		this.__$diploma = diploma,
		this.__$endDate = endDate,
		this.__$id = id,
		this.__$ordre = ordre,
		this.__$startDate = startDate,
		this.__$version = 0;
	
	Period.empty() : super()
	{
		this.diploma = new Diploma.empty();
	}

	Period.fromMap(Map<String, dynamic> json) :
		this.__$diploma = new Diploma.fromMap(json["diploma"] as Map),
		this.__$endDate = DateTime.parse(json["endDate"]),
		this.__$id = json["id"],
		this.__$ordre = json["ordre"],
		this.__$startDate = DateTime.parse(json["startDate"]),
		this.__$version = json["version"];
	
	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"diploma" : this.diploma.toMapForUpdate(),
        	"endDate" : this.endDate.toString(),
        	"ordre" : this.ordre,
        	"startDate" : this.startDate.toString()
        } as Map<String,dynamic>;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"diploma" : this.diploma.toMapForUpdate(),
        	"endDate" : this.endDate.toString(),
        	"id" : this.id,
        	"ordre" : this.ordre,
        	"startDate" : this.startDate.toString(),
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Period #$id: $ordre trimestre du ${diploma.name}";
	
	/* Requests */
	static Future<Period> period(String id) {
    	return new Future<Period>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("periods/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Period.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<Period>> periodList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Period>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("periods?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Period> list = new List<Period>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Period.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("periods/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<List<Evaluation>> evaluations([int limit = 25, int offset = 0]) 
    {
    	return new Future<List<Evaluation>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("periods/$id/evaluations?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Evaluation> list = new List<Evaluation>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Evaluation.fromMap(_));
    				});
    				return list;
            	case 400:
            		return throw new Exception(req.response);
    			case 401:
    				throw new UnauthorizedException();
    			case 404:
    				throw new NotFoundException();
    			default:
    				throw new UnknownException();
    		}
    	});
	}
	
	Future<Period> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Period> _save() 
	{
		return new Future<Period>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("periods"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Period.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Period> _update() 
	{
		return new Future<Period>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("periods/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Period.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
