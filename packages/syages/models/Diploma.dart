library syages.models.Diploma;

import "../syages.dart";

class Diploma extends ChangeNotifier {
	
	static final List<String> STATUS = ["Active", "Inactive"];
	
	@reflectable @observable int get endYear => __$endYear; int __$endYear; @reflectable set endYear(int value) { __$endYear = notifyPropertyChange(#endYear, __$endYear, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get name => __$name; String __$name; @reflectable set name(String value) { __$name = notifyPropertyChange(#name, __$name, value); }
	@reflectable @observable int get startYear => __$startYear; int __$startYear; @reflectable set startYear(int value) { __$startYear = notifyPropertyChange(#startYear, __$startYear, value); }
	@reflectable @observable String get status => __$status; String __$status; @reflectable set status(String value) { __$status = notifyPropertyChange(#status, __$status, value); }
	@reflectable @observable Teacher get supervisor => __$supervisor; Teacher __$supervisor; @reflectable set supervisor(Teacher value) { __$supervisor = notifyPropertyChange(#supervisor, __$supervisor, value); }

	Diploma(String id, String name, String status, Teacher supervisor, int startYear, int endYear):
		this.__$endYear = endYear,
		this.__$id = id,
		this.__$name = name,
		this.__$startYear = startYear,
		this.__$status = status,
		this.__$supervisor = supervisor;

	Diploma.empty() : super()
	{
		this.supervisor = new Teacher.empty();
	}
	
	Diploma.fromMap(Map<String, dynamic> json):
		this.__$endYear = json["endYear"],
		this.__$id = json["id"],
		this.__$name = json["name"],
		this.__$startYear = json["startYear"],
		this.__$status = json["status"],
		this.__$supervisor = Teacher.createFromJson(json["supervisor"] as Map);
	
	static Diploma createFromJson(Map<String, dynamic> json){
		if(json==null){
			return new Diploma.empty();
		}
		return new Diploma.fromMap(json);
	}
	
	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"endYear" : this.endYear,
        	"name" : this.name,
        	"startYear" : this.startYear,
        	"status" : this.status,
        	"supervisor" : this.supervisor.toMapForCreation()
        } as Map<String,dynamic>;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"endYear" : this.endYear,
        	"id" : this.id,
        	"name" : this.name,
        	"startYear" : this.startYear,
        	"status" : this.status,
        	"supervisor" : this.supervisor.toMapForUpdate()
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Diploma #$id: $name - Promotion $endYear supervisée par ${supervisor.user.name}";
	
	/* Requests */
	static Future<Diploma> diploma(String id) {
    	return new Future<Diploma>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<Diploma>> diplomaList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Diploma>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Diploma> list = new List<Diploma>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Diploma.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("diplomas/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<Diploma> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Diploma> _save() 
	{
		return new Future<Diploma>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("diplomas"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Diploma> _update() 
	{
		return new Future<Diploma>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("diplomas/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
	
	Future<Diploma> changeStatus(String status) 
	{
		return new Future<Diploma>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("PATCH", SyagesInstance.shared.apiUrl("diplomas/$id/status"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString('{"status": "$status" }'));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Diploma.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<List<Intern>> internFollower([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Intern>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id/interns"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Intern> list = new List<Intern>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Intern.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<List<Lesson>> lessons() 
	{
		return new Future<List<Lesson>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id/lessons"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Lesson> list = new List<Lesson>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Lesson.fromMap(_));
					});
					return list;
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<List<Period>> periods() 
	{
		return new Future<List<Period>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("diplomas/$id/periods"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Period> list = new List<Period>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Period.fromMap(_));
					});
					return list;
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
}
