library syages.models.Information;

import "../syages.dart";

class Information extends ChangeNotifier {
	@reflectable @observable User get author => __$author; User __$author; @reflectable set author(User value) { __$author = notifyPropertyChange(#author, __$author, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get post => __$post; String __$post; @reflectable set post(String value) { __$post = notifyPropertyChange(#post, __$post, value); }
	@reflectable @observable DateTime get posted => __$posted; DateTime __$posted; @reflectable set posted(DateTime value) { __$posted = notifyPropertyChange(#posted, __$posted, value); }
	@reflectable @observable String get title => __$title; String __$title; @reflectable set title(String value) { __$title = notifyPropertyChange(#title, __$title, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }
	
	Information(String title, String post, User author) :
		this.__$author = author,
		this.__$id = null,
        this.__$post = post,
		this.__$title = title,
        this.__$version = 0;
	
	Information.empty() : super()
	{
		this.author = new User.empty();
	}

	Information.fromMap(Map<String, dynamic> json) :
		this.__$author = new User.fromMap(json["author"] as Map),
		this.__$id = json["id"],
		this.__$post = json["post"],
		this.__$posted = new DateTime.fromMillisecondsSinceEpoch(json["posted"]),
		this.__$title = json["title"],
		this.__$version = json["version"];
	
	Map<String, dynamic> toMapForCreation()
	{
    	return {
        	"post" : this.post,
        	"title" : this.title
        } as Map<String,dynamic>;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"author" : this.author.toMapForUpdate(),
        	"id" : this.id,
        	"post" : this.post,
        	"posted" : this.posted.toString(),
        	"title" : this.title,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Information #$id: “$title” posté par ${author.name} le $posted";
	
	/* Requests */
	static Future<Information> information(String id) {
    	return new Future<Information>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("informations/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Information.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}	
	
	static Future<List<Information>> informationsList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Information>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("informations?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Information> list = new List<Information>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Information.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("informations/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<Information> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Information> _save() 
	{
		return new Future<Information>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("informations"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Information.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Information> _update() 
	{
		return new Future<Information>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("informations/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			return new Information.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
