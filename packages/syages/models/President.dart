library syages.models.President;

import "../syages.dart";

class President extends ChangeNotifier {
	@reflectable @observable int get day => __$day; int __$day; @reflectable set day(int value) { __$day = notifyPropertyChange(#day, __$day, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get hour => __$hour; String __$hour; @reflectable set hour(String value) { __$hour = notifyPropertyChange(#hour, __$hour, value); }
	@reflectable @observable String get office => __$office; String __$office; @reflectable set office(String value) { __$office = notifyPropertyChange(#office, __$office, value); }
	@reflectable @observable String get number => __$number; String __$number; @reflectable set number(String value) { __$number = notifyPropertyChange(#number, __$number, value); }
	@reflectable @observable int get recurrence => __$recurrence; int __$recurrence; @reflectable set recurrence(int value) { __$recurrence = notifyPropertyChange(#recurrence, __$recurrence, value); }
	@reflectable @observable User get user => __$user; User __$user; @reflectable set user(User value) { __$user = notifyPropertyChange(#user, __$user, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	President(User user, [int day=null, String hour=null, String office=null, 
								String number=null, int recurrence=null]) :
		this.__$day = day,
		this.__$id = null,
		this.__$hour = hour,
		this.__$office = office,
		this.__$number = number,
		this.__$recurrence = recurrence,
		this.__$user = user,
		this.__$version = 0;
	
	President.empty() : super()
	{
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_PRESIDENT];
	}

	President.fromMap(Map<String, dynamic> json)
	{
		this.day = json["day"];
		this.id = json["id"];
		this.hour = json["hour"];
		this.office = json["office"];
		this.number = json["number"];
		this.recurrence = json["recurrence"];
		this.user = new User.fromMap(json["user"]);
		this.version = json["version"];
	}
	
	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
        	"user" : this.user.toMapForCreation()
        } as Map<String,dynamic>;
        
        if (this.day != null)	m.putIfAbsent("day", () => this.day);
        if (this.hour != null)	m.putIfAbsent("hour", () => this.hour);
        if (this.office != null)	m.putIfAbsent("office", () => this.office);
        if (this.number != null)	m.putIfAbsent("number", () => this.number);
        if (this.recurrence != null)	m.putIfAbsent("recurrence", () => this.recurrence);

        return m;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"day" : this.day,
        	"id" : this.id,
        	"hour" : this.hour,
        	"office" : this.office,
        	"number" : this.number,
        	"recurrence" : this.recurrence,
        	"user" : this.user.toMapForUpdate(),
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "President #${user.id}: ${user.name}, office[$office], number[$number]";
	
	static Future<President> president(String id) {
    	return new Future<President>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("presidents/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new President.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<President>> presidentList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<President>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("presidents?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<President> list = new List<President>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new President.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("presidents/${user.id}"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<President> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<President> _save() 
	{
		return new Future<President>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("presidents"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new President.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<President> _update() 
	{
		return new Future<President>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("presidents/${user.id}"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			return new President.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
