library syages.models.Teacher;

import "../syages.dart";

class Teacher extends ChangeNotifier {
	@reflectable @observable List<Domain> get domains => __$domains; List<Domain> __$domains; @reflectable set domains(List<Domain> value) { __$domains = notifyPropertyChange(#domains, __$domains, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get number => __$number; String __$number; @reflectable set number(String value) { __$number = notifyPropertyChange(#number, __$number, value); }
	@reflectable @observable String get office => __$office; String __$office; @reflectable set office(String value) { __$office = notifyPropertyChange(#office, __$office, value); }
	@reflectable @observable User get user => __$user; User __$user; @reflectable set user(User value) { __$user = notifyPropertyChange(#user, __$user, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }
	

	Teacher(String email, String firstName, String lastName, List<Domain> domains,
			[String number=null, String office=null]) :
		this.__$user = new User(email, firstName, lastName),
		this.__$id = null,
        this.__$version = 0,
        this.__$number = number,
        this.__$office = office,
        this.__$domains = domains
	{
		this.user.kind = User.KINDS[User.KIND_TEACHER];
	}
	
	Teacher.empty():super()
	{
		this.domains = new List<Domain>();
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_TEACHER];
	}

	Teacher.fromMap(Map<String, dynamic> json)
	{
		this.user = new User.fromMap(json["user"] as Map);
		this.id = json["id"];
        this.version = json["version"];
        this.number = json["number"];
        this.office = json["office"];
        this.domains = new List<Domain>();
        if (json["domains"] != null){
		    (json["domains"] as List).forEach((d){
		    	this.domains.add(new Domain.fromMap(d));
		    });
        }
        this.user.kind = User.KINDS[User.KIND_TEACHER];
	}
	
	static Teacher createFromJson(Map<String, dynamic> json){
		if(json == null){
			return new Teacher.empty();
		}
		return new Teacher.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation() {
		Map<String, dynamic> m = {
        	"user": this.user.toMapForCreation(),
             
        } as Map<String,String>;
		
        if (this.number != null)	m.putIfAbsent("number", () => this.number);
        if (this.office != null)	m.putIfAbsent("office", () => this.office);
       	if (this.domains == null) return m;
        List<String> domainsId = new List<String>();
        for (int i = 0; i < this.domains.length; i++) {
        	domainsId.add(this.domains[i].id);
       	}
        m.putIfAbsent("domains", () => domainsId);
        return m;
    }

	Map<String, dynamic> toMapForUpdate() {
		Map<String, dynamic> m = {
        	"user": this.user.toMapForUpdate(),
        	"number": this.number,
        	"office": this.office,
       		"id" : this.id,
            "version" : this.version
       } as Map<String,String>;
       if (domains.length == 0) return m;

       List<String> domainsId = new List<String>();
       for (int i = 0; i < domains.length; i++) {
           domainsId.add(domains[i].id);
       }
       m.putIfAbsent("domains", () => domainsId);
       return m;
	}

	String toString() => "Teacher #$id: ${user.name}";

	/* Requests */
	Future<Empty> delete() {
		return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
            req.open("DELETE", SyagesInstance.shared.apiUrl("teachers/${user.id}"), async: false);
            addHeadersToRequest(req, requestHeaders());
            req.send();

            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
            {
                case 204:
                	return new Empty();
            	case 400:
            		return throw new Exception(req.response);
                case 404:
                	throw new NotFoundException();
                default:
                	throw new UnknownException();
        	}
    	});
    }
	
	static Future<List<Teacher>> teacherInDomain(String id) {
    		return new Future<List<Teacher>>.microtask(() {
            	HttpRequest req = new HttpRequest();
                req.open("GET", SyagesInstance.shared.apiUrl("teachers/domains/$id"), async: false);
                addHeadersToRequest(req, requestHeaders());
                req.send();

                if (req.response == null) throw new RequestFailureException();

                switch(req.status)
                {
                    case 200:
                    	List<Map<String,dynamic>> res = JSON.decode(req.response);
                        List<Teacher> list = new List<Teacher>();
                        res.forEach( (Map<String, dynamic> _)
                        {
                        	list.add(new Teacher.fromMap(_));
                        });
                      	return list;
                    case 404:
                    	throw new NotFoundException();
                    default:
                    	throw new UnknownException();
            	}
        	});
        }

	Future<Teacher> save() {
    	if (id == null) return _save();
    	else return _update();
    }

	Future<Teacher> _save() {
        return new Future<Teacher>.microtask(() {
       		HttpRequest req = new HttpRequest();
            req.open("POST", SyagesInstance.shared.apiUrl("teachers"), async: false);
            addHeadersToRequest(req, requestHeaders());
            Map<String, dynamic> m = this.toMapForCreation();
            String data = toJsonString(m);
            warn(data);
            req.send(data);
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
        	{
        		case 201:
        			return new Teacher.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}

	static Future<Teacher> teacher(String id) {
        return new Future<Teacher>.microtask(() {
        	HttpRequest req = new HttpRequest();
            req.open("GET", SyagesInstance.shared.apiUrl("teachers/$id"), async: false);
            addHeadersToRequest(req, requestHeaders());
            req.send();
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
            {
            	case 200:
            		return new Teacher.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		return throw new Exception(req.response);
            	case 401:
            		throw new UnauthorizedException();
            	default:
            		throw new UnknownException();
            }
        });
	}

	static Future<List<Teacher>>teachersList([int limit = 25, int offset = 0]) {
		return new Future<List<Teacher>>.microtask(() {
        	HttpRequest req = new HttpRequest();
         	req.open("GET", SyagesInstance.shared.apiUrl("teachers?limit=$limit&offset=$offset"), async: false);
            addHeadersToRequest(req, requestHeaders());
            req.send();
            if (req.response == null) throw new RequestFailureException();
           	switch(req.status) {
                case 200:
                	List<Map<String, dynamic>> res = JSON.decode(req.response);
                	List<Teacher> list = new List<Teacher>();
                	res.forEach((Map<String, dynamic> _) {
                		list.add(new Teacher.fromMap(_));
                	});
                	return list;
            	case 400:
            		return throw new Exception(req.response);
               	case 401:
                	throw new UnauthorizedException();
                default:
                	throw new UnknownException();
               }
        });
	}

	Future<Teacher> _update() {
    	return new Future<Teacher>.microtask(() {
            HttpRequest req = new HttpRequest();
            req.open("PUT", SyagesInstance.shared.apiUrl("teachers/${this.user.id}"), async: false);
            addHeadersToRequest(req, requestHeaders());
            String data = toJsonString(this.toMapForUpdate());
            req.send(data);
            if (req.response == null) throw new RequestFailureException();

	     	switch(req.status)
            {
                case 200:
               		return new Teacher.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		return throw new Exception(req.response);
               	case 401:
               		throw new UnauthorizedException();
               	default:
               		throw new UnknownException();
            }
        });
     }
}
