library syages.models.Intern;

import "../syages.dart";

class Intern extends ChangeNotifier {
	@reflectable @observable String get dateOfBirth => __$dateOfBirth; String __$dateOfBirth; @reflectable set dateOfBirth(String value) { __$dateOfBirth = notifyPropertyChange(#dateOfBirth, __$dateOfBirth, value); }
	@reflectable @observable List<Diploma> get followedDiplomas => __$followedDiplomas; List<Diploma> __$followedDiplomas; @reflectable set followedDiplomas(List<Diploma> value) { __$followedDiplomas = notifyPropertyChange(#followedDiplomas, __$followedDiplomas, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable User get user => __$user; User __$user; @reflectable set user(User value) { __$user = notifyPropertyChange(#user, __$user, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	Intern(String email, String firstName, String lastName, String dateOfBirth) :
		this.__$dateOfBirth = dateOfBirth,
		this.__$followedDiplomas = new List<Diploma>(),
		this.__$id = null,
		this.__$user = new User(email, firstName, lastName),
	    this.__$version = 0
	{
		this.user.kind = User.KINDS[User.KIND_INTERN];
	}

	Intern.empty() : super()
	{
		this.followedDiplomas = new List<Diploma>();
		this.user = new User.empty();
		this.user.kind = User.KINDS[User.KIND_INTERN];
	}

	Intern.fromMap(Map<String, dynamic> json)
	{
  		this.dateOfBirth = json["dataOfBirth"];
        this.followedDiplomas = json["followerDiplomas"] as List<Diploma>;
		this.id = json["id"];
		this.user = new User.fromMap(json["user"] as Map);
		this.user.kind = User.KINDS[User.KIND_INTERN];
        this.version = json["version"];
	}

	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"dateOfBirth" : this.dateOfBirth,
        	"user" : this.user.toMapForCreation()
        } as Map<String,dynamic>;

        List<dynamic> listDiplomas = {} as List<dynamic>;
        followedDiplomas.forEach( (diploma)
        {	listDiplomas.add( diploma.toMapForCreation() );
        });
		m.putIfAbsent("followedDiplomas", () => listDiplomas);

        return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"dateOfBirth" : this.dateOfBirth,
        	"followedDiplomas" : this.followedDiplomas,
        	"id" : this.id,
        	"user" : this.user.toMapForUpdate(),
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Intern #$id: ${user.name}";

	static Future<Intern> intern(String id) {
    	return new Future<Intern>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("interns/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Intern.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}

	static Future<List<Intern>> internList([int limit = 25, int offset = 0])
	{
		return new Future<List<Intern>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("interns?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Intern> list = new List<Intern>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Intern.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}



	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("interns/${user.id}"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}

	static Future<bool> isFirst(String id)
	{
		return new Future<bool>.microtask(() {
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("interns/$id/isFirst"), async: false);
			addHeadersToRequest(req, requestHeaders());
            req.send();
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
            {
            	case 200:
            		Map<String, bool> res = JSON.decode(req.response);
            		return res["first"];
            	case 400:
                	return throw new Exception(req.response);
                case 404:
                	throw new NotFoundException();
                default:
                    throw new UnknownException();
            }
		});
	}

	Future<Intern> save()
	{
		if (id == null) return _save();
		else return _update();
	}

	static Future<bool> saveInfos(String id, String lastSchool, String professionalProjet, int lastSchoolYear, bool worked, String job)
	{
		return new Future<bool>.microtask(()
        {
    		HttpRequest req = new HttpRequest();
       		req.open("POST", SyagesInstance.shared.apiUrl("interns/$id/infos"), async: false);
       		addHeadersToRequest(req, requestHeaders());

       		Map<String, dynamic> m = {
				"job": job,
  				"lastSchool": lastSchool,
  				"lastSchoolYear":lastSchoolYear,
  				"professionalProject": professionalProjet,
  				"worked": worked
            } as Map<String,dynamic>;

        	req.send(toJsonString(m));
        	if (req.response == null) throw new RequestFailureException();

			switch(req.status)
        	{
        		case 204:
        			return true;
                case 400:
                    return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        		}
        	});
	}

	Future<Intern> _save()
	{
		return new Future<Intern>.microtask(()
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("interns"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Intern.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Intern> _update()
	{
		return new Future<Intern>.microtask(()
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("interns/${user.id}"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Intern.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
