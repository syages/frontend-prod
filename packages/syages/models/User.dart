library syages.models.User;

import "../syages.dart";

class User extends ChangeNotifier {

	static final List<String> KINDS = ["Administrator", "Intern", "President", "Staff", "Teacher", "Unknown"];
	static final int KIND_ADMINISTRATOR = 0;
	static final int KIND_INTERN = 1;
	static final int KIND_PRESIDENT = 2;
	static final int KIND_STAFF = 3;
	static final int KIND_TEACHER = 4;
	static final int KIND_UNKNOWN = 5;

	@reflectable @observable String get email => __$email; String __$email; @reflectable set email(String value) { __$email = notifyPropertyChange(#email, __$email, value); }
	@reflectable @observable String get firstName => __$firstName; String __$firstName; @reflectable set firstName(String value) { __$firstName = notifyPropertyChange(#firstName, __$firstName, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get kind => __$kind; String __$kind; @reflectable set kind(String value) { __$kind = notifyPropertyChange(#kind, __$kind, value); }
	@reflectable @observable String get lastName => __$lastName; String __$lastName; @reflectable set lastName(String value) { __$lastName = notifyPropertyChange(#lastName, __$lastName, value); }
	@reflectable @observable String get pictureUrl => __$pictureUrl; String __$pictureUrl; @reflectable set pictureUrl(String value) { __$pictureUrl = notifyPropertyChange(#pictureUrl, __$pictureUrl, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	User(String email, String firstName, String lastName) :
		this.__$email = email,
		this.__$firstName = firstName,
        this.__$id = null,
        this.__$version = 0,
        this.__$kind = "None",
        this.__$lastName = lastName,
        this.__$pictureUrl = "";
	
	User.empty();
	
	User.fromMap(Map<String, dynamic> json)
	{
        this.email = json["email"];
        this.firstName = json["firstName"];
        this.id = json["id"];
        this.kind = json["flagType"];
        this.lastName = json["lastName"];
        this.pictureUrl = SyagesInstance.shared.apiUrl("users/$id/picture");
        this.version = json["version"];
	}

	String get name => "$firstName $lastName";

	Map<String, dynamic> toMapForCreation()
	{
		return {
        	"email" : this.email,
        	"firstName" : this.firstName,
        	"lastName" : this.lastName,
		} as Map<String,dynamic>;
	}

	Map<String, dynamic> toMapForUpdate()
	{
		return {
        	"id" : this.id,
        	"email" : this.email,
        	"firstName" : this.firstName,
        	"flagType" : this.kind,
        	"lastName" : this.lastName,
        	"version" : this.version
		} as Map<String,dynamic>;
	}
        
	String toString() => "User #$id: $name ($kind)";

	/* Requests */
	static Future<User> me() {
		if ($Cache.sharedInstance.user != null) return new Future.value($Cache.sharedInstance.user);
		return new Future<User>.microtask(() {
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("me"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
            if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					$Cache.sharedInstance.user = new User.fromMap(JSON.decode(req.response));
					return me();
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	static Future<List<Lesson>> meLessons(){
    		return new Future<List<Lesson>>.microtask((){
    			HttpRequest req = new HttpRequest();
    			req.open("GET", SyagesInstance.shared.apiUrl("me/lessons"), async: false);
    			addHeadersToRequest(req, requestHeaders());
    			req.send();
    			if(req.response == null) throw new RequestFailureException();
    			
    			switch(req.status)
    			{
    				case 200:
    					List<Map<String,dynamic>> res = JSON.decode(req.response);
    					List<Lesson> list = new List<Lesson>();
    					res.forEach((Map<String, dynamic> _) {
    						list.add(new Lesson.fromMap(_));
    					});
    					return list;
    				case 400:
                		return throw new Exception(req.response);
    				case 401:
    					throw new UnauthorizedException();
    				default:
    					throw new UnknownException(); 
    			}
    		});
    	}
	
	static Future<User> user(String id) {
    	return new Future<User>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("users/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new User.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<User>> usersList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<User>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("users?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<User> list = new List<User>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new User.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	
	
}