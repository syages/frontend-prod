library syages.models;

export "Administrator.dart";
export "Course.dart";
export "DeclaredAbsence.dart";
export "Diploma.dart";
export "Domain.dart";
export "Empty.dart";
export "Evaluation.dart";
export "Information.dart";
export "Intern.dart";
export "Lecture.dart";
export "Lesson.dart";
export "Mark.dart";
export "NotedAbsence.dart";
export "Period.dart";
export "President.dart";
export "Report.dart";
export "Staff.dart";
export "Teacher.dart";
export "User.dart";
