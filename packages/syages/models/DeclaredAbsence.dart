library syages.models.DeclaredAbsence;

import "../syages.dart";

class DeclaredAbsence extends ChangeNotifier {
	
	static final List<String> REASONS = ["Death", "DrivingTestNotification", "MedicalCertificate", 
	                                        "Other", "PublicTransportProblem", "ReligiousHoliday",
	                                        "Summons ", "TheoryDrivingTestNotification" ];
	static final List<String> TIMESOFDAY = ["Afternoon", "Morning"];
	
	@reflectable @observable String get commentary => __$commentary; String __$commentary; @reflectable set commentary(String value) { __$commentary = notifyPropertyChange(#commentary, __$commentary, value); }
	@reflectable @observable DateTime get endDate => __$endDate; DateTime __$endDate; @reflectable set endDate(DateTime value) { __$endDate = notifyPropertyChange(#endDate, __$endDate, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable Intern get intern => __$intern; Intern __$intern; @reflectable set intern(Intern value) { __$intern = notifyPropertyChange(#intern, __$intern, value); }
	@reflectable @observable String get reason => __$reason; String __$reason; @reflectable set reason(String value) { __$reason = notifyPropertyChange(#reason, __$reason, value); }
	@reflectable @observable DateTime get startDate => __$startDate; DateTime __$startDate; @reflectable set startDate(DateTime value) { __$startDate = notifyPropertyChange(#startDate, __$startDate, value); }
	@reflectable @observable String get status => __$status; String __$status; @reflectable set status(String value) { __$status = notifyPropertyChange(#status, __$status, value); }
	@reflectable @observable String get timeOfDayEnd => __$timeOfDayEnd; String __$timeOfDayEnd; @reflectable set timeOfDayEnd(String value) { __$timeOfDayEnd = notifyPropertyChange(#timeOfDayEnd, __$timeOfDayEnd, value); }
	@reflectable @observable String get timeOfDayStart => __$timeOfDayStart; String __$timeOfDayStart; @reflectable set timeOfDayStart(String value) { __$timeOfDayStart = notifyPropertyChange(#timeOfDayStart, __$timeOfDayStart, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	DeclaredAbsence(DateTime startDate, String timeOfDayStart, DateTime endDate, String timeOfDayEnd, String status, String reason, Intern intern, [String commentary=null]) :
		this.__$commentary = commentary,
		this.__$endDate = endDate,
		this.__$id = null,
		this.__$intern = intern,
		this.__$reason = reason,
		this.__$startDate = startDate,
		this.__$status = status,
		this.__$timeOfDayEnd = timeOfDayEnd,
		this.__$timeOfDayStart = timeOfDayStart,
		this.__$version = 0;
	
	DeclaredAbsence.empty() : super()
	{
		this.intern = new Intern.empty();
	}

	DeclaredAbsence.fromMap(Map<String, dynamic> json) :
		this.__$commentary = json["commentary"],
		this.__$endDate = DateTime.parse(json["endDate"]),
		this.__$id = json["id"],
		this.__$intern = new Intern.fromMap(json["intern"] as Map),
		this.__$reason = json["reason"],
		this.__$status = json ["status"],
		this.__$startDate = DateTime.parse(json["startDate"]),
		this.__$timeOfDayEnd = json["timeOfDayEnd"],
		this.__$timeOfDayStart = json["timeOfDayStart"],
		this.__$version = json["version"];
	
	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
        	"endDate" : this.endDate,
        	"intern" : this.toMapForCreation(),
        	"reason" : this.reason,
        	"startDate" : this.startDate.toString(),
        	"status" : this.status,
        	"timeOfDayEnd" : this.timeOfDayEnd,
        	"timeOfDayStart" : this.timeOfDayStart
        } as Map<String,dynamic>;
        
        if (this.commentary != null)	m.putIfAbsent("commentary", () => this.commentary);
        
        return m;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"commentary" : this.commentary,
        	"endDate" : this.endDate,
        	"id" : this.id,
        	"intern" : this.toMapForUpdate(),
        	"reason" : this.reason,
        	"startDate" : this.startDate.toString(),
        	"status" : this.status,
        	"timeOfDayEnd" : this.timeOfDayEnd,
        	"timeOfDayStart" : this.timeOfDayStart,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "DeclaredAbsence #$id ($status): ${intern.user.name} du $startDate ($timeOfDayStart) au $endDate ($timeOfDayEnd)\n\tMotif: $reason";
	
	/* Requests */
	static Future<DeclaredAbsence> declaredAbsence(String id) {
    	return new Future<DeclaredAbsence>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("declaredAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<DeclaredAbsence>> declaredAbsenceList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<DeclaredAbsence>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("declaredAbsences?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<DeclaredAbsence> list = new List<DeclaredAbsence>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new DeclaredAbsence.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<DeclaredAbsence> changestatus(String newStatus) {
    	return new Future<DeclaredAbsence>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("PATCH", SyagesInstance.shared.apiUrl("declaredAbsences/$id/status"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		
    		Map<String, dynamic> m = new Map<String, dynamic>();
    		m.putIfAbsent("status", () => newStatus);
    		
    		req.send(JSON.encode(m));
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("declaredAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<DeclaredAbsence> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<DeclaredAbsence> _save() 
	{
		return new Future<DeclaredAbsence>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("declaredAbsences"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<DeclaredAbsence> _update() 
	{
		return new Future<DeclaredAbsence>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("declaredAbsences/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new DeclaredAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
