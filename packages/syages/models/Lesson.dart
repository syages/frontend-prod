library syages.models.Lesson;

import "../syages.dart";

class Lesson extends ChangeNotifier {
	@reflectable @observable double get coefficient => __$coefficient; double __$coefficient; @reflectable set coefficient(double value) { __$coefficient = notifyPropertyChange(#coefficient, __$coefficient, value); }
	@reflectable @observable Course get course => __$course; Course __$course; @reflectable set course(Course value) { __$course = notifyPropertyChange(#course, __$course, value); }
	@reflectable @observable Diploma get diploma => __$diploma; Diploma __$diploma; @reflectable set diploma(Diploma value) { __$diploma = notifyPropertyChange(#diploma, __$diploma, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable bool get optional => __$optional; bool __$optional; @reflectable set optional(bool value) { __$optional = notifyPropertyChange(#optional, __$optional, value); }
	@reflectable @observable double get passMark => __$passMark; double __$passMark; @reflectable set passMark(double value) { __$passMark = notifyPropertyChange(#passMark, __$passMark, value); }
	@reflectable @observable Teacher get teacher => __$teacher; Teacher __$teacher; @reflectable set teacher(Teacher value) { __$teacher = notifyPropertyChange(#teacher, __$teacher, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }
	
	Lesson(String id, Course course, Diploma diploma, double coefficient, bool optional,
			double passMark, [Teacher teacher=null]) :
		this.__$coefficient = coefficient,
		this.__$course = course,
		this.__$diploma = diploma,
		this.__$id = id,
		this.__$optional = optional,
		this.__$passMark = passMark,
		this.__$teacher = teacher,
		this.__$version = 0;

	Lesson.empty() : super()
	{
		this.course = new Course.empty();
		this.diploma = new Diploma.empty();
		this.teacher = new Teacher.empty();
	}
	
	Lesson.fromMap(Map<String, dynamic> json) :
		this.__$coefficient = json["coefficient"],
        this.__$course = Course.createFromJson(json["course"] as Map),
        this.__$diploma = Diploma.createFromJson(json["diploma"] as Map),
        this.__$id = json["id"],
        this.__$optional = json["optional"],
        this.__$passMark = json["passMark"],
        this.__$teacher = Teacher.createFromJson(json["teacher"] as Map),
        this.__$version = json["version"];
	
	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"coefficient" : this.coefficient,
        	"course" : this.course.toMapForCreation(),
        	"diploma" : this.diploma.toMapForCreation(),
        	"optional" : this.optional,
        	"passMark" : this.passMark
        } as Map<String,dynamic>;
        
        if (this.teacher != null)	m.putIfAbsent("teacher", () => this.teacher.toMapForCreation());
        
        return m;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"coefficient" : this.coefficient,
        	"course" : this.course.toMapForUpdate(),
        	"diploma" : this.diploma.toMapForUpdate(),
        	"id" : this.id,
        	"optional" : this.optional,
        	"passMark" : this.passMark,
        	"teacher" : this.teacher.toMapForUpdate(),
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
    	
	String toString() => "Lesson #$id: ${course.name} par ${teacher.user.name}";
	
	/* Requests */
	static Future<Lesson> lesson(String id) {
    	return new Future<Lesson>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Lesson.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<Lesson>> lessonList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Lesson>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("lessons?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Lesson> list = new List<Lesson>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Lesson.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<List<Evaluation>> evaluations([int limit = 25, int offset = 0]) 
    {
    	return new Future<List<Evaluation>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id/evaluations?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Evaluation> list = new List<Evaluation>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Evaluation.fromMap(_));
    				});
    				return list;
            	case 400:
            		return throw new Exception(req.response);
    			case 401:
    				throw new UnauthorizedException();
    			case 404:
    				throw new NotFoundException();
    			default:
    				throw new UnknownException();
    		}
    	});
	}
	
	Future<List<Intern>> interns([int limit = 25, int offset = 0]) 
    {
    	return new Future<List<Intern>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id/interns?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Intern> list = new List<Intern>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Intern.fromMap(_));
    				});
    				return list;
            	case 400:
            		return throw new Exception(req.response);
    			case 401:
    				throw new UnauthorizedException();
    			case 404:
    				throw new NotFoundException();
    			default:
    				throw new UnknownException();
    		}
    	});
	}
	
	Future<List<Lecture>> lectures([int limit = 25, int offset = 0]) 
    {
    	return new Future<List<Lecture>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lessons/$id/lectures?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<Lecture> list = new List<Lecture>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new Lecture.fromMap(_));
    				});
    				return list;
            	case 400:
            		return throw new Exception(req.response);
    			case 401:
    				throw new UnauthorizedException();
    			case 404:
    				throw new NotFoundException();
    			default:
    				throw new UnknownException();
    		}
    	});
	}

	Future<Lesson> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Lesson> _save() 
	{
		return new Future<Lesson>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("lessons"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Lesson.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Lesson> _update() 
	{
		return new Future<Lesson>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("lessons/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Lesson.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
