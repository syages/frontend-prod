library syages.models.Domain;

import "../syages.dart";

class Domain extends ChangeNotifier {
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get name => __$name; String __$name; @reflectable set name(String value) { __$name = notifyPropertyChange(#name, __$name, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	Domain(String name) :
		this.__$id = null,
        this.__$version = 0,
        this.__$name = name;

	Domain.empty() : super(){}
	
	Domain.fromMap(Map<String, dynamic> json)
	{
		this.id = json["id"];
        this.version = json["version"];
        this.name = json["name"];
	}
	
	static Domain createFromJson(Map<String, dynamic> json){
		if(json==null){
			return new Domain.empty();
		}
		return new Domain.fromMap(json);
	}

	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"name": this.name
        } as Map<String,dynamic>;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"id": this.id,
			"name": this.name,
			"version": this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Domain #$id: $name";
	
	/* Requests */
	static Future<Domain> domain(String id) 
	{
		return new Future<Domain>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("domains/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Domain.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});	
	}
	
	static Future<List<Domain>> domainList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Domain>>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("domains?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Domain> list = new List<Domain>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Domain.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("domains/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<Domain> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Domain> _save() 
	{
		return new Future<Domain>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("domains"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Domain.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Domain> _update() 
	{
		return new Future<Domain>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("PUT", SyagesInstance.shared.apiUrl("domains/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForUpdate()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Domain.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}
}
