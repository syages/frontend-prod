library syages.models.Lecture;

import "../syages.dart";

class Lecture extends ChangeNotifier {
	@reflectable @observable String get classroom => __$classroom; String __$classroom; @reflectable set classroom(String value) { __$classroom = notifyPropertyChange(#classroom, __$classroom, value); }
	@reflectable @observable DateTime get date => __$date; DateTime __$date = new DateTime.now(); @reflectable set date(DateTime value) { __$date = notifyPropertyChange(#date, __$date, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable Lesson get lesson => __$lesson; Lesson __$lesson; @reflectable set lesson(Lesson value) { __$lesson = notifyPropertyChange(#lesson, __$lesson, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }
	
	Lecture(String id, Lesson lesson, DateTime date, [String classroom=null]) :
		this.__$classroom = classroom,
		this.__$date = date,
		this.__$id = id,
		this.__$lesson = lesson,
		this.__$version = 0;

	Lecture.empty() : super()
	{
		this.lesson = new Lesson.empty();
	}
	
	Lecture.fromMap(Map<String, dynamic> json) :
		this.__$classroom = json["classroom"],
        this.__$date = DateTime.parse(json["date"]),
        this.__$id = json["id"],
        this.__$lesson = new Lesson.fromMap(json["lesson"] as Map),
        this.__$version = json["version"];
	
	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
        	"date" : this.date.toString(),
        	"lesson" : this.lesson.toMapForCreation()
        } as Map<String,dynamic>;
        
        if (this.classroom != null)	m.putIfAbsent("classroom", () => this.classroom);
        
        return m;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"classroom" : this.classroom,
        	"date" : this.date.toString(),
        	"id" : this.id,
        	"lesson" : this.lesson.toMapForUpdate(),
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Lecture #$id: ${lesson.course.name} en $classroom à $date";
	
	/* Requests */
	static Future<Lecture> lecture(String id) {
    	return new Future<Lecture>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("lectures/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Lecture.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<Lecture>> lectureList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Lecture>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("lectures?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Lecture> list = new List<Lecture>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Lecture.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<List<NotedAbsence>> addAbsences(List<Mark> marks) 
	{
    	return new Future<List<NotedAbsence>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("POST", SyagesInstance.shared.apiUrl("lectures/$id/absences"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send( toJsonString(JSON.encode(marks)) );
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<NotedAbsence> list = new List<NotedAbsence>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new NotedAbsence.fromMap(_));
                    });
                    return list;
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	Future<List<DeclaredAbsence>> absencesList([int limit = 25, int offset = 0]) 
    {
    	return new Future<List<DeclaredAbsence>>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("lectures/$id/absences?limit=$limit&offset=$offset"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
   			if (req.response == null) throw new RequestFailureException();

   			switch(req.status)
    		{
    			case 200:
    				List<Map<String,dynamic>> res = JSON.decode(req.response);
    				List<DeclaredAbsence> list = new List<DeclaredAbsence>();
    				res.forEach((Map<String, dynamic> _) {
    					list.add(new DeclaredAbsence.fromMap(_));
    				});
    				return list;
            	case 400:
            		return throw new Exception(req.response);
    			case 401:
    				throw new UnauthorizedException();
    			case 404:
    				throw new NotFoundException();
    			default:
    				throw new UnknownException();
    		}
    	});
    }

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("lectures/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<Lecture> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Lecture> _save() 
	{
		return new Future<Lecture>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("lectures"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Lecture.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Lecture> _update() 
	{
		return new Future<Lecture>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("lectures/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Lecture.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
