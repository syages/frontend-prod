library syages.models.Mark;

import "../syages.dart";

class Mark extends ChangeNotifier {
	
	static final List<String> STATUS = ["Absent", "Excuse", "Present"];
	
	@reflectable @observable Evaluation get evaluation => __$evaluation; Evaluation __$evaluation; @reflectable set evaluation(Evaluation value) { __$evaluation = notifyPropertyChange(#evaluation, __$evaluation, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable Intern get intern => __$intern; Intern __$intern; @reflectable set intern(Intern value) { __$intern = notifyPropertyChange(#intern, __$intern, value); }
	@reflectable @observable double get mark => __$mark; double __$mark; @reflectable set mark(double value) { __$mark = notifyPropertyChange(#mark, __$mark, value); }
	@reflectable @observable String get status => __$status; String __$status; @reflectable set status(String value) { __$status = notifyPropertyChange(#status, __$status, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	Mark(String id, Intern intern, String status, Evaluation evaluation, [double mark=null]):
		this.__$evaluation = evaluation,
		this.__$id = id,
		this.__$intern = intern,
		this.__$mark = mark,
		this.__$status = status,
		this.__$version = 0;
	
	Mark.empty() : super()
	{
		this.evaluation = new Evaluation.empty();
		this.intern = new Intern.empty();
	}

	Mark.fromMap(Map<String, dynamic> json):
		this.__$evaluation = new Evaluation.fromMap(json["evaluation"] as Map),
		this.__$id = json["id"],
		this.__$intern = new Intern.fromMap(json["intern"] as Map),
		this.__$mark = json["mark"],
		this.__$status = json["status"],
		this.__$version = json["version"];
	
	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"evaluation" : this.evaluation.toMapForCreation(),
        	"intern" : this.intern.toMapForCreation(),
        	"status" : this.status
        } as Map<String,dynamic>;
        
        if (this.mark != null)	m.putIfAbsent("mark", () => this.mark);
        
        return m;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"evaluation" : this.evaluation.toMapForUpdate(),
        	"id" : this.id,
        	"intern" : this.intern.toMapForUpdate(),
        	"mark" : this.mark,
        	"status" : this.status,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Mark #$id: ${intern.user.name} à eu ${mark}/${evaluation.gradingScale} ($status}";
	
	/* Requests */
	static Future<Mark> getMark(String id) {
    	return new Future<Mark>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("marks/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Mark.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<Mark>> markList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Mark>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("marks?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Mark> list = new List<Mark>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Mark.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("marks/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<Mark> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Mark> _save() 
	{
		return new Future<Mark>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("marks"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Mark.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Mark> _update() 
	{
		return new Future<Mark>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("marks/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Mark.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
