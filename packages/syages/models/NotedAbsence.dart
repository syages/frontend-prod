library syages.models.NotedAbsence;

import "../syages.dart";

class NotedAbsence extends ChangeNotifier {
	
	static final List<String> TIMEOFDAY = ["Afternoon", "Morning"];
	
	@reflectable @observable String get date => __$date; String __$date; @reflectable set date(String value) { __$date = notifyPropertyChange(#date, __$date, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable Intern get intern => __$intern; Intern __$intern; @reflectable set intern(Intern value) { __$intern = notifyPropertyChange(#intern, __$intern, value); }
	@reflectable @observable User get observerUser => __$observerUser; User __$observerUser; @reflectable set observerUser(User value) { __$observerUser = notifyPropertyChange(#observerUser, __$observerUser, value); }
	@reflectable @observable String get status => __$status; String __$status; @reflectable set status(String value) { __$status = notifyPropertyChange(#status, __$status, value); }
	@reflectable @observable String get timeOfDay => __$timeOfDay; String __$timeOfDay; @reflectable set timeOfDay(String value) { __$timeOfDay = notifyPropertyChange(#timeOfDay, __$timeOfDay, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	NotedAbsence(String id, String date, String timeOfDay,String status, Intern intern, User observerUser):
		this.__$date = date,
		this.__$id = id,
		this.__$intern = intern,
		this.__$observerUser = observerUser,
		this.__$timeOfDay = timeOfDay,
		this.__$status = status,
		this.__$version = 0;

	NotedAbsence.empty() : super()
	{
		this.intern = new Intern.empty();
		this.observerUser = new User.empty();
		
	}
	
	NotedAbsence.fromMap(Map<String, dynamic> json):
		this.__$date = json["date"],
		this.__$id = json["id"],
		this.__$intern = new Intern.fromMap(json["intern"] as Map),
		this.__$observerUser = new User.fromMap(json["observerUser"] as Map),
		this.__$status = json ["status"],
		this.__$timeOfDay = json["timeOfDay"],
		this.__$version = json["version"];
	
	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"date" : this.date,
        	"id" : this.id,
        	"intern" : this.intern.toMapForCreation(),
        	"observerUser" : this.observerUser.toMapForCreation(),
        	"status" : this.status,
        	"timeOfDay" : this.timeOfDay,
        	"version" : this.version
        } as Map<String,dynamic>;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"date" : this.date,
        	"id" : this.id,
        	"intern" : this.intern.toMapForUpdate(),
        	"observerUser" : this.observerUser.toMapForUpdate(),
        	"status" : this.status,
        	"timeOfDay" : this.timeOfDay,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "NotedAbsence #$id: ${intern.user.name} absent le $date ($status)";
	
	/* Requests */
	static Future<NotedAbsence> notedAbsence(String id) {
    	return new Future<NotedAbsence>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("notedAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new NotedAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<NotedAbsence>> notedAbsenceList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<NotedAbsence>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("notedAbsences?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<NotedAbsence> list = new List<NotedAbsence>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new NotedAbsence.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("notedAbsences/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<NotedAbsence> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<NotedAbsence> _save() 
	{
		return new Future<NotedAbsence>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("notedAbsences"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new NotedAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<NotedAbsence> _update() 
	{
		return new Future<NotedAbsence>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("notedAbsences/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new NotedAbsence.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
