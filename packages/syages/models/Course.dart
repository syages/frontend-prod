library syages.models.Course;

import "../syages.dart";

class Course extends ChangeNotifier {
	@reflectable @observable Domain get domain => __$domain; Domain __$domain; @reflectable set domain(Domain value) { __$domain = notifyPropertyChange(#domain, __$domain, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get name => __$name; String __$name; @reflectable set name(String value) { __$name = notifyPropertyChange(#name, __$name, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	Course(String name, [Domain domain=null]) :
        this.__$domain = domain,
		this.__$id = null,
		this.__$name = name,
		this.__$version = 0;

	Course.empty() : super()
	{
		this.domain = new Domain.empty();
	}
	
	Course.fromMap(Map<String, dynamic> json)
	{	
		this.domain = Domain.createFromJson(json["domain"] as Map);
		this.id = json["id"];
		this.name = json["name"];
		this.version = json["version"];
	}
	
	static Course createFromJson(Map<String, dynamic> json){
		if(json == null){
			return new Course.empty();
		}
		return new Course.fromMap(json);
	}
	
	Map<String, dynamic> toMapForCreation()
	{
		Map<String, dynamic> m = {
    		"name": this.name
        } as Map<String,dynamic>;
        
        if (this.domain != null)	m.putIfAbsent("domain", () => this.domain.toMapForCreation());
        
        return m;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"id": this.id,
			"domain": this.domain.id,
			"name": this.name,
			"version": this.version
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Course #$id: $name [$domain]";
	
	
	/* Requests */
	static Future<Course> course(String id) 
	{
		return new Future<Course>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("GET", SyagesInstance.shared.apiUrl("courses/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Course.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});	
	}
	
	static Future<List<Course>> courseList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Course>>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("courses?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Course> list = new List<Course>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Course.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("courses/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<Course> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Course> _save() 
	{
		return new Future<Course>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("courses"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Course.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Course> _update() 
	{
		return new Future<Course>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("PUT", SyagesInstance.shared.apiUrl("courses/$id"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForUpdate()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					return new Course.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}
}
