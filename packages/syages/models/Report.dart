library syages.models.Report;

import "../syages.dart";

class Report extends ChangeNotifier {
	static final int PRIORITY_COMMON = 0;
	static final int PRIORITY_NORMAL = 1;
	static final int PRIORITY_CRITICAL = 2;
	static final List<String> PRIORITIES = ["Critical", "Minor", "Normal"];

	static final int STATUS_CLOSED = 0;
	static final int STATUS_OPENED = 1;
	static final List<String> STATUSES = ["Closed", "Opened"];

	@reflectable @observable String get context => __$context; String __$context; @reflectable set context(String value) { __$context = notifyPropertyChange(#context, __$context, value); }
	@reflectable @observable String get controller => __$controller; String __$controller; @reflectable set controller(String value) { __$controller = notifyPropertyChange(#controller, __$controller, value); }
	@reflectable @observable String get commentary => __$commentary; String __$commentary; @reflectable set commentary(String value) { __$commentary = notifyPropertyChange(#commentary, __$commentary, value); }
	@reflectable @observable DateTime get createdAt => __$createdAt; DateTime __$createdAt; @reflectable set createdAt(DateTime value) { __$createdAt = notifyPropertyChange(#createdAt, __$createdAt, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable String get priority => __$priority; String __$priority; @reflectable set priority(String value) { __$priority = notifyPropertyChange(#priority, __$priority, value); }
	@reflectable @observable String get route => __$route; String __$route; @reflectable set route(String value) { __$route = notifyPropertyChange(#route, __$route, value); }
	@reflectable @observable String get status => __$status; String __$status; @reflectable set status(String value) { __$status = notifyPropertyChange(#status, __$status, value); } // true si "opened", false sinon
	@reflectable @observable String get type => __$type; String __$type; @reflectable set type(String value) { __$type = notifyPropertyChange(#type, __$type, value); }
	@reflectable @observable User get user => __$user; User __$user; @reflectable set user(User value) { __$user = notifyPropertyChange(#user, __$user, value); }
	@reflectable @observable String get userInfo => __$userInfo; String __$userInfo; @reflectable set userInfo(String value) { __$userInfo = notifyPropertyChange(#userInfo, __$userInfo, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }

	Report.bug(int priority, String commentary) :
		this.__$context = toJsonString(new SyagesInstance().applicationController.controller == null ? null : new SyagesInstance().applicationController.controller.serializedState()),
		this.__$controller = SyagesInstance.shared.applicationController.currentContext == null ? null : SyagesInstance.shared.applicationController.currentContext.configuration.controller,
		this.__$commentary = commentary,
		this.__$createdAt = null,
		this.__$id = null,
		this.__$priority = PRIORITIES[priority],
		this.__$route = window.location.hash,
		this.__$status = STATUSES[1],
		this.__$type = "Bug",
		this.__$user = $Cache.sharedInstance.user,
		this.__$userInfo = toJsonString(gatherUserConfiguration()),
		this.__$version = null;
	
	Report.empty() : super()
	{
		this.user = new User.empty();
	}

	Report.message(String message) :
    	this.__$context = toJsonString(new SyagesInstance().applicationController.controller == null ? null : new SyagesInstance().applicationController.controller.serializedState()),
    	this.__$controller = SyagesInstance.shared.applicationController.currentContext == null ? null : SyagesInstance.shared.applicationController.currentContext.configuration.controller,
    	this.__$commentary = message,
    	this.__$createdAt = null,
    	this.__$id = null,
    	this.__$priority = null,
    	this.__$route = window.location.hash,
    	this.__$status = STATUSES[1],
    	this.__$type = "Message",
    	this.__$user = $Cache.sharedInstance.user,
    	this.__$userInfo = toJsonString(gatherUserConfiguration()),
    	this.__$version = null;

	Report.fromMap(Map<String, dynamic> values)
	{
		this.context = values["context"];
		this.controller = values["controller"];
		this.commentary = values["commentary"];
		this.createdAt = values["createdAt"] == null ? null : new DateTime.fromMillisecondsSinceEpoch(values["createdAt"]);
		this.id = values["id"];
		this.priority = PRIORITIES.contains(values["priority"]) ? values["priority"] : null;
		this.route = values["route"];
		this.status = values["status"];
		this.type = values["type"];
		this.userInfo = values["userInfo"];
		this.version = null;

		dynamic user = values['user'];
		if(user is User) this.user = user;
		else if(user is Map) this.user = new User.fromMap(user);
		else if(user is String) {
			Map<String, dynamic> map = JSON.decode(user);
			this.user = new User.fromMap(map);
		} else this.user = null;
	}

	Map<String, dynamic> toMapForCreation() {
		Map<String,String> m = {
			"context": this.context,
			"controller": this.controller,
			"commentary": this.commentary,
			"route": this.route,
			"type": this.type,
			"userInfo": this.userInfo
		} as Map<String,String>;
		if (this.priority != null)
			m.putIfAbsent("priority", () => this.priority);
		return m;
	}

	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"context" : this.context,
        	"controller" : this.controller,
        	"commentary" : this.commentary,
        	"createdAt" : this.createdAt.toString(),
        	"id" : this.id,
        	"priority" : this.priority,
        	"route" : this.route,
        	"status" : this.status,
        	"type" : this.type,
        	"user" : this.user.toMapForUpdate(),
        	"userInfo" : this.userInfo,
        	"version" : this.version
    	} as Map<String,dynamic>;
    }

	String toString() => "Report #$id: $type ($status) sur $controller";

	/* Requests */
	static Future<List<Report>> closedReportsList([int limit = 25, int offset = 0]) {
    	return new Future<List<Report>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("reports/closed?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Report> list = new List<Report>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Report.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
    }

	static Future<List<Report>> openedReportsList([int limit = 25, int offset = 0]) {
    	return new Future<List<Report>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("reports/opened?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Report> list = new List<Report>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Report.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
    }

	static Future<Report> report(String id) {
        return new Future<Report>.microtask(() {
       		HttpRequest req = new HttpRequest();
       		req.open("GET", SyagesInstance.shared.apiUrl("reports/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
    		{
    			case 200:
    				return new Report.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		return throw new Exception(req.response);
    			case 401:
    				throw new UnauthorizedException();
    			default:
    				throw new UnknownException();
    		}
		});
	}

	static Future<List<Report>> reportsList([int limit = 25, int offset = 0]) {
		return new Future<List<Report>>.microtask(() {
            HttpRequest req = new HttpRequest();
            req.open("GET", SyagesInstance.shared.apiUrl("reports?limit=$limit&offset=$offset"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send();
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 200:
        			List<Map<String,dynamic>> res = JSON.decode(req.response);
        			List<Report> list = new List<Report>();
        			res.forEach((Map<String, dynamic> _) {
        				list.add(new Report.fromMap(_));
        			});
        			return list;
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
		});
	}

	Future<Report> save() {
		return new Future<Report>.microtask(() {
			HttpRequest req = new HttpRequest();
            req.open("POST", SyagesInstance.shared.apiUrl("reports"), async: false);
            addHeadersToRequest(req, requestHeaders());
            String data = toJsonString(this.toMapForCreation());
            req.send(data);
            if (req.response == null) throw new RequestFailureException();

            switch(req.status)
			{
				case 201:
					return new Report.fromMap(JSON.decode(req.response) as Map<String, dynamic>);
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}
}