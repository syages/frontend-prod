library syages.models.Evaluation;

import "../syages.dart";

class Evaluation extends ChangeNotifier {
	static final List<String> TYPE = ["Continue", "Final"];
	
	@reflectable @observable double get coef => __$coef; double __$coef; @reflectable set coef(double value) { __$coef = notifyPropertyChange(#coef, __$coef, value); }
	@reflectable @observable DateTime get date => __$date; DateTime __$date; @reflectable set date(DateTime value) { __$date = notifyPropertyChange(#date, __$date, value); }
	@reflectable @observable int get gradingScale => __$gradingScale; int __$gradingScale; @reflectable set gradingScale(int value) { __$gradingScale = notifyPropertyChange(#gradingScale, __$gradingScale, value); }
	@reflectable @observable String get id => __$id; String __$id; @reflectable set id(String value) { __$id = notifyPropertyChange(#id, __$id, value); }
	@reflectable @observable Lesson get lesson => __$lesson; Lesson __$lesson; @reflectable set lesson(Lesson value) { __$lesson = notifyPropertyChange(#lesson, __$lesson, value); }
	@reflectable @observable String get name => __$name; String __$name; @reflectable set name(String value) { __$name = notifyPropertyChange(#name, __$name, value); }
	@reflectable @observable Period get period => __$period; Period __$period; @reflectable set period(Period value) { __$period = notifyPropertyChange(#period, __$period, value); }
	@reflectable @observable String get type => __$type; String __$type; @reflectable set type(String value) { __$type = notifyPropertyChange(#type, __$type, value); }
	@reflectable @observable int get version => __$version; int __$version; @reflectable set version(int value) { __$version = notifyPropertyChange(#version, __$version, value); }
	@reflectable @observable bool get visible => __$visible; bool __$visible; @reflectable set visible(bool value) { __$visible = notifyPropertyChange(#visible, __$visible, value); }

	Evaluation(String name, DateTime date, int gradingScale, double coef, bool visible, String type, Lesson lesson)
	{
        this.coef = coef;
        this.date = date;
        this.gradingScale = gradingScale;
		this.id = null;
        this.lesson = lesson;
        this.name = name;
        this.type = type;
        this.version = 0;
        this.visible = visible;
	}

	Evaluation.empty() : super()
	{
		this.lesson = new Lesson.empty();
		this.period = new Period.empty();
	}
	
	Evaluation.fromMap(Map<String, dynamic> json)
	{
        this.coef = json["coefficient"] as double;
        this.date = DateTime.parse(json["date"]);
        this.gradingScale = json["gradingScale"];
		this.id = json["id"];
        this.lesson = new Lesson.fromMap(json["lesson"] as Map);
        this.name = json["name"];
        this.type = json["type"];
        this.version = json["version"];
        this.visible = json["visible"];
	}
	
	Map<String, dynamic> toMapForCreation()
	{
    	return {
    		"coef" : this.coef,
        	"date" : this.date,
        	"gradingScale" : this.gradingScale,
        	"id" : this.id,
        	"lesson" : this.lesson.toMapForCreation(),
        	"name" : this.name,
        	"period" : this.period.toMapForCreation(),
        	"type" : this.type,
        	"version" : this.version,
        	"visible" : this.visible
        } as Map<String,dynamic>;
	}
	
	Map<String, dynamic> toMapForUpdate()
    {
    	return {
    		"coef" : this.coef,
        	"date" : this.date,
        	"gradingScale" : this.gradingScale,
        	"id" : this.id,
        	"lesson" : this.lesson.toMapForUpdate(),
        	"name" : this.name,
        	"period" : this.period.toMapForUpdate(),
        	"type" : this.type,
        	"version" : this.version,
        	"visible" : this.visible
    	} as Map<String,dynamic>;
    }
	
	String toString() => "Evaluation #$id: $name ($type) en ${lesson.course.name}";

	/* Requests */
	static Future<Evaluation> evaluation(String id) {
    	return new Future<Evaluation>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("evaluations/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
					return new Evaluation.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	static Future<List<Evaluation>> evaluationList([int limit = 25, int offset = 0]) 
	{
		return new Future<List<Evaluation>>.microtask(() {
    		HttpRequest req = new HttpRequest();
    		req.open("GET", SyagesInstance.shared.apiUrl("evaluations?limit=$limit&offset=$offset"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send();
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 200:
					List<Map<String,dynamic>> res = JSON.decode(req.response);
					List<Evaluation> list = new List<Evaluation>();
					res.forEach((Map<String, dynamic> _) {
						list.add(new Evaluation.fromMap(_));
					});
					return list;
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				case 404:
					throw new NotFoundException();
				default:
					throw new UnknownException();
			}
		});
	}
	
	Future<List<Mark>> addMarks(List<Mark> marks) 
	{
    	return new Future<List<Mark>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("POST", SyagesInstance.shared.apiUrl("evaluations/$id/marks"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send( toJsonString(JSON.encode(marks)) );
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<Mark> list = new List<Mark>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new Mark.fromMap(_));
                    });
                    return list;
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}

	Future<Empty> delete() {
    	return new Future<Empty>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("DELETE", SyagesInstance.shared.apiUrl("evaluations/$id"), async: false);
    		addHeadersToRequest(req, requestHeaders(null));
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 204:
					return new Empty();
            	case 400:
            		return throw new Exception(req.response);
   				case 404:
   					throw new NotFoundException();
   				default:
   					throw new UnknownException();
   			}
   		});
   	}
	
	Future<List<Mark>> editMarks(List<Mark> marks) 
	{
    	return new Future<List<Mark>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("evaluations/$id/marks"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send( toJsonString(JSON.encode(marks)) );
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<Mark> list = new List<Mark>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new Mark.fromMap(_));
                    });
                    return list;
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	Future<List<Mark>> marks() {
    	return new Future<List<Mark>>.microtask(() {
        	HttpRequest req = new HttpRequest();
        	req.open("GET", SyagesInstance.shared.apiUrl("evaluations/$id/marks"), async: false);
    		addHeadersToRequest(req, requestHeaders());
    		req.send();
    		if (req.response == null) throw new RequestFailureException();

    		switch(req.status)
   			{
   				case 200:
   					List<Map<String,dynamic>> res = JSON.decode(req.response);
                    List<Mark> list = new List<Mark>();
                    res.forEach((Map<String, dynamic> _) {
                    	list.add(new Mark.fromMap(_));
                    });
                    return list;
            	case 400:
            		return throw new Exception(req.response);
   				case 401:
   					throw new UnauthorizedException();
   				default:
   					throw new UnknownException();
   			}
   		});	
   	}
	
	
	Future<Evaluation> save() 
	{
		if (id == null) return _save();
		else return _update();
	}

	Future<Evaluation> _save() 
	{
		return new Future<Evaluation>.microtask(() 
		{
			HttpRequest req = new HttpRequest();
			req.open("POST", SyagesInstance.shared.apiUrl("evaluations"), async: false);
			addHeadersToRequest(req, requestHeaders());
			req.send(toJsonString(this.toMapForCreation()));
			if (req.response == null) throw new RequestFailureException();

			switch(req.status)
			{
				case 201:
					return new Evaluation.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
				case 401:
					throw new UnauthorizedException();
				default:
					throw new UnknownException();
			}
		});
	}

	Future<Evaluation> _update() 
	{
		return new Future<Evaluation>.microtask(() 
		{
        	HttpRequest req = new HttpRequest();
        	req.open("PUT", SyagesInstance.shared.apiUrl("evaluations/$id"), async: false);
        	addHeadersToRequest(req, requestHeaders());
        	req.send(toJsonString(this.toMapForUpdate()));
        	if (req.response == null) throw new RequestFailureException();

        	switch(req.status)
        	{
        		case 201:
        			return new Evaluation.fromMap(JSON.decode(req.response));
            	case 400:
            		return throw new Exception(req.response);
        		case 401:
        			throw new UnauthorizedException();
        		default:
        			throw new UnknownException();
        	}
        });
	}
}
