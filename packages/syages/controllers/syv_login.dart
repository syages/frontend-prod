library syages.controllers.syv_login;

import '../syages.dart';

@CustomTag("syv-login")
class LoginController extends ViewController with ChangeNotifier  {
    LoginController.created() : super.created();

    attached()
    {
    	if (SyagesSession.sharedInstance.isConnected) {
            new SyagesInstance().replaceRoute("dashboard");
       	}
    }

    @reflectable @observable LoginModel get model => __$model; LoginModel __$model = new LoginModel(); @reflectable set model(LoginModel value) { __$model = notifyPropertyChange(#model, __$model, value); }
    @reflectable @observable bool get isPhone => __$isPhone; bool __$isPhone = false; @reflectable set isPhone(bool value) { __$isPhone = notifyPropertyChange(#isPhone, __$isPhone, value); }
    String _onLogin = null;

    void login() {
        if (model.email == null || model.password == null) {
        	_loginError("empty"); return;
        }
        if (model.email.isEmpty || model.password.isEmpty){
        	_loginError("empty"); return;
        }
        if (SyagesSession.sharedInstance.authenticate(model.email, model.password) == false) {
        	_loginError("wrongCredentials"); return;
        }
		if (_onLogin == "popContext") {
			new SyagesInstance().popContext();
			return;
		}
		Future<User> me = SyagesSession.sharedInstance.loggedUser;
		me.then((User me){
			if (me.kind == "Intern"){
				Future<bool> futureFirst = Intern.isFirst(me.id);
                futureFirst.then((bool first){
            		if (first)
                	{
            			SyagesInstance.shared.pushContext(ContextFactory.i_firstLogin());
                	} else {
                		new SyagesInstance().replaceRoute("dashboard");
                	}
             	});
			} else {
				new SyagesInstance().replaceRoute("dashboard");
			}
		});
    }

    void _loginError([String reason = null]) {
    	this.$["login-error-popup"].show();
    }

    void deserializeState(Map<String, dynamic> state) {
    	_onLogin = state["onLogin"];
    }
    Map<String, dynamic> serializedState() => new Map<String, dynamic>();
}

class LoginModel extends ChangeNotifier {
    @reflectable @observable String get email => __$email; String __$email; @reflectable set email(String value) { __$email = notifyPropertyChange(#email, __$email, value); }
    @reflectable @observable String get error => __$error; String __$error; @reflectable set error(String value) { __$error = notifyPropertyChange(#error, __$error, value); }
    @reflectable @observable String get password => __$password; String __$password; @reflectable set password(String value) { __$password = notifyPropertyChange(#password, __$password, value); }
}
