library syages.controllers.context_tester;

import '../syages.dart';

@CustomTag("context-tester")
class ContextTesterController extends ViewController with ChangeNotifier  {
	ContextTesterController.created() : super.created() { initVars(); }
    void initVars() {
    	this.model = new Map<String,String>();
    }

	@reflectable @observable Map<String, String> get model => __$model; Map<String, String> __$model; @reflectable set model(Map<String, String> value) { __$model = notifyPropertyChange(#model, __$model, value); }
	@reflectable @observable int get count => __$count; int __$count; @reflectable set count(int value) { __$count = notifyPropertyChange(#count, __$count, value); }

    void pop() {
    	new SyagesInstance().popContext();
    }
    void push() {
    	new SyagesInstance().pushContext(ContextFactory.contextTester());
    }
    void replace() {
    	new SyagesInstance().replaceContext(ContextFactory.contextTester());
    }

    void deserializeState(Map<String, dynamic> state) {
    	initVars();
    	if (state == null) return;
    	if (state.containsKey("model") == false) return;
    	this.model = state["model"];
    }
    Map<String, dynamic> serializedState() => {"model": this.model, "count": this.count} as Map<String, dynamic>;
}