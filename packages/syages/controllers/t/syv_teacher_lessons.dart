library syages.controllers.t.teacherLessonsController;

import '../../syages.dart';

@CustomTag("syv-teacher-lessons--t")
class TeacherLessonsController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<Lesson> get lessons => __$lessons; List<Lesson> __$lessons; @reflectable set lessons(List<Lesson> value) { __$lessons = notifyPropertyChange(#lessons, __$lessons, value); }
	
	TeacherLessonsController.created(): super.created(){
		this.lessons = null;
		load();
	}
	
	void load(){
		User.meLessons()
			.then((List<Lesson> l) {
				this.lessons = toObservable(l);
    		}).catchError((error) {
    			this.lessons = null;
    			//TODO: display error
    		});
		
	}
	
	void open(Event event){
		String lessonId = (event.currentTarget as Element).attributes["data-lesson-id"];
		SyagesInstance.shared.pushContext(ContextFactory.t_classroom(lessonId));
	}
	
	
	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}