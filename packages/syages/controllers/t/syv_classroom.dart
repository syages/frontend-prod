import '../../syages.dart';

@CustomTag('syv-classroom--t')
class TeacherClassroomController extends ViewController with ChangeNotifier {
	@reflectable @observable List<Lecture> get lectures => __$lectures; List<Lecture> __$lectures; @reflectable set lectures(List<Lecture> value) { __$lectures = notifyPropertyChange(#lectures, __$lectures, value); }
	@reflectable @observable List<Evaluation> get evaluations => __$evaluations; List<Evaluation> __$evaluations; @reflectable set evaluations(List<Evaluation> value) { __$evaluations = notifyPropertyChange(#evaluations, __$evaluations, value); }
	@reflectable @observable Lesson get lesson => __$lesson; Lesson __$lesson; @reflectable set lesson(Lesson value) { __$lesson = notifyPropertyChange(#lesson, __$lesson, value); }
	@reflectable @published String get lessonId => __$lessonId; String __$lessonId; @reflectable set lessonId(String value) { __$lessonId = notifyPropertyChange(#lessonId, __$lessonId, value); }
	
	TeacherClassroomController.created() : super.created(){
		this.lesson = toObservable(new Lesson.empty());
		this.lectures = null;
		this.evaluations = null;
	}
	
	lessonIdChanged(){
		Lesson.lesson(lessonId).then((Lesson l){
			this.lesson = toObservable(l);
			lesson.lectures().then((List<Lecture> lectures){
    			this.lectures = toObservable(lectures);
    		}).catchError((error){
    			warn(error);
    		});
    		lesson.evaluations().then((List<Evaluation> evaluations){
    			this.evaluations = toObservable(evaluations);
    		}).catchError((error){
    			warn(error);
    		});
		}).catchError((error){
			warn(error);
		});
		
	}
	
	addEval(Event event){
		SyagesInstance.shared.pushContext(ContextFactory.apst_evaluationForm(this.lessonId, this.lesson.course.name));
	}
	
	addLecture(Event event){
		SyagesInstance.shared.pushContext(ContextFactory.apst_lectureForm(this.lessonId, this.lesson.course.name));
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
	}
	@override Map<String, dynamic> serializedState() =>{};
	@override bool shouldSerializeState() => false;
}