library syages.controllers.syd_error_reporter;

import '../syages.dart';

@CustomTag("syd-error-reporter")
class ErrorReporterController extends PolymerElement with ChangeNotifier  {
	@reflectable @observable bool get deployed => __$deployed; bool __$deployed = false; @reflectable set deployed(bool value) { __$deployed = notifyPropertyChange(#deployed, __$deployed, value); }
	@reflectable @observable String get message => __$message; String __$message = ""; @reflectable set message(String value) { __$message = notifyPropertyChange(#message, __$message, value); }
	@reflectable @observable String get priority => __$priority; String __$priority = "1"; @reflectable set priority(String value) { __$priority = notifyPropertyChange(#priority, __$priority, value); }
	@reflectable @observable String get type => __$type; String __$type = "Bug"; @reflectable set type(String value) { __$type = notifyPropertyChange(#type, __$type, value); }

	ErrorReporterController.created() : super.created()
	{
		window.onResize.listen(onResizeHandler);
	}

	@override
	void attached() {
		super.attached();
		onResizeHandler(null);
	}

	void onResizeHandler(Event e) {
		var toolbarHeight = (this.$["toolbar"] as Element).clientHeight;
		var dialogHeight = (this.$["dialog"] as Element).clientHeight;
		var finalHeight = dialogHeight - toolbarHeight;
		(this.$["dialog-panel"] as Element).style.height = finalHeight.toString() + "px";
	}

	void toggle() {
		deployed = !deployed;
		if (deployed == false)
		{
        	this.message = "";
			this.priority = "1";
			this.type = "Bug";
		} else {
			onResizeHandler(null);
		}
	}

	void send() {
		if (type == "Bug") {
			new Report.bug(int.parse(this.priority), this.message).save().whenComplete(() {
				this.toggle();
			});
		} else if (type == "Message") {
			new Report.message(this.message).save().whenComplete(() {
				this.toggle();
			});
		} else {
			this.toggle();
		}
	}
}