library syages.controllers.apst.evaluationFormController;

import '../../syages.dart';

@CustomTag("syv-evaluation-form--apst")
class EvaluationFormController extends ViewController with ChangeNotifier {
	@reflectable @published String get lessonId => __$lessonId; String __$lessonId; @reflectable set lessonId(String value) { __$lessonId = notifyPropertyChange(#lessonId, __$lessonId, value); }
	@reflectable @published String get lessonName => __$lessonName; String __$lessonName; @reflectable set lessonName(String value) { __$lessonName = notifyPropertyChange(#lessonName, __$lessonName, value); }
	@reflectable @published String get evaluationId => __$evaluationId; String __$evaluationId; @reflectable set evaluationId(String value) { __$evaluationId = notifyPropertyChange(#evaluationId, __$evaluationId, value); }
	@reflectable @observable Evaluation get evaluation => __$evaluation; Evaluation __$evaluation; @reflectable set evaluation(Evaluation value) { __$evaluation = notifyPropertyChange(#evaluation, __$evaluation, value); }
	
	EvaluationFormController.created(): super.created(){
		this.evaluation = toObservable(new Evaluation.empty());
	}
	
	evaluationIdChanged(){
		Evaluation.evaluation(evaluationId).then((Evaluation e){
			this.evaluation= toObservable(e);
		}).catchError((error){
			warn(error);
		});
	}
	
	cancel(){
		SyagesInstance.shared.popContext();
	}
	
	save(){
		warn("sauvegarde evaluation");
	}

	
	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
		this.evaluationId = state["evaluationId"];
		this.lessonName = state["lessonName"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}
