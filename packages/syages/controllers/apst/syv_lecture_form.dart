library syages.controllers.apst.lectureFormController;

import '../../syages.dart';

@CustomTag("syv-lecture-form--apst")
class LectureFormController extends ViewController with ChangeNotifier {
	
	@reflectable @published String get lessonId => __$lessonId; String __$lessonId; @reflectable set lessonId(String value) { __$lessonId = notifyPropertyChange(#lessonId, __$lessonId, value); }
	@reflectable @published String get lessonName => __$lessonName; String __$lessonName; @reflectable set lessonName(String value) { __$lessonName = notifyPropertyChange(#lessonName, __$lessonName, value); }
	@reflectable @published String get lectureId => __$lectureId; String __$lectureId; @reflectable set lectureId(String value) { __$lectureId = notifyPropertyChange(#lectureId, __$lectureId, value); }
	@reflectable @observable Lecture get lecture => __$lecture; Lecture __$lecture; @reflectable set lecture(Lecture value) { __$lecture = notifyPropertyChange(#lecture, __$lecture, value); }
	
	LectureFormController.created(): super.created(){
		this.lecture = toObservable(new Lecture.empty());
	}
	
	lectureIdChanged(){
		if(lectureId != "new"){
			Lecture.lecture(lectureId).then((Lecture l){
				this.lecture = toObservable(l);
			}).catchError((error){
				warn(error);
			});
		}
	}
	
	save(){
		warn("sauvegarde cours");
	}
	
	cancel(){
		SyagesInstance.shared.popContext();
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.lessonId = state["lessonId"];
		this.lectureId = state["lectureId"];
		this.lessonName = state["lessonName"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}