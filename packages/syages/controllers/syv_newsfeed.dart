library syages.controllers.syv_newsFeedController;

import '../syages.dart';

@CustomTag("syv-newsfeed")
class NewsFeedController extends ViewController with ChangeNotifier {
	@reflectable @observable List<Information> get informations => __$informations; List<Information> __$informations; @reflectable set informations(List<Information> value) { __$informations = notifyPropertyChange(#informations, __$informations, value); }
	
	NewsFeedController.created(): super.created(){
		this.informations = null;
		this.refresh();
	}
	
	void refresh() {
		Future<List<Information>> future;
		future = Information.informationsList();
		future.then((List<Information> l) {
			this.informations = toObservable(l);
		}).catchError((error) {
			this.informations = null;
			//TODO: display error
		});
	}
	
	void create(Event event){
		SyagesInstance.shared.pushContext(ContextFactory.aps_newsForm());
	}
	
	void edit(Event event){
		String informationId = (event.target as Element).attributes["data-information-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_newsForm(informationId));
	}

	
	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return true; }
}