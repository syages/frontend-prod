library syages.controllers.syv_navbar;

import '../syages.dart';

@CustomTag("syv-navbar")
class NavbarController extends PolymerElement with ChangeNotifier  {
	@reflectable @observable bool get canGoBack => __$canGoBack; bool __$canGoBack = false; @reflectable set canGoBack(bool value) { __$canGoBack = notifyPropertyChange(#canGoBack, __$canGoBack, value); } @reflectable @observable bool get canGoForward => __$canGoForward; bool __$canGoForward = false; @reflectable set canGoForward(bool value) { __$canGoForward = notifyPropertyChange(#canGoForward, __$canGoForward, value); } @reflectable @observable bool get canGoHome => __$canGoHome; bool __$canGoHome = false; @reflectable set canGoHome(bool value) { __$canGoHome = notifyPropertyChange(#canGoHome, __$canGoHome, value); }
	@reflectable @published bool get displayed => __$displayed; bool __$displayed = false; @reflectable set displayed(bool value) { __$displayed = notifyPropertyChange(#displayed, __$displayed, value); }
	@reflectable @observable List<IAction> get navbar => __$navbar; List<IAction> __$navbar; @reflectable set navbar(List<IAction> value) { __$navbar = notifyPropertyChange(#navbar, __$navbar, value); }
	@reflectable @observable User get user => __$user; User __$user; @reflectable set user(User value) { __$user = notifyPropertyChange(#user, __$user, value); }

	NavbarController.created() : super.created()
	{
		this.navbar = new List<IAction>();

		// Context-changes Listeners
		new SyagesInstance().onContextChange.listen(_onContextChangeHandler);

		// Session Listeners
        SyagesSession.sharedInstance.onLogin.listen(_onLoginHandler);
        if (SyagesSession.sharedInstance.isConnected) _onLoginHandler(SyagesSession.sharedInstance);
        SyagesSession.sharedInstance.onLogout.listen(_onLogoutHandler);

		// Window resizing listeners
		window.onResize.listen(_onResizeHandler);
		window.onAnimationEnd.listen(_onResizeHandler);
		window.onTransitionEnd.listen(_onResizeHandler);
	}

	attached() {
		super.attached();
		_onContextChangeHandler(null);
		_onResizeHandler(null);
	}

	// Navigation
	void goBack() {
		if (this.canGoBack) SyagesInstance.shared.popContext();
	}

	void goForward() {
		if (this.canGoForward) SyagesInstance.shared.repushContext();
	}

	void goHome() {
		if (this.canGoHome) SyagesInstance.shared.pushContext(ContextFactory.dashboard());
	}

	void goToProfileSettings() {
		SyagesInstance.shared.pushContext(ContextFactory.profileSettings());
	}

	void logout() {
		SyagesSession.sharedInstance.logout();
	}

	void toggle() {
		SyagesInstance.shared.applicationController.userToggledNavigation();
	}

	// Handlers
	void _onContextChangeHandler(SyagesAppEvent ev) {
		this.canGoBack = new SyagesInstance().historyStack.length > 1;
		this.canGoForward = new SyagesInstance().repushStack.length > 0;
		this.canGoHome = SyagesInstance.shared.currentContext != null ? SyagesInstance.shared.currentContext.configuration.controller != "syv-dashboard" : true;
		// TODO: Mettre à jour le selector
	}

	void _onLoginHandler(SyagesSession session) {
		User.me().catchError(() {

		}).whenComplete(() {

		}).then((User u) {
			this.user = u;
			this.navbar = toObservable(NavBarFactory.generateForUser(this.user));
		});
    }

	void _onLogoutHandler(SyagesSession session) {
    	this.navbar.clear();
    	this.user = null;
    }

	void _onResizeHandler(Event e) {
		var bar = this.clientHeight;
		var footer = ($["footer"] as Element).clientHeight;
		var header = ($["header"] as Element).clientHeight;

		($["links"] as Element).style.height = (bar-(footer+header)).toString()+ "px";
	}
}

@CustomTag("sye-navbar-action")
class ActionController extends PolymerElement with ChangeNotifier  {

	@reflectable @published Action get model => __$model; Action __$model; @reflectable set model(Action value) { __$model = notifyPropertyChange(#model, __$model, value); }

	ActionController.created() : super.created();

	void onTap() {
		(SyagesInstance.shared.applicationController.$["navbar"] as NavbarController).toggle();
		if (this.model != null)
			this.model.onTap();
	}

}

@CustomTag("sye-navbar-group")
class ActionGroupController extends PolymerElement with ChangeNotifier  {

	CoreCollapse get collapseEl => $['collapseEl'] as CoreCollapse;
	@reflectable @published ActionGroup get model => __$model; ActionGroup __$model; @reflectable set model(ActionGroup value) { __$model = notifyPropertyChange(#model, __$model, value); }

	ActionGroupController.created() : super.created();

	void onTap() {
    	if (this.model != null)
    		this.model.onTap();
    	if (this.collapseEl != null)
    		this.collapseEl.opened = this.model.collapsed;
    }

}

/* Models */
abstract class IAction extends ChangeNotifier {
	@reflectable @observable String get label => __$label; String __$label; @reflectable set label(String value) { __$label = notifyPropertyChange(#label, __$label, value); }
	@observable final bool isGroup;

	IAction(String label, bool isGroup) :
		this.isGroup = isGroup,
		this.__$label = label;

	void onTap();

	String toString() => (isGroup ? "ActionGroup" : "Action") +": $label";
}

class Action extends IAction with ChangeNotifier  {
	@reflectable @observable Context get context => __$context; Context __$context; @reflectable set context(Context value) { __$context = notifyPropertyChange(#context, __$context, value); }

	Action(String label, Context context) :
		super(label, false),
		__$context = context;

	void onTap() {
		if (context == null) return;
		new SyagesInstance().pushContext(context);
	}
}

class ActionGroup extends IAction with ChangeNotifier  {
	@reflectable @observable List<Action> get actions => __$actions; List<Action> __$actions; @reflectable set actions(List<Action> value) { __$actions = notifyPropertyChange(#actions, __$actions, value); }
	@reflectable @observable bool get collapsed => __$collapsed; bool __$collapsed; @reflectable set collapsed(bool value) { __$collapsed = notifyPropertyChange(#collapsed, __$collapsed, value); }

	ActionGroup(String label) :
		super(label, true),
		__$actions = toObservable(new List<Action>()),
		__$collapsed = toObservable(false);

	void onTap() {
		collapsed = !collapsed;
	}
}
