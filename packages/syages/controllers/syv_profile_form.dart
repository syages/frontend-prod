library syages.controllers.aps.profileFormController;

import '../syages.dart';

@CustomTag("syv-profile-form")
class profileFormController extends ViewController with ChangeNotifier {
	@reflectable @observable User get user => __$user; User __$user; @reflectable set user(User value) { __$user = notifyPropertyChange(#user, __$user, value); }
	@reflectable @published String get userId => __$userId; String __$userId; @reflectable set userId(String value) { __$userId = notifyPropertyChange(#userId, __$userId, value); }
	
	profileFormController.created(): super.created(){
		User.me().then( (user)
		{ 
			this.user = toObservable(user);
		});
	}
	
	profileIdChanged(){
		if(this.userId != "new"){
			Future<User> future;
			future = User.user(this.userId);
			future.then((User user){
				this.user = toObservable(user);
			});
		}
	}
	
	void save(Event event)
	{		
		SyagesInstance.shared.replaceContext(ContextFactory.dashboard());
		/*	TODO : il n'y a pas de méthode Save dans User.
    	this.user.save().then((i){
    		SyagesInstance.shared.replaceContext(ContextFactory.dashboard());
    	}).catchError((error){
    		warn("Erreur save profile.");
    	});
    	*/
    }
    	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.userId = state["userId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}