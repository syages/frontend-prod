library syages.controllers.i.firstLoginController;

import '../../syages.dart';

@CustomTag("syv-first-login--i")
class FirstLoginController extends ViewController with ChangeNotifier {
	 @reflectable @observable FirstLoginModel get model => __$model; FirstLoginModel __$model = new FirstLoginModel(); @reflectable set model(FirstLoginModel value) { __$model = notifyPropertyChange(#model, __$model, value); }

	FirstLoginController.created(): super.created(){

	}

	@override void deserializeState(Map<String, dynamic> state) {

	}

	void validate() {
		Future<User> me = SyagesSession.sharedInstance.loggedUser;
        me.then((User me){
        	Future<bool> send = Intern.saveInfos(me.id, model.lastSchool, model.professionalProjet, model.lastSchoolYear, model.worked, model.job);
			send.then((bool status){
				if (status){

				}
			});

        });

	}

	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}

class FirstLoginModel extends ChangeNotifier {
	@reflectable @observable String get lastSchool => __$lastSchool; String __$lastSchool; @reflectable set lastSchool(String value) { __$lastSchool = notifyPropertyChange(#lastSchool, __$lastSchool, value); }
    @reflectable @observable String get professionalProjet => __$professionalProjet; String __$professionalProjet; @reflectable set professionalProjet(String value) { __$professionalProjet = notifyPropertyChange(#professionalProjet, __$professionalProjet, value); }
    @reflectable @observable int get lastSchoolYear => __$lastSchoolYear; int __$lastSchoolYear; @reflectable set lastSchoolYear(int value) { __$lastSchoolYear = notifyPropertyChange(#lastSchoolYear, __$lastSchoolYear, value); }
    @reflectable @observable bool get worked => __$worked; bool __$worked; @reflectable set worked(bool value) { __$worked = notifyPropertyChange(#worked, __$worked, value); }
    @reflectable @observable String get job => __$job; String __$job; @reflectable set job(String value) { __$job = notifyPropertyChange(#job, __$job, value); }
}
