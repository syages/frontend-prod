library syages.controllers.a.bugReportDetailController;

import "../../syages.dart";

@CustomTag("syv-bugreport-detail--a")
class BugReportDetailController extends ViewController with ChangeNotifier  {

	@reflectable @observable Report get report => __$report; Report __$report; @reflectable set report(Report value) { __$report = notifyPropertyChange(#report, __$report, value); }
	@reflectable @published String get reportId => __$reportId; String __$reportId; @reflectable set reportId(String value) { __$reportId = notifyPropertyChange(#reportId, __$reportId, value); }

	Function formatDate = filters.date;

	BugReportDetailController.created() : super.created()
	{
		this.report = null;
	}

	reportIdChanged() {
		Report.report(this.reportId).then((r) {
        	this.report = toObservable(r);
		});
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.reportId = state["reportId"];
	}
	@override Map<String, dynamic> serializedState() => {
		"report": this.report.toMapForUpdate()
	};
	@override bool shouldSerializeState() => false;
}
