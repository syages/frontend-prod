library syages.controllers.a.bugReportListController;

import "../../syages.dart";

@CustomTag("syv-bugreport-list--a")
class BugReportListController extends ViewController with ChangeNotifier  {
	@reflectable @observable String get filter => __$filter; String __$filter; @reflectable set filter(String value) { __$filter = notifyPropertyChange(#filter, __$filter, value); } // 0 = all, 1 = open, 2 = closed
	@reflectable @observable List<Report> get reports => __$reports; List<Report> __$reports; @reflectable set reports(List<Report> value) { __$reports = notifyPropertyChange(#reports, __$reports, value); }

	Function formatDate = filters.date;

	BugReportListController.created() : super.created()
	{
		this.filter = "0";
		this.reports = null;
		this.refresh();
	}

	void more(Event event)
	{
		String reportId = (event.target as Element).attributes["data-report-id"];
		SyagesInstance.shared.pushContext(ContextFactory.a_bugReportDetail(reportId));
		print("Affichage de bug-report-detail--a avec le report #$reportId");
	}

	void refresh() {
		Future<List<Report>> future;
		switch (int.parse(this.filter))
		{
			case 1:
				future = Report.openedReportsList();
				break;
			case 2:
				future = Report.closedReportsList();
				break;
			default:
				future = Report.reportsList();
		}

		future.then((List<Report> l) {
			this.reports = toObservable(l);
		}).catchError((error) {
			this.reports = null;
			//TODO: display error
		});
	}

	@override void deserializeState(Map<String, dynamic> state) {}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}
