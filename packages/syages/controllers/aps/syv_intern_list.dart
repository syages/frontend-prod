library syages.controllers.aps.internListController;

import '../../syages.dart';

@CustomTag("syv-intern-list--aps")
class InternListController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<Intern> get interns => __$interns; List<Intern> __$interns; @reflectable set interns(List<Intern> value) { __$interns = notifyPropertyChange(#interns, __$interns, value); }

	InternListController.created(): super.created(){
		this.interns = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Intern>> future;
		future = Intern.internList();
		future.then((List<Intern> l) {
			this.interns = toObservable(l);
		}).catchError((error) {
			this.interns = null;
		});
	}

	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_internForm());
	}

	void edit(Event event){
		String internId = (event.currentTarget as Element).attributes["data-intern-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_internForm(internId));
	}


	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}