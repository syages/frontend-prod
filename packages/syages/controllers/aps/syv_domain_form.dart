library syages.controllers.aps.domainFormController;

import '../../syages.dart';

@CustomTag("syv-domain-form--aps")
class DomainFormController extends ViewController with ChangeNotifier {
	@reflectable @observable Domain get domain => __$domain; Domain __$domain; @reflectable set domain(Domain value) { __$domain = notifyPropertyChange(#domain, __$domain, value); }
	@reflectable @published String get domainId => __$domainId; String __$domainId; @reflectable set domainId(String value) { __$domainId = notifyPropertyChange(#domainId, __$domainId, value); }
	
	DomainFormController.created(): super.created(){
		this.domain = toObservable(new Domain.empty());
	}
	
	domainIdChanged(){
		if(this.domainId != "new"){
			Future<Domain> future;
			future = Domain.domain(this.domainId);
			future.then((Domain domain){
				this.domain = toObservable(domain);
			});
		}
	}
	
	void save(Event event){		
    		this.domain.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_domains());
    		}).catchError((error){
    			warn("Erreur save domain.");
    		});
    	}
    	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.domainId = state["domainId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}