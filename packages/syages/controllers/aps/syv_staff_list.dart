library syages.controllers.aps.staffListController;

import '../../syages.dart';

@CustomTag("syv-staff-list--aps")
class StaffListController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<Staff> get staffs => __$staffs; List<Staff> __$staffs; @reflectable set staffs(List<Staff> value) { __$staffs = notifyPropertyChange(#staffs, __$staffs, value); }
	
	StaffListController.created(): super.created(){
		this.staffs = null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<Staff>> future;
		future = Staff.staffList();
		future.then((List<Staff> l) {
			this.staffs = toObservable(l);
		}).catchError((error) {
			this.staffs = null;
			//TODO: display error
		});
	}
	
	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_staffForm());
	}
	
	void edit(Event event){
		String staffId = (event.currentTarget as Element).attributes["data-staff-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_staffForm(staffId));
	}
	
	
	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}