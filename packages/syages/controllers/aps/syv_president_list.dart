library syages.controllers.aps.presidentListController;

import '../../syages.dart';

@CustomTag("syv-president-list--aps")
class PresidentListController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<President> get presidents => __$presidents; List<President> __$presidents; @reflectable set presidents(List<President> value) { __$presidents = notifyPropertyChange(#presidents, __$presidents, value); }
	
	PresidentListController.created(): super.created(){
		this.presidents = null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<President>> future;
		future = President.presidentList();
		future.then((List<President> l) {
			this.presidents = toObservable(l);
		}).catchError((error) {
			this.presidents = null;
			//TODO: display error
		});
	}
	
	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_presidentForm());
	}
	
	void edit(Event event){
		String presidentId = (event.currentTarget as Element).attributes["data-president-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_presidentForm(presidentId));
	}
	
	
	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}