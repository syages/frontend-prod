library syages.controllers.aps.teacherListController;

import '../../syages.dart';

@CustomTag("syv-teacher-list--aps")
class TeacherListController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<Teacher> get teachers => __$teachers; List<Teacher> __$teachers; @reflectable set teachers(List<Teacher> value) { __$teachers = notifyPropertyChange(#teachers, __$teachers, value); }
	
	TeacherListController.created(): super.created(){
		this.teachers = null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<Teacher>> future;
		future = Teacher.teachersList();
		future.then((List<Teacher> l) {
			this.teachers = toObservable(l);
		}).catchError((error) {
			this.teachers = null;
			//TODO: display error
		});
	}
	
	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_teacherForm());
	}
	
	void edit(Event event){
		String teacherId = (event.currentTarget as Element).attributes["data-teacher-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_teacherForm(teacherId));
	}
	
	
	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}