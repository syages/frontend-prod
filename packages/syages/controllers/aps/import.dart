library syages.controllers.aps;

export 'syv_administrator_form.dart';
export 'syv_administrator_list.dart';
export 'syv_course_list.dart';
export 'syv_diploma_list.dart';
export 'syv_domain_form.dart';
export 'syv_domain_list.dart';
export 'syv_intern_form.dart';
export "syv_intern_list.dart";
export 'syv_news_form.dart';
export 'syv_president_form.dart';
export 'syv_president_list.dart';
export 'syv_staff_form.dart';
export 'syv_staff_list.dart';
export 'syv_teacher_form.dart';
export 'syv_teacher_list.dart';