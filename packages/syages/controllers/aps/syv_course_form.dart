library syages.controllers.aps.courseFormController;

import '../../syages.dart';

@CustomTag('syv-course-form--aps')
class CourseFormController extends ViewController with ChangeNotifier {
	@reflectable @observable Course get course => __$course; Course __$course; @reflectable set course(Course value) { __$course = notifyPropertyChange(#course, __$course, value); }
	@reflectable @published String get courseId => __$courseId; String __$courseId; @reflectable set courseId(String value) { __$courseId = notifyPropertyChange(#courseId, __$courseId, value); }
	
	CourseFormController.created(): super.created(){
		this.course = toObservable(new Course.empty());
	}
	
	courseIdChanged(){
		if(this.courseId != "new"){
			Future<Course> future;
			future = Course.course(this.courseId);
			future.then((Course course){
				this.course = toObservable(course);
			});
		}
	}

	void save(Event event){		
		this.course.save().then((i){
			SyagesInstance.shared.replaceContext(ContextFactory.aps_courses());
		}).catchError((error){
			warn("Erreur save course.");
		});
	}
	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}

	@override void deserializeState(Map<String, dynamic> state) {
		this.courseId = state['courseId'];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}