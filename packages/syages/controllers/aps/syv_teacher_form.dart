library syages.controllers.aps.teacherFormController;

import '../../syages.dart';

@CustomTag("syv-teacher-form--aps")
class TeacherFormController extends ViewController with ChangeNotifier {
	@reflectable @observable Teacher get teacher => __$teacher; Teacher __$teacher; @reflectable set teacher(Teacher value) { __$teacher = notifyPropertyChange(#teacher, __$teacher, value); }
	@reflectable @published String get teacherId => __$teacherId; String __$teacherId; @reflectable set teacherId(String value) { __$teacherId = notifyPropertyChange(#teacherId, __$teacherId, value); }
	
	TeacherFormController.created(): super.created(){
		this.teacher = toObservable(new Teacher.empty());
	}
	
	teacherIdChanged(){
		if(this.teacherId != "new"){
			Future<Teacher> future;
			future = Teacher.teacher(this.teacherId);
			future.then((Teacher teacher){
				this.teacher = toObservable(teacher);
			});
		}
	}
	
	void save(Event event){		
    		this.teacher.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_teachers());
    		}).catchError((error){
    			warn("Erreur save teacher.");
    		});
    	}
    	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.teacherId = state["teacherId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}