library syages.controllers.aps.domainListController;

import '../../syages.dart';

@CustomTag("syv-domain-list--aps")
class DomainListController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<Domain> get domains => __$domains; List<Domain> __$domains; @reflectable set domains(List<Domain> value) { __$domains = notifyPropertyChange(#domains, __$domains, value); }
	
	DomainListController.created(): super.created(){
		this.domains = null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<Domain>> future;
		future = Domain.domainList();
		future.then((List<Domain> l) {
			this.domains = toObservable(l);
		}).catchError((error) {
			this.domains = null;
			//TODO: display error
		});
	}
	
	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_domainForm());
	}
	
	void edit(Event event){
		String domainId = (event.currentTarget as Element).attributes["data-domain-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_domainForm(domainId));
	}
	
	
	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}