library syages.controllers.aps.presidentFormController;

import '../../syages.dart';

@CustomTag("syv-president-form--aps")
class PresidentFormController extends ViewController with ChangeNotifier {
	@reflectable @observable President get president => __$president; President __$president; @reflectable set president(President value) { __$president = notifyPropertyChange(#president, __$president, value); }
	@reflectable @published String get presidentId => __$presidentId; String __$presidentId; @reflectable set presidentId(String value) { __$presidentId = notifyPropertyChange(#presidentId, __$presidentId, value); }
	
	PresidentFormController.created(): super.created(){
		this.president = toObservable(new President.empty());
	}
	
	presidentIdChanged(){
		if(this.presidentId != "new"){
			Future<President> future;
			future = President.president(this.presidentId);
			future.then((President president){
				this.president = toObservable(president);
			});
		}
	}
	
	void save(Event event){		
    		this.president.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_presidents());
    		}).catchError((error){
    			warn("Erreur save president.");
    		});
    	}
    	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.presidentId = state["presidentId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}