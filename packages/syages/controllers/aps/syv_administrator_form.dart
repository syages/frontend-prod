library syages.controllers.aps.administratorFormController;

import '../../syages.dart';

@CustomTag("syv-administrator-form--aps")
class AdministratorFormController extends ViewController with ChangeNotifier {
	@reflectable @observable Administrator get administrator => __$administrator; Administrator __$administrator; @reflectable set administrator(Administrator value) { __$administrator = notifyPropertyChange(#administrator, __$administrator, value); }
	@reflectable @published String get administratorId => __$administratorId; String __$administratorId; @reflectable set administratorId(String value) { __$administratorId = notifyPropertyChange(#administratorId, __$administratorId, value); }
	
	AdministratorFormController.created(): super.created(){
		this.administrator = toObservable(new Administrator.empty());
	}
	
	administratorIdChanged(){
		if(this.administratorId != "new"){
			Future<Administrator> future;
			future = Administrator.administrator(this.administratorId);
			future.then((Administrator administrator){
				this.administrator = toObservable(administrator);
			});
		}
	}
	
	void save(Event event){		
    		this.administrator.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_administrators());
    		}).catchError((error){
    			warn("Erreur save administrator.");
    		});
    	}
    	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.administratorId = state["administratorId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}