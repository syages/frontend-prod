library syages.controllers.aps.administratorListController;

import '../../syages.dart';

@CustomTag("syv-administrator-list--aps")
class AdministratorListController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<Administrator> get administrators => __$administrators; List<Administrator> __$administrators; @reflectable set administrators(List<Administrator> value) { __$administrators = notifyPropertyChange(#administrators, __$administrators, value); }
	
	AdministratorListController.created(): super.created(){
		this.administrators = null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<Administrator>> future;
		future = Administrator.administratorList();
		future.then((List<Administrator> l) {
			this.administrators = toObservable(l);
		}).catchError((error) {
			this.administrators = null;
			//TODO: display error
		});
	}
	
	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_administratorForm());
	}
	
	void edit(Event event){
		String administratorId = (event.currentTarget as Element).attributes["data-administrator-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_administratorForm(administratorId));
	}
	
	
	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}