library syages.controllers.aps.newsFormController;

import '../../syages.dart';

@CustomTag('syv-news-form--aps')
class NewsFormController extends ViewController with ChangeNotifier {
	@reflectable @observable Information get information => __$information; Information __$information; @reflectable set information(Information value) { __$information = notifyPropertyChange(#information, __$information, value); }
	@reflectable @published String get informationId => __$informationId; String __$informationId; @reflectable set informationId(String value) { __$informationId = notifyPropertyChange(#informationId, __$informationId, value); }
	@reflectable @observable bool get edit => __$edit; bool __$edit; @reflectable set edit(bool value) { __$edit = notifyPropertyChange(#edit, __$edit, value); }
	NewsFormController.created(): super.created(){
		this.information = toObservable(new Information.empty());
	}
	
	informationIdChanged() {
		if(this.informationId != "new"){
			this.edit = true;
			Information.information(this.informationId).then((i) {
            			this.information = toObservable(i);
			});
		}
		else{
			this.information = toObservable(new Information.empty());
		}
	}
	
	void save(Event event){
		this.information.save().then((i){
			SyagesInstance.shared.replaceContext(ContextFactory.newsFeed());
		}).catchError((error){
			warn("Erreur save news.");
		});
		
	}
	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}
	
	
	void deserializeState(Map<String, dynamic> state) {
		this.informationId = state["informationId"];
	}
    Map<String, dynamic> serializedState() => {};
    bool shouldSerializeState() { return true; }
}
