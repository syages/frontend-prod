library syages.controllers.aps.courseListController;

import '../../syages.dart';

@CustomTag('syv-course-list--aps')
class CourseListController extends ViewController with ChangeNotifier {
	@reflectable @observable List<Course> get courses => __$courses; List<Course> __$courses; @reflectable set courses(List<Course> value) { __$courses = notifyPropertyChange(#courses, __$courses, value); }
	
	CourseListController.created():super.created(){
		this.courses = null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<Course>> future;
		future = Course.courseList();
		future.then((List<Course> c){
			this.courses = toObservable(c);
		});
	}
	
	void create(){
		SyagesInstance.shared.pushContext(ContextFactory.aps_courseForm());
	}
    	
	void edit(Event event){
		String courseId = (event.currentTarget as Element).attributes["data-course-id"];
		SyagesInstance.shared.pushContext(ContextFactory.aps_courseForm(courseId));
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
	}
	@override Map<String, dynamic> serializedState() => {
	};
	@override bool shouldSerializeState() => false;
}