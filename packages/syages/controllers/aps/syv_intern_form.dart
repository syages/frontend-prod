library syages.controllers.aps.internFormController;

import '../../syages.dart';

@CustomTag("syv-intern-form--aps")
class InternFormController extends ViewController with ChangeNotifier {
	@reflectable @observable Intern get intern => __$intern; Intern __$intern; @reflectable set intern(Intern value) { __$intern = notifyPropertyChange(#intern, __$intern, value); }
	@reflectable @published String get internId => __$internId; String __$internId; @reflectable set internId(String value) { __$internId = notifyPropertyChange(#internId, __$internId, value); }

	InternFormController.created(): super.created(){
    		this.intern = toObservable(new Intern.empty());
    	}

	@override void deserializeState(Map<String, dynamic> state) {
    		this.internId = state["internId"];
    }
   	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
   	@override bool shouldSerializeState() => false;
}
