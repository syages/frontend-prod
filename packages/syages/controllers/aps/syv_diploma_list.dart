library syages.controllers.aps.diplomaListController;

import '../../syages.dart';

@CustomTag("syv-diploma-list--aps")
class DiplomaListController extends ViewController with ChangeNotifier  {
	@reflectable @observable List<Diploma> get diplomas => __$diplomas; List<Diploma> __$diplomas; @reflectable set diplomas(List<Diploma> value) { __$diplomas = notifyPropertyChange(#diplomas, __$diplomas, value); }

	DiplomaListController.created(): super.created(){
		this.diplomas = null;
		this.refresh();
	}

	void refresh(){
		Future<List<Diploma>> future;
		future = Diploma.diplomaList();
		future.then((List<Diploma> d) {
			this.diplomas = toObservable(d);
		}).catchError((error) {
			this.diplomas = null;
			//TODO: display error
		});
	}

	void create(){

	}

	void edit(Event event){

	}

	@override void deserializeState(Map<String, dynamic> state) {}
    @override Map<String, dynamic> serializedState() => {};
    @override bool shouldSerializeState() { return false; }
}