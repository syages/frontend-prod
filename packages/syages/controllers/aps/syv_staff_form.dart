library syages.controllers.aps.staffFormController;

import '../../syages.dart';

@CustomTag("syv-staff-form--aps")
class StaffFormController extends ViewController with ChangeNotifier {
	@reflectable @observable Staff get staff => __$staff; Staff __$staff; @reflectable set staff(Staff value) { __$staff = notifyPropertyChange(#staff, __$staff, value); }
	@reflectable @published String get staffId => __$staffId; String __$staffId; @reflectable set staffId(String value) { __$staffId = notifyPropertyChange(#staffId, __$staffId, value); }
	
	StaffFormController.created(): super.created(){
		this.staff = toObservable(new Staff.empty());
	}
	
	staffIdChanged(){
		if(this.staffId != "new"){
			Future<Staff> future;
			future = Staff.staff(this.staffId);
			future.then((Staff staff){
				this.staff = toObservable(staff);
			});
		}
	}
	
	void save(Event event){		
    		this.staff.save().then((i){
    			SyagesInstance.shared.replaceContext(ContextFactory.aps_staffs());
    		}).catchError((error){
    			warn("Erreur save staff.");
    		});
    	}
    	
	void cancel(Event event){
		SyagesInstance.shared.popContext();
	}
	
	@override void deserializeState(Map<String, dynamic> state) {
		this.staffId = state["staffId"];
	}
	@override Map<String, dynamic> serializedState() => new Map<String, dynamic>();
	@override bool shouldSerializeState() => false;
}