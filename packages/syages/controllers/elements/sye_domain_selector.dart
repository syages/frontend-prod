library syages.controllers.elements.domainSelector;

import '../../syages.dart';

@CustomTag("sye-domain-selector")
class DomainSelector extends PolymerElement with ChangeNotifier {
	@reflectable @observable List<Domain> get domains => __$domains; List<Domain> __$domains; @reflectable set domains(List<Domain> value) { __$domains = notifyPropertyChange(#domains, __$domains, value); }
	@reflectable @observable dynamic get selected => __$selected; dynamic __$selected = -1; @reflectable set selected(dynamic value) { __$selected = notifyPropertyChange(#selected, __$selected, value); }
	
	@published
	Domain get value => readValue(#value);
    set value(Domain val) => writeValue(#value, val);
	
	DomainSelector.created(): super.created(){
		this.domains= null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<Domain>> future;
		future = Domain.domainList();
		future.then((List<Domain> domains){
			this.domains = toObservable(domains);
		}).catchError((error){
			this.domains = null;
		});
	}
	
	valueChanged(){
		if(this.domains!= null){
			this.selected = this.domains.indexOf(this.domains.singleWhere((i) => i.id == this.value.id));
			warn(this.selected);
		}
	}
	
	selectedChanged(){
		this.value = this.domains.elementAt(this.selected);
	}
}