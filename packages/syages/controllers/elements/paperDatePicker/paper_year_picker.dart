library syages.controllers.elements.paperDatePicker.paperYearPickerController;

import '../../../syages.dart';

@CustomTag('paper-year-picker')
class PaperYearPickerController extends PolymerElement with ChangeNotifier {
	/**
	* The currently selected year
	*
	* @attribute year
	* @type integer
	* @default undefined
	*/
	@reflectable @published int get year => __$year; int __$year; @reflectable set year(int value) { __$year = notifyPropertyChange(#year, __$year, value); }
	
	/**
	* Lowest selectable date (inclusive)
	*
	* @attribute min
	* @type Date
	* @default 900
	*/
	@reflectable @published DateTime get min => __$min; DateTime __$min = new DateTime(1900); @reflectable set min(DateTime value) { __$min = notifyPropertyChange(#min, __$min, value); }
	
	/**
	* Highest selectable date (inclusive)
	*
	* @attribute max
	* @type Date
	* @default 2100
	*/
	@reflectable @published DateTime get max => __$max; DateTime __$max = new DateTime(2100); @reflectable set max(DateTime value) { __$max = notifyPropertyChange(#max, __$max, value); }
	
	@reflectable @observable List<DateTime> get years => __$years; List<DateTime> __$years; @reflectable set years(List<DateTime> value) { __$years = notifyPropertyChange(#years, __$years, value); }
	@reflectable @observable int get yearElementHeight => __$yearElementHeight; int __$yearElementHeight = 62; @reflectable set yearElementHeight(int value) { __$yearElementHeight = notifyPropertyChange(#yearElementHeight, __$yearElementHeight, value); }
	
	@reflectable @observable String get selected => __$selected; String __$selected = 'selected'; @reflectable set selected(String value) { __$selected = notifyPropertyChange(#selected, __$selected, value); }
	
	PaperYearPickerController.created(): super.created();
	
	
	setYearArray(){
		this.years = [];
		for (var i = this.min.year; i <= this.max.year; i++) {
			this.years.add(new DateTime(i));
		}
	}
	
	refreshScrollPosition(){
		var that = this;
		new Timer(new Duration(seconds: 1), () =>
			that.$["container"].scrollTop = ((that.year - that.min.year+0.5) * that.yearElementHeight - that.$["container"].clientHeight/2 ).round());
	}
	
	yearChanged(){
		this.setYearArray();
		this.refreshScrollPosition();
	}
	
	yearSelected(Event e){
		this.year = int.parse((e.currentTarget as Element).attributes['data-year']);
	}
}