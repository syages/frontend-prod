library syages.controllers.elements.paperDatePicker.paperDatePickerDialogController;

import '../../../syages.dart';

@CustomTag('paper-date-picker-dialog')
class PaperDatePickerDialogController extends PolymerElement with ChangeNotifier {

	/**
	* `value`, the selected date.
	*
	* @attribute value
	* @type String
	* @default undefined
	*/
	@reflectable @published DateTime get value => __$value; DateTime __$value; @reflectable set value(DateTime value) { __$value = notifyPropertyChange(#value, __$value, value); }
	
	/**
	* immediateDate, the currently selected
	* date. Even before hitting 'ok'.
	*
	* @attribute immediateDate
	* @type Date
	* @default today
	*/
	@reflectable @observable DateTime get immediateDate => __$immediateDate; DateTime __$immediateDate; @reflectable set immediateDate(DateTime value) { __$immediateDate = notifyPropertyChange(#immediateDate, __$immediateDate, value); }
	
	/**
	* Lowest selectable date (inclusive)
	*
	* @attribute min
	* @type Date
	* @default 1/1/1900
	*/
	@reflectable @observable DateTime get min => __$min; DateTime __$min = new DateTime(1900,0,1); @reflectable set min(DateTime value) { __$min = notifyPropertyChange(#min, __$min, value); }
	
	/**
	* Highest selectable date (inclusive)
	*
	* @attribute max
	* @type Date
	* @default 31/12/2100
	*/
	@reflectable @observable DateTime get max => __$max; DateTime __$max = new DateTime(2100,11,31); @reflectable set max(DateTime value) { __$max = notifyPropertyChange(#max, __$max, value); }
	
	/**
	* Show only a single year at a time
	* `'auto'` will cause this to be
	* `false` for (most) mobile devices
	* and `true` for other devices.
	*
	* @attribute infiniteScrolling
	* @type String or boolean
	* @default 'auto'
	*/
	@reflectable @observable String get infiniteScrolling => __$infiniteScrolling; String __$infiniteScrolling = 'auto'; @reflectable set infiniteScrolling(String value) { __$infiniteScrolling = notifyPropertyChange(#infiniteScrolling, __$infiniteScrolling, value); }
	
	/**
	* Start day of week, expressed as 0-6
	*
	* @attribute startDayOfWeek
	* @type Integer
	* @default 1
	*
	*/
	@reflectable @observable int get startDayOfWeek => __$startDayOfWeek; int __$startDayOfWeek = 1; @reflectable set startDayOfWeek(int value) { __$startDayOfWeek = notifyPropertyChange(#startDayOfWeek, __$startDayOfWeek, value); }
	
	@reflectable @observable String get dayOfWeek => __$dayOfWeek; String __$dayOfWeek =  ''; @reflectable set dayOfWeek(String value) { __$dayOfWeek = notifyPropertyChange(#dayOfWeek, __$dayOfWeek, value); }
	@reflectable @observable int get dayOfMonth => __$dayOfMonth; int __$dayOfMonth = 0; @reflectable set dayOfMonth(int value) { __$dayOfMonth = notifyPropertyChange(#dayOfMonth, __$dayOfMonth, value); }
	@reflectable @observable String get monthShortName => __$monthShortName; String __$monthShortName = ''; @reflectable set monthShortName(String value) { __$monthShortName = notifyPropertyChange(#monthShortName, __$monthShortName, value); }
	@reflectable @observable String get formattedYear => __$formattedYear; String __$formattedYear = ''; @reflectable set formattedYear(String value) { __$formattedYear = notifyPropertyChange(#formattedYear, __$formattedYear, value); }
	@reflectable @observable int get year => __$year; int __$year = 0; @reflectable set year(int value) { __$year = notifyPropertyChange(#year, __$year, value); }
	@reflectable @observable int get page => __$page; int __$page = 0; @reflectable set page(int value) { __$page = notifyPropertyChange(#page, __$page, value); }
	
	PaperDatePickerDialogController.created(): super.created();
	
	readied(){
		this.immediateDate = this.value;
		this.immediateDateChanged();
	}
	
	
	immediateDateChanged(){
		this.dayOfWeek = i18n.WEEKDAYS[this.immediateDate.weekday];
		this.dayOfMonth = this.immediateDate.day;
		this.monthShortName = i18n.MONTHS[this.immediateDate.month].substring(0, 3);
		this.formattedYear = this.immediateDate.year.toString();
	
		this.year = this.immediateDate.year;
		this.fire('selection-changed');
	}
	
	open() {
		this.$["dialog"].open();
	}
	
	dialogOpened(){
		this.$['datePicker'].refreshScrollPosition();
		this.$['yearPicker'].refreshScrollPosition();
	}
	
	yearChanged(){
		this.immediateDate = new DateTime(this.year, this.immediateDate.month, this.immediateDate.day);
		DateTime newDate = this.immediateDate;
		if(newDate.isBefore(this.min)){
			this.immediateDate = DateTime.parse(this.min.toString());
		}else if(newDate.isAfter(this.max)){
			this.immediateDate = DateTime.parse(this.max.toString());
		}else{
			this.immediateDate = DateTime.parse(newDate.toString());
		}
		this.page = 0;
	}
	
	showYearPicker() {
		this.page = 1;
	}
	
	showDatePicker() {
		this.page = 0;
	}
	
	setDate(){
		if(this.immediateDate.isBefore(this.max) && this.immediateDate.isAfter(this.min)){
			this.value = this.immediateDate;
			this.fire('value-changed');
		}
	}
	
	pageChanged() {
		if (this.page == 0) {
			this.$["day"].classList.add("selected");
			this.$["month"].classList.add("selected");
			this.$["year"].classList.remove("selected");
		} else {
			this.$["day"].classList.remove("selected");
			this.$["month"].classList.remove("selected");
			this.$["year"].classList.add("selected");
		}
		this.$["datePicker"].refreshScrollPosition();
		this.$["yearPicker"].refreshScrollPosition();
	}
}