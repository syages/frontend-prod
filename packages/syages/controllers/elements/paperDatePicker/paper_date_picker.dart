library syages.controllers.elements.paperDatePicker.paperDatePickerController;

import '../../../syages.dart';

@CustomTag('paper-date-picker')
class PaperDatePickerController extends PolymerElement with ChangeNotifier {
	PaperDatePickerController.created(): super.created();

	/**
	* Current date
	*
	* @attribute date
	* @type Date
	* @default 'today\'s date'
	*/
	@reflectable @observable DateTime get date => __$date; DateTime __$date = new DateTime.now(); @reflectable set date(DateTime value) { __$date = notifyPropertyChange(#date, __$date, value); }
	
	/**
	* Lowest selectable date (inclusive)
	*
	* @attribute min
	* @type Date
	* @default 1/1/1900
	*/
	@reflectable @observable DateTime get min => __$min; DateTime __$min = new DateTime(1900,0,1); @reflectable set min(DateTime value) { __$min = notifyPropertyChange(#min, __$min, value); }
	
	/**
	* Highest selectable date (inclusive)
	*
	* @attribute max
	* @type Date
	* @default 31/12/2100
	*/
	@reflectable @observable DateTime get max => __$max; DateTime __$max = new DateTime(2100,11,31); @reflectable set max(DateTime value) { __$max = notifyPropertyChange(#max, __$max, value); }
	
	/**
	* Show only a single year at a time
	* `'auto'` will cause this to be
	* `false` for (most) mobile devices
	* and `true` for other devices.
	*
	* @attribute infiniteScrolling
	* @type String or boolean
	* @default 'auto'
	*/
	@reflectable @observable String get infiniteScrolling => __$infiniteScrolling; String __$infiniteScrolling = 'auto'; @reflectable set infiniteScrolling(String value) { __$infiniteScrolling = notifyPropertyChange(#infiniteScrolling, __$infiniteScrolling, value); }
	
	/**
	* Start day of week, expressed as 0-6
	* 
	* @attribute startDayOfWeek
	* @type Integer
	* @default 1
	* 
	*/
	@reflectable @observable int get startDayOfWeek => __$startDayOfWeek; int __$startDayOfWeek = 1; @reflectable set startDayOfWeek(int value) { __$startDayOfWeek = notifyPropertyChange(#startDayOfWeek, __$startDayOfWeek, value); }
	
	int currentYear = 0;
	int currentMonth = 0;
	int currentDay = 0;
	
	int loadedRange = 14;
	int triggerRange = 1;
	int loadPerTrigger = 7;
	
	List weekDayNames = [];
	
	List months = [];
	
	bool nonNumericDates = false;
	
	cleanMonthDateArrayObject(dynamic obj){
		var m = obj[0] % 12;
		return [m < 0 ? m + 12: m, obj[1] + (obj[0]/12).round()];
	}
	
//	renderMonths(dynamic start, dynamic end){
//		var st = cleanMonthDateArrayObject(start);
//		var en = cleanMonthDateArrayObject(end);
//	
//		var curMonth = st[0];
//		var curYear = st[1];
//		var date, start_, end_;
//	
//		this.months = [];
//	
//		while(curYear < end[1] || (curYear == end[1] && curMonth <= end[0])){
//	
//			var month = {
//				days: [],
//				name: "",
//				number: curMonth,
//				year: curYear
//			};
//	
//			date = new Date(curYear, curMonth-1, 1);
//			startPoint = (date.getDay() - this.startDayOfWeek + 7) % 7;
//			month.name = Intl.DateTimeFormat(this.locale, {month: 'long', year: 'numeric' }).format(date);
//			date = new Date(curYear, curMonth, 0);
//			endPoint = date.getDate();      
//	
//			for(i=0;i<startPoint;i++){
//				month.days.push(undefined);
//			}
//	
//			for(i=1;i<=endPoint;i++){
//				var thisDate = new Date(curYear, curMonth-1, i);
//				month.days.push({
//					n:i,
//					day: this.intl.day(thisDate),
//					enabled: thisDate >= this.min && thisDate <= this.max
//				});
//			}
//	
//			this.months.push(month);
//	
//			curMonth++;
//			if(curMonth == 13){
//				curMonth = 1;
//				curYear++;
//			}
//	
//		}
//	}
//	
//	pickDate(e){
//		var data = e.path[1].dataset.date ? e.path[1].dataset : e.path[0].dataset;
//		var day = data.date;
//		var month = data.month;
//		var year = data.year;
//		if(day && month && year){
//			warn("set date?");
//			this.date = new DateTime(year, month-1, day);
//			this.fire("selection-changed");
//		}
//	}
//	
//	refreshScrollPosition(){
//		var that = this;
//	
//		setTimeout(function(){
//	
//			var monthElements = that.shadowRoot.querySelectorAll(".month");
//			var startLoadedDateOutsideTriggerRange = new Date(monthElements[that.triggerRange].dataset.year,monthElements[that.triggerRange].dataset.month*1-1,1);
//			var endLoadedDateOutsideTriggerRange = new Date(monthElements[monthElements.length - that.triggerRange - 1].dataset.year,monthElements[monthElements.length - that.triggerRange - 1].dataset.month*1-1+1,1);
//	
//			if(!(that.date >= startLoadedDateOutsideTriggerRange && that.date < endLoadedDateOutsideTriggerRange)){
//				that.surroundDate(that.date);
//			}
//	
//		},0);
//	
//		setTimeout(function(){
//	
//			var monthElements = that.shadowRoot.querySelectorAll(".month");
//			var scrollTop = 0;
//	
//			for(var i=0;i<monthElements.length;i++){
//				if(monthElements[i].querySelector(".day.selected")){
//					break;
//				}else{
//					scrollTop += monthElements[i].clientHeight;
//				}
//			}
//	
//			if(Math.abs(that.$.container.scrollTop-scrollTop) > that.$.container.clientHeight){
//				that.$.container.scrollTop = scrollTop;
//			}
//	
//		},0);
//	
//	}
//	
//	dateChanged(){
//		this.setCurrentDateValues();
//		this.refreshScrollPosition();
//	}
//	
//	setCurrentDateValues() {
//		this.currentYear = this.date.getFullYear();
//		this.currentMonth = this.date.getMonth() + 1;
//		this.currentDay = this.date.getDate();
//	}
//	
//	scrollFinished(){
//	
//		if(this.infiniteScrolling == true){
//			var monthElements = this.shadowRoot.querySelectorAll(".month");
//	
//			for(var i=monthElements.length-1;i>=0;i--){
//				if(this.$.container.scrollTop+this.$.container.clientHeight/2 > monthElements[i].offsetTop){
//	
//					if(i<this.triggerRange){
//						var newStart = [monthElements[0].dataset.month*1-this.loadPerTrigger,monthElements[0].dataset.year*1];
//						var newEnd = [monthElements[monthElements.length-1].dataset.month*1-this.loadPerTrigger,monthElements[monthElements.length-1].dataset.year*1];
//						this.renderMonths(newStart, newEnd);
//						this.$.container.scrollTop += monthElements[0].clientHeight*this.loadPerTrigger;
//					}
//	
//					if(i>monthElements.length-this.triggerRange-1){
//						var newStart = [monthElements[0].dataset.month*1+this.loadPerTrigger,monthElements[0].dataset.year*1];
//						var newEnd = [monthElements[monthElements.length-1].dataset.month*1+this.loadPerTrigger,monthElements[monthElements.length-1].dataset.year*1];
//						this.renderMonths(newStart, newEnd);
//						this.$.container.scrollTop -= monthElements[0].clientHeight*this.loadPerTrigger;
//					}
//	
//					break;
//	
//				}
//			}
//		}
//	
//	}
//	
//	surroundDate(date){
//		if(this.infiniteScrolling == true){
//			var prevDate = [date.getMonth()+1-Math.floor(this.loadedRange/2),date.getFullYear()];
//			var nextDate = [date.getMonth()+1+Math.ceil(this.loadedRange/2),date.getFullYear()];
//		}else{
//			var prevDate = [1,date.getFullYear()];
//			var nextDate = [12,date.getFullYear()];
//		}
//	
//		this.renderMonths(prevDate,nextDate);
//	}
//	
//	ready(){
//	
//		this.weekDayNames = [];
//		for(var i=this.startDayOfWeek-1;i<this.startDayOfWeek+6;i++){
//			var date = new Date(2000, 1, i, 0, 0, 0);
//			this.weekDayNames.push(new Intl.DateTimeFormat(this.locale, {weekday:'narrow'}).format(date));
//		}
//	
//	//Infininte scrolling doesn't work (nicely) on mobile devices, so
//	// it is disabled by default.
//		if(this.infiniteScrolling == 'auto'){
//			if(typeof window.orientation !== 'undefined'){
//				this.infiniteScrolling = false;
//			}else{
//				this.infiniteScrolling = true;
//			}
//		}
//		this.surroundDate(this.date);
//		this.dateChanged();
//	
//	}
}