library syages.controllers.elements.teacherSelector;

import '../../syages.dart';

@CustomTag("sye-teacher-selector")
class TeacherSelector extends PolymerElement with ChangeNotifier {
	@reflectable @observable List<Teacher> get teachers => __$teachers; List<Teacher> __$teachers; @reflectable set teachers(List<Teacher> value) { __$teachers = notifyPropertyChange(#teachers, __$teachers, value); }
	@reflectable @observable dynamic get selected => __$selected; dynamic __$selected = -1; @reflectable set selected(dynamic value) { __$selected = notifyPropertyChange(#selected, __$selected, value); }

	@published
	Teacher get value => readValue(#value);
    set value(Teacher val) => writeValue(#value, val);

	TeacherSelector.created(): super.created(){
		this.teachers= null;
		this.refresh();
	}

	void refresh(){
		Future<List<Teacher>> future;
		future = Teacher.teachersList();
		future.then((List<Teacher> teachers){
			this.teachers = toObservable(teachers);
		}).catchError((error){
			this.teachers = null;
		});
	}

	valueChanged(){
		if(this.teachers!= null){
			this.selected = this.teachers.indexOf(this.teachers.singleWhere((i) => i.id == this.value.id));
			warn(this.selected);
		}
	}

	selectedChanged(){
		this.value = this.teachers.elementAt(this.selected);
	}
}