library syages.controllers.elements.domainsSelector;

import '../../syages.dart';

@CustomTag("sye-domains-selector")
class DomainsSelector extends PolymerElement with ChangeNotifier {
	@reflectable @observable List<Domain> get domains => __$domains; List<Domain> __$domains; @reflectable set domains(List<Domain> value) { __$domains = notifyPropertyChange(#domains, __$domains, value); }
	@reflectable @observable Map<String,bool> get checkedDomains => __$checkedDomains; Map<String,bool> __$checkedDomains; @reflectable set checkedDomains(Map<String,bool> value) { __$checkedDomains = notifyPropertyChange(#checkedDomains, __$checkedDomains, value); }
	
	@published
	List<Domain> get values => readValue(#values);
    set values(List<Domain> value) => writeValue(#values, value);
	
	DomainsSelector.created(): super.created(){
		this.domains= null;
		this.refresh();
	}
	
	void refresh(){
		Future<List<Domain>> future;
		future = Domain.domainList();
		future.then((List<Domain> domains){
			this.domains = toObservable(domains);
			this.checkedDomains = toObservable(new Map.fromIterable(this.domains,
			                                                    key: (item) => item.id.toString(),
			                                                   	value: (item) => false));
		}).catchError((error){
			this.domains = null;
		});
	}
	
	valuesChanged(oldValue, newValue){
		if(oldValue == null){
			this.values.forEach((input){
				this.checkedDomains[input.id] = true;
			});
		}
	}
	
	void updateValues(){
		this.values = new List();
		this.checkedDomains.forEach((k,v){
			if(v){
				this.values.add(this.domains.singleWhere((item) => item.id == k));
			}
		});
	}
	
	changeListener(Event e, var detail, Node target){
		updateValues();
	}
	
	
	
	
	
}