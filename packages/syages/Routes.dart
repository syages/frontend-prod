library syages.Routes;

import 'syages.dart';

abstract class Routes {
	static RouteConfiguration get notFound => new RouteConfiguration('^\r\$', "syv-error404", requireAuth: false, withNavbar: false);
	static RouteConfiguration get notFoundWhenLogged => new RouteConfiguration('^\r\$', "syv-error404", requireAuth: true, withNavbar: true);
	static RouteConfiguration get root => new RouteConfiguration('^(login)?\$', "syv-login", requireAuth: false, withNavbar: false);

	static RouteConfiguration get contactDirectory => notFoundWhenLogged;
	static RouteConfiguration get contextTester => new RouteConfiguration(r"""^ctx\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "context-tester", paramCaptures: {0: new RouteParameter("model.field")}, requireAuth: false, withNavbar: false);
	static RouteConfiguration get dashboard => new RouteConfiguration('^dashboard\$', "syv-dashboard", requireAuth: true, withNavbar: true);
	static RouteConfiguration get newsFeed => new RouteConfiguration('^news\$', "syv-newsfeed", requireAuth: true, withNavbar: true);
	static RouteConfiguration get profileSettings => new RouteConfiguration('^profile\$', "syv-profile-form", requireAuth: true, withNavbar: true);

	// Administrators
	static RouteConfiguration get a_bugReports => new RouteConfiguration('^bugReport\$', "syv-bugreport-list--a", requireAuth: true, withNavbar: true);
	static RouteConfiguration get a_bugReportDetail => new RouteConfiguration(r"""^bugReport\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-bugreport-detail--a", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_administrators => new RouteConfiguration(r"""^administrators$""", "syv-administrator-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_administratorForm => new RouteConfiguration(r"""^administrators\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-administrator-form--aps", paramCaptures: {0: new RouteParameter("administratorId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_courseForm => new RouteConfiguration(r"""^courses\/(\w{8}(-\w{4}){3}-\w{12}?)$""", 'syv-course-form--aps', paramCaptures: {0: new RouteParameter("courseId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_courses => new RouteConfiguration(r"""^courses$""", 'syv-course-list--aps', requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_diplomas => new RouteConfiguration(r"""^diplomas$""", "syv-diploma-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_domains => new RouteConfiguration(r"""^domains$""", "syv-domain-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_domainForm => new RouteConfiguration(r"""^domains\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-domain-form--aps", paramCaptures: {0: new RouteParameter("domainId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_interns => new RouteConfiguration(r"""^interns$""", "syv-intern-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_internForm => new RouteConfiguration(r"""^interns\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-intern-form--aps", paramCaptures: {0: new RouteParameter("internId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_newsForm => new RouteConfiguration(r"""^news\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-news-form--aps", paramCaptures: {0: new RouteParameter("informationId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_presidents => new RouteConfiguration(r"""^presidents$""", "syv-president-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_presidentForm => new RouteConfiguration(r"""^presidents\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-president-form--aps", paramCaptures: {0: new RouteParameter("presidentId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_staffs => new RouteConfiguration(r"""^staffs$""", "syv-staff-list--aps", requireAuth: true, withNavbar: true);
    static RouteConfiguration get aps_staffForm => new RouteConfiguration(r"""^staffs\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-staff-form--aps", paramCaptures: {0: new RouteParameter("staffId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_teachers => new RouteConfiguration(r"""^teachers$""", "syv-teacher-list--aps", requireAuth: true, withNavbar: true);
	static RouteConfiguration get aps_teacherForm => new RouteConfiguration(r"""^teachers\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-teacher-form--aps", paramCaptures: {0: new RouteParameter("teacherId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get apst_evaluationForm => new RouteConfiguration(r"""evaluations\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-evaluation-form--apst", paramCaptures: {0: new RouteParameter("evaluationId")}, requireAuth: true, withNavbar: true);
	static RouteConfiguration get apst_lectureForm => new RouteConfiguration(r"""lectures\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-lecture-form--apst", paramCaptures: {0: new RouteParameter("lectureId")}, requireAuth: true, withNavbar: true);

	// President
    static RouteConfiguration get ps_diplomas => notFoundWhenLogged;

    // Staff members

    // Teachers
    static RouteConfiguration get t_lessons => new RouteConfiguration(r"""^lessons$""", "syv-teacher-lessons--t", requireAuth: true, withNavbar: true);
    static RouteConfiguration get t_classroom => new RouteConfiguration(r"""^classroom\/(\w{8}(-\w{4}){3}-\w{12}?)$""", "syv-classroom--t", paramCaptures: {0: new RouteParameter("lessonId")}, requireAuth: true, withNavbar: true);
    static RouteConfiguration get t_lectures => notFoundWhenLogged;

    // Interns
    static RouteConfiguration get i_absences => notFoundWhenLogged;
    static RouteConfiguration get i_firstLogin => new RouteConfiguration(r"""^firstlogin$""", "syv-first-login--i", requireAuth: true, withNavbar: false);


}
