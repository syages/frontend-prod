@documented("f0e21a2e")
library syages.NavbarFactory;

import 'syages.dart';

/// Classe responsable de la génération de la barre de navigation
///
/// Cette classe est responsable de la génération de la liste d'actions qui
/// compose la barre de navigation. Cette génération dépends de l'utilisateur
/// connecté, plus précisément de sa catégorie (stagiaire, enseignant, ...).
class NavBarFactory
{
	/// Retourne une liste d'action dépendante du type de l'utilisateur
	///
	/// @see IAction
	/// @see Action
	/// @see ActionGroup
	static List<IAction> generateForUser(User user)
	{
		switch(user.kind)
		{
			case "Administrator":
				return _generateForAdministrator();
			case "Intern":
				return _generateForIntern();
			case "President":
				return _generateForPresident();
			case "Staff":
				return _generateForStaff();
			case "Teacher":
				return _generateForTeacher();
			default:
				warn("Impossible de générer la barre de navigation");
		}
		return null;
	}

	/// Cette fonction privée retourne la liste des actions commune à tous les
	/// type d'utilisateurs.
	static List<IAction> _generateCommonNavBar()
	{
		List<IAction> list = new List<IAction>();
		list.add(new Action('Actualités', ContextFactory.newsFeed()));
		list.add(new Action('Annuaire', ContextFactory.contactDirectory()));
		return list;
	}

	static List<IAction> _generateAPSNavBar()
	{
		List<IAction> list = new List<IAction>();

		ActionGroup gestionPersonnel = new ActionGroup("Comptes utilisateurs");
		gestionPersonnel.actions.add(new Action('Administrateur', ContextFactory.aps_administrators())); // TODO: AP uniquement
		gestionPersonnel.actions.add(new Action('Enseignant', ContextFactory.aps_teachers()));
		gestionPersonnel.actions.add(new Action('Personnel Administratif', ContextFactory.aps_staffs()));
		gestionPersonnel.actions.add(new Action('Président', ContextFactory.aps_presidents())); // TODO: AP uniquement
		gestionPersonnel.actions.add(new Action('Stagiaire', ContextFactory.aps_interns()));

		ActionGroup formations = new ActionGroup("Formation");
		formations.actions.add(new Action('Formations', ContextFactory.aps_diplomas()));
		formations.actions.add(new Action('Domaine', ContextFactory.aps_domains()));
		formations.actions.add(new Action('Matière', ContextFactory.aps_courses()));

		list.add(gestionPersonnel);
		list.add(formations);

		return list;
	}

	/// Cette fonction privée retourne la liste des actions propres aux
	/// administrateurs système.
	static List<IAction> _generateForAdministrator()
	{
		List<IAction> list = _generateCommonNavBar();
		list.addAll(_generateAPSNavBar());

		list.add(new Action('Rapports de Bug', ContextFactory.a_bugReports()));
		return list;
		/*
		// Temporaire
		ActionGroup grp = new ActionGroup("coucou");
		grp.actions.add(new Action("salut", null));
		grp.actions.add(new Action("hello", null));
		list.add(grp);

		ActionGroup grp2 = new ActionGroup("coucou2");
        grp2.actions.add(new Action("salut2", null));
        grp2.actions.add(new Action("hello2", null));
        list.add(grp2);
		// Fin temporaire
		 */
	}

	/// Cette fonction privée retourne la liste des actions propres aux stagiaires.
	static List<IAction> _generateForIntern()
    {
		List<IAction> list = _generateCommonNavBar();
		list.add(new Action('Absences', ContextFactory.i_absences()));
		return list;
    }

	/// Cette fonction privée retourne la liste des actions propres aux présidents.
	static List<IAction> _generateForPresident()
   	{
		List<IAction> list = _generateCommonNavBar();
		list.addAll(_generateAPSNavBar());
		list.add(new Action('Formations', ContextFactory.ps_diplomas()));
		return list;
   	}

	/// Cette fonction privée retourne la liste des actions propres aux administratifs.
	static List<IAction> _generateForStaff()
   	{
		List<IAction> list = _generateCommonNavBar();
		list.addAll(_generateAPSNavBar());
		list.add(new Action('Formations', ContextFactory.ps_diplomas()));
		return list;
   	}

	/// Cette fonction privée retourne la liste des actions propres aux enseignants.
	static List<IAction> _generateForTeacher()
    {
		List<IAction> list = _generateCommonNavBar();
		list.add(new Action('Mes enseignements', ContextFactory.t_lessons()));
		return list;
    }
}
