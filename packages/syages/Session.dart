library syages.Session;

import 'syages.dart';

class SyagesSession {

	static SyagesSession _sharedInstance = new SyagesSession._newSharedInstance();
    factory SyagesSession() => _sharedInstance;
    static SyagesSession get sharedInstance => _sharedInstance;
	
	StreamController<SyagesSession> _onLoginController;
	StreamController<SyagesSession> _onLogoutController;
	
	Stream<SyagesSession> get onLogin => _onLoginController.stream;
	Stream<SyagesSession> get onLogout => _onLogoutController.stream;
	
	DateTime _lastValidation = null;
	String _token = null;

	SyagesSession._newSharedInstance()
	{
		_onLoginController = new StreamController<SyagesSession>.broadcast();
		_onLogoutController = new StreamController<SyagesSession>.broadcast();
	}

	bool authenticate(String userName, String password) {
		if (userName == null || password == null) return false;
		HttpRequest req = new HttpRequest();
		String url = (new SyagesInstance().apiUrl("oauth/token")) + "?username=" + userName + "&password=" + password;

		req.open("GET", url, async: false);
		req.send();
		if (req.status == 204 && req.responseHeaders.containsKey("authorization")) {
			_token = req.responseHeaders["authorization"];
			window.localStorage["Authorization"] = _token;
			_onLoginController.add(this);
			return true;
		}

		_token = null;
		return false;
	}

	bool get isConnected {
		_token = window.localStorage["Authorization"];
		if (_token == null) return false;
		
		if (_lastValidation != null && new DateTime.now().difference(_lastValidation).inSeconds < 5) return true;

		String url = new SyagesInstance().apiUrl("oauth/validate");
		HttpRequest req = _preparedRequest("GET", url, async: false);
  		req.send();
  		if(req.status == 200) {
  			_lastValidation = new DateTime.now();
			return true;
  		} else {
  			logout();
  			return false;
  		}
	}

	Future<User> get loggedUser => User.me();

	void logout() {
		if (_token != null) {
      		String url = new SyagesInstance().apiUrl("oauth/revoke");
            HttpRequest req = _preparedRequest("GET", url, async: false);
            req.send();
            
            _token = null;
            window.localStorage.remove("Authorization");
      		_onLogoutController.add(this);
		}
	}
	
	String get token => _token;
	
	static HttpRequest _preparedRequest(String method, String url, {bool async: true, SyagesSession session: null}) {
		if (session == null) session = SyagesSession.sharedInstance;
		HttpRequest req = new HttpRequest();
		req.open(method, url, async: async);
		req.setRequestHeader("Content-Type", "application/json");
		if (session._token != null)
			req.setRequestHeader("Authorization", session._token);
		return req;
	}
}


